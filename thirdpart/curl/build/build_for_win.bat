@echo off

@set workdir=%cd%
@set sevenzip=%workdir%\7-Zip\7z.exe

:: build openssl
@cd %workdir%\openssl
@call build_for_win.bat %sevenzip% md static
@cd %workdir%

@pause

:: build libcurl
@cd %workdir%\libcurl
@call build_for_win.bat %sevenzip% noenable_xp md static
@cd %workdir%

:: delete openssl temp files
::@if exist %workdir%\openssl\openssl-1.0.2l (rd /s /q "%workdir%\openssl\openssl-1.0.2l") else echo can not find openssl-1.0.2l dir
::@if exist %workdir%\openssl\output_lib (rd /s /q "%workdir%\openssl\output_lib") else echo can not find output_lib dir

:: delete libcurl temp files
::@if exist %workdir%\libcurl\curl-7.63.0 (rd /s /q "%workdir%\libcurl\curl-7.63.0") else echo can not find curl-7.63 dir

@echo on

@pause