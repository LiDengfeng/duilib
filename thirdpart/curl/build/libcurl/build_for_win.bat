@echo off

echo --------------------------------------------
echo - config libcurl vc environment ...
echo --------------------------------------------
@set VS2017="%VS140COMNTOOLS%..\..\VC\Auxiliary\Build\vcvars32.bat"
@set VS2017_AMD64="%VS140COMNTOOLS%..\..\VC\Auxiliary\Build\vcvars64.bat"
@call %VS2017%


echo --------------------------------------------
echo - build libcurl ...
:: build libcurl, (use /MT)
::   1) unzip libcurl
::   2) support Windows XP
::   3) build
::   4) sync build result to kernel
echo --------------------------------------------
:: 1)unzip libcurl
@set currentPath=%cd%
@set sevenZip=%1
@set supportXP=%2
@set md=%3
@set static=%4

::@if exist %currentPath%\curl-7.63.0 (rd /s /q "%currentPath%\curl-7.63.0") else echo unzip libcurl source code ...
@if not exist %currentPath%\curl-7.63.0 (
	@%sevenZip% e %currentPath%\curl-7.63.0.tar.gz
	@%sevenZip% x %currentPath%\curl-7.63.0.tar
)
@if exist %currentPath%\curl-7.63.0.tar (del "%currentPath%\curl-7.63.0.tar")

@cd %currentPath%\curl-7.63.0\winbuild

:: 2) support Windows XP (add build command into "MakefileBuild.vc")
if "%supportXP%"=="enable_xp" (
    echo modify "winbuild/MakefileBuild.vc" to support windows xp
    python %currentPath%\build_for_win_support_xp.py %currentPath%\curl-7.63.0\winbuild\MakefileBuild.vc
)

:: 3)build
@set RTLIBCFG=static
if "%md%"=="md" (
    @set RTLIBCFG=dll
)
@set mode=dll
if "%static%"=="static" (
    @set mode=static
)
@nmake /f Makefile.vc WITH_DEVEL=../../../openssl/output_lib mode=%mode% VC=14 RTLIBCFG=%RTLIBCFG% WITH_SSL=%mode% ENABLE_SSPI=no ENABLE_WINSSL=no ENABLE_IDN=no GEN_PDB=yes DEBUG=no MACHINE=x86

:: 4)sync build result to kernel
echo --------------------------------------------
echo - sync build result to kernel ...
echo --------------------------------------------
@set output=%currentPath%\curl-7.63.0\builds\libcurl-vc14-x86-release-dll-ssl-dll-ipv6
if "%static%"=="static" (
    @set output=%currentPath%\curl-7.63.0\builds\libcurl-vc14-x86-release-static-ssl-static-ipv6
)
echo %output%
@copy /y "%output%\bin\libcurl.dll" "%currentPath%\..\..\output\bin"
@copy /y "%output%\bin\libcurl.dll" "%currentPath%\..\..\output\binr"

@copy /y "%output%\lib\libcurl.lib" "%currentPath%\..\..\output\lib"
@copy /y "%output%\lib\libcurl.lib" "%currentPath%\..\..\output\libr"

@copy /y "%output%\lib\libcurl_a.lib" "%currentPath%\..\..\output\lib\curl_a.lib"
@copy /y "%output%\lib\libcurl_a.lib" "%currentPath%\..\..\output\libr\curl_a.lib"
@copy /y "%output%\lib\libcurl_a.lib" "%currentPath%\..\..\..\..\libs\curl_a.lib"

@copy /y "%output%\lib\libcurl_a.pdb" "%currentPath%\..\..\output\lib\curl_a.pdb"
@copy /y "%output%\lib\libcurl_a.pdb" "%currentPath%\..\..\output\libr\curl_a.pdb"


@copy /y "%output%\lib\libcurl.pdb" "%currentPath%\..\..\output\bin"
@copy /y "%output%\lib\libcurl.pdb" "%currentPath%\..\..\output\binr"

@cd %currentPath%

@echo on