@echo off

echo --------------------------------------------
echo - config openssl vc environment ...
echo --------------------------------------------
@set VS2017="%VS140COMNTOOLS%..\..\VC\Auxiliary\Build\vcvars32.bat"
@set VS2017_AMD64="%VS140COMNTOOLS%..\..\VC\Auxiliary\Build\vcvars64.bat"
@call %VS2017%


echo --------------------------------------------
echo - build openssl ...
:: build openssl, (use /MT)
::   1) unzip openssl
::   2) set output dir, and create output dir
::   3) generate VC project config
::   4) replace "/MD" to "/MT" in ms/ntdll.mak
::   5) build
::   6) sync .dll and .lib files to kernel
::   7) sync .pdb files to kernel
echo --------------------------------------------
@set currentPath=%cd%
@set sevenZip=%1
@set md=%2
@set static=%3

:: 1)unzip openssl
::@if exist %currentPath%\openssl-1.0.2l (rd /s /q "%currentPath%\openssl-1.0.2l") else echo unzip openssl source code ...
@if not exist %currentPath%\openssl-1.0.2l (
	@%sevenZip% e %currentPath%\openssl-1.0.2l.tar.gz
	@%sevenZip% x %currentPath%\openssl-1.0.2l.tar
)
@if exist %currentPath%\openssl-1.0.2l.tar (del "%currentPath%\openssl-1.0.2l.tar")

:: 2)cd build dir, and create output dir
@cd %currentPath%\openssl-1.0.2l
@set outputlib=%currentPath%\output_lib

@if exist %outputlib% (rd /s /q "%outputlib%") else echo create openssl output dir
@md "%outputlib%"

:: 3)generate VC project config
@perl configure VC-WIN32 --prefix=%outputlib%
@call ms\do_ms.bat
@call ms\do_nasm.bat
echo --------------------------------------------
echo - configure finished ...
echo --------------------------------------------

:: 4)replace "/MD" to "/MT" in ms/ntdll.mak
@if /i %md% == "mt" ( goto config_mt ) else ( goto build_begin )
:config_mt
@setlocal enabledelayedexpansion

@set ntdll_mak_file=%currentPath%\openssl-1.0.2l\ms\ntdll.mak
@set ntdll_mak_file_temp=%currentPath%\openssl-1.0.2l\ms\ntdll_temp.mak
if exist %ntdll_mak_file_temp% (del %ntdll_mak_file_temp%) else echo create temp file ntdll_temp.mak"

for /f "delims=" %%i in (%ntdll_mak_file%) do (
    set str=%%i
    set str=!str:/MD=/MT!
    echo !str!>>%ntdll_mak_file_temp%
)
@move /y "%ntdll_mak_file_temp%" "%ntdll_mak_file%"

@set nt_mak_file=%currentPath%\openssl-1.0.2l\ms\nt.mak
@set nt_mak_file_temp=%currentPath%\openssl-1.0.2l\ms\nt_temp.mak
if exist %nt_mak_file_temp% (del %nt_mak_file_temp%) else echo create temp file nt_temp.mak"

for /f "delims=" %%i in (%nt_mak_file%) do (
    set str=%%i
    set str=!str:/MD=/MT!
    echo !str!>>%nt_mak_file_temp%
)
@move /y "%nt_mak_file_temp%" "%nt_mak_file%"

@endlocal enabledelayedexpansion

:build_begin
:: 5)build
echo --------------------------------------------
echo - build %static% %md% ...
echo --------------------------------------------
@set mak_file=ms\ntdll.mak
@if /i "%static%" == "static" (
	@set mak_file=ms\nt.mak
)
@nmake -f %mak_file%
@nmake -f %mak_file% install
@cd %currentPath%

:: 6)sync .dll and .lib files to kernel
echo --------------------------------------------
echo - sync .dll and .lib files to kernel %currentPath%\..\..\output...
echo --------------------------------------------
@copy /y "%outputlib%\lib\ssleay32.lib" "%currentPath%\..\..\..\..\libs"
@copy /y "%outputlib%\lib\ssleay32.lib" "%currentPath%\..\..\output\lib"
@copy /y "%outputlib%\lib\ssleay32.lib" "%currentPath%\..\..\output\libr"
@copy /y "%outputlib%\bin\ssleay32.dll" "%currentPath%\..\..\output\bin"
@copy /y "%outputlib%\bin\ssleay32.dll" "%currentPath%\..\..\output\binr"

@copy /y "%outputlib%\lib\libeay32.lib" "%currentPath%\..\..\..\..\libs"
@copy /y "%outputlib%\lib\libeay32.lib" "%currentPath%\..\..\output\lib"
@copy /y "%outputlib%\lib\libeay32.lib" "%currentPath%\..\..\output\libr"
@copy /y "%outputlib%\bin\libeay32.dll" "%currentPath%\..\..\output\bin"
@copy /y "%outputlib%\bin\libeay32.dll" "%currentPath%\..\..\output\binr"

:: 7)sync .pdb files to kernel
echo --------------------------------------------
echo - sync .pdb files to kernel ...
echo --------------------------------------------
@set build_cache_dir=%currentPath%\openssl-1.0.2l\out32dll

@copy /y "%build_cache_dir%\ssleay32.pdb" "%currentPath%\..\..\output\bin"
@copy /y "%build_cache_dir%\ssleay32.pdb" "%currentPath%\..\..\output\binr"

@copy /y "%build_cache_dir%\libeay32.pdb" "%currentPath%\..\..\output\bin"
@copy /y "%build_cache_dir%\libeay32.pdb" "%currentPath%\..\..\output\binr"


@echo on