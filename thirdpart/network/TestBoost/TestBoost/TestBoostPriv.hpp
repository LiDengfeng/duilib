//
//  TestBoostPriv.hpp
//  TestBoost
//
//  Created by Dengfeng Li on 2019/6/23.
//  Copyright © 2019 Dengfeng Li. All rights reserved.
//

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class TestBoostPriv
{
    public:
    void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
