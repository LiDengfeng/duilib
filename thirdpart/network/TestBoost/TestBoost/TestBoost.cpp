//
//  TestBoost.cpp
//  TestBoost
//
//  Created by Dengfeng Li on 2019/6/23.
//  Copyright © 2019 Dengfeng Li. All rights reserved.
//

#include <iostream>
#include "TestBoost.hpp"
#include "TestBoostPriv.hpp"

void TestBoost::HelloWorld(const char * s)
{
    TestBoostPriv *theObj = new TestBoostPriv;
    theObj->HelloWorldPriv(s);
    delete theObj;
};

void TestBoostPriv::HelloWorldPriv(const char * s) 
{
    std::cout << s << std::endl;
};

