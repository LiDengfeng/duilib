//
//  TestBoost.hpp
//  TestBoost
//
//  Created by Dengfeng Li on 2019/6/23.
//  Copyright © 2019 Dengfeng Li. All rights reserved.
//

#ifndef TestBoost_
#define TestBoost_

/* The classes below are exported */
#pragma GCC visibility push(default)

class TestBoost
{
    public:
    void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
