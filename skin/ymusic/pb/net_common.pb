
��
PB/net_common.protoPB"3
a3d_pos
x (Rx
z (Rz
y (Ry">
name_roleid_pair
roleid (Rroleid
name (Rname"`
color_info_t
color_index (R
colorIndex/
color_concentration (RcolorConcentration"Y
hometown_object_pos
valid (Rvalid
x (Rx
y (Ry
dir (Rdir"�
hometown_object
index (Rindex 

object_tid (:0R	objectTid
count (Rcount&
valid_pos_count (RvalidPosCount)
pos (2.PB.hometown_object_posRpos
	pos_index (RposIndex
npc_tid (RnpcTid"W
guest_raise
roleid (Rroleid
	timestamp (R	timestamp
name (Rname"�
farm_record
raise_ts (RraiseTs
stealer (Rstealer%
raise (2.PB.guest_raiseRraise
	gather_ts (RgatherTs,
guests (2.PB.name_roleid_pairRguests"�
farm_object
farmid (Rfarmid
objectid (Robjectid3
stage (2.PB.farm_object.FARM_OB_STAGERstage
growth (Rgrowth
products (Rproducts
next_ts (RnextTs'
record (2.PB.farm_recordRrecord

farm_level (R	farmLevel"y
FARM_OB_STAGE
STAGE_DISABLE 
STAGE_EMPTY
STAGE_CHILD
STAGE_YOUTH
STAGE_ADULT
STAGE_HARVEST"�
revive_data_t/
stand_revive_times (:0RstandReviveTimes<
stand_revive_times_lianxu (:0RstandReviveTimesLianxu3
perfect_revive_times (:0RperfectReviveTimes@
perfect_revive_times_lianxu (:0RperfectReviveTimesLianxu+
last_revive_type (:0RlastReviveType&
revive_on_enter (RreviveOnEnter"�
resurrect_state_t 

delay_time (:0R	delayTime5
last_revive_timestamp (:0RlastReviveTimestamp"
death_times (:0R
deathTimes 

death_type (:0R	deathType
flags (:0Rflags"/
	name_ruid
id (Rid
name (Rname"^
red_envelope_history0
receiver (2.PB.name_roleid_pairRreceiver
money (Rmoney"�
red_envelope_info
id (Rid
	timestamp (R	timestamp(
redenvelopetype (Rredenvelopetype,
sender (2.PB.name_roleid_pairRsender#
sender_gender (RsenderGender!
sender_photo (RsenderPhoto2
	receivers (2.PB.name_roleid_pairR	receivers
content (Rcontent(
receiver_num_max	 (RreceiverNumMax
money_total
 (R
moneyTotal%
money_received (RmoneyReceived4
historys (2.PB.red_envelope_historyRhistorys"I
lbs_pos
	longitude (:0R	longitude
latitude (:0Rlatitude".

reputation
id (Rid
val (Rval"P
inner_friend_extend_date
	signature (R	signature
nation (Rnation"�
roamer_ds_info
roleid (Rroleid
name (Rname
account (Raccount
level (Rlevel

cur_nation (R	curNation
nation (Rnation
lineid (Rlineid
mid (Rmid
gender	 (Rgender
prof
 (Rprof0
reputations (2.PB.reputationRreputations$
fightcapacity (Rfightcapacity
idphoto (Ridphoto6
efriend (2.PB.inner_friend_extend_dateRefriend$
last_scene_tag (RlastSceneTag
corp_id (RcorpId
	corp_name (RcorpName.
lbs_position (2.PB.lbs_posRlbsPosition&
is_lost_connect (RisLostConnect"�
corps_battle_config@
state (2*.PB.corps_battle_config.CORPS_BATTLE_STATERstate
	timestamp (R	timestamp;
corps_battle_enable_zoneid (RcorpsBattleEnableZoneid"w
CORPS_BATTLE_STATE
CORPS_BATTLE_STATE_CLEAR
CORPS_BATTLE_STATE_MATCHED#
CORPS_BATTLE_STATE_BATTLE_BEGIN"]
corps_tower_battle_rankB
tower_players (2.PB.corps_tower_battle_playerRtowerPlayers"�
corps_battle_order
index (Rindex/
corps_battle_index_1 (RcorpsBattleIndex1/
corps_battle_index_2 (RcorpsBattleIndex2
battle_time (R
battleTime
result (:-1RresultB
city_players_1 (2.PB.corps_city_battle_playerRcityPlayers1B
city_players_2 (2.PB.corps_city_battle_playerRcityPlayers2E
tower_players_1 (2.PB.corps_tower_battle_playerRtowerPlayers1E
tower_players_2	 (2.PB.corps_tower_battle_playerRtowerPlayers2)
corps_battle_id_1
 (RcorpsBattleId1)
corps_battle_id_2 (RcorpsBattleId2-
corps_battle_name_1 (RcorpsBattleName1-
corps_battle_name_2 (RcorpsBattleName2/
corps_battle_level_1 (RcorpsBattleLevel1/
corps_battle_level_2 (RcorpsBattleLevel2;
corps_server_battle_target (RcorpsServerBattleTarget*
single_battle (:falseRsingleBattle"�
corps_battle_info
index (Rindex
id (Rid
name (Rname!
battle_level (RbattleLevel1
records (2.PB.corps_battle_recordRrecords
order_index (R
orderIndex"
total_count (:0R
totalCount
	win_count (:0RwinCount+
last_order_index	 (:0RlastOrderIndex
activity
 (:0Ractivity 

apply_time (:0R	applyTimeN
server_battle_cities (2.PB.corps_server_battle_cityRserverBattleCities'
server_result (:-1RserverResult9
server_result_timestamp (:0RserverResultTimestamp"J
dyn_surface
tid (Rtid)
expire_timestamp (RexpireTimestamp"A
talisman_combination_info
id (Rid
level (Rlevel"�
black_shop_info
shop_id (RshopId*
next_refresh_time (RnextRefreshTime?
goods (2).PB.black_shop_info.black_shop_goods_infoRgoods�
black_shop_goods_info
store_id (RstoreId
goods_id (RgoodsId
	is_selled (RisSelled
	goods_pos (RgoodsPos"�
climbtower_magic_shop_info
shop_id (RshopId
level (RlevelJ
goods (24.PB.climbtower_magic_shop_info.magic_shop_goods_infoRgoods�
magic_shop_goods_info
store_id (RstoreId
goods_id (RgoodsId
	is_selled (RisSelled
	goods_pos (RgoodsPos"�
db_hero_trial_config,
free_refresh_times (RfreeRefreshTimes*
pay_refresh_times (RpayRefreshTimes 
cur_hero_tid (R
curHeroTid"=
level_shopid
level (Rlevel
shop_id (RshopId"�
corp_farmland(
cur_planting_tid (RcurPlantingTid

start_time (R	startTime!
harvest_time (RharvestTime
	can_speed (RcanSpeed"l
db_corp_farm_data
level (Rlevel
exp (Rexp/
	farmlands (2.PB.corp_farmlandR	farmlands"V
player_corps_cache_t
corps_id (:0RcorpsId 
in_corps (:falseRinCorps"�
db_chat_public_detail
vip (Rvip
prof (Rprof
gender (Rgender
city_pos (RcityPos
nation (Rnation
photo (Rphoto
hide_vip (RhideVip"
hide_vip_name (RhideVipName$
bubble_index	 (:0RbubbleIndex 

font_index
 (:0R	fontIndex"�
db_rename_info_item
roleid (:0Rroleid,
remaining_chance (:0RremainingChance5
last_rename_timestamp (:0RlastRenameTimestamp"l
db_rename_info$
total_chance (:0RtotalChance4
	item_list (2.PB.db_rename_info_itemRitemList"e
db_bind_mobile_phone_info
award_times (R
awardTimes'
award_timestamp (RawardTimestamp"�
refence_db_operate0
op (2 .PB.refence_db_operate.oper_typeRop
cash_shared (R
cashShared"k
	oper_type
REFER_OP_SEND_GIFT
REFER_OP_SEND_CASH
REFER_OP_RECV_GIFT
REFER_OP_RECV_CASH"�
db_refence_db_info(
referee_roleid (:0RrefereeRoleid&
gift_received (:0RgiftReceived&
cash_received (:0RcashReceived*
referral_roleid (:0RreferralRoleid"
cash_shared (:0R
cashShared"
gift_shared (:0R
giftShared"�
	item_info
item_tid (:0RitemTid 

item_count (:0R	itemCount"
item_expire (:0R
itemExpire 

item_state (:0R	itemState/
item_has_content (:falseRitemHasContent!
item_content (RitemContent"�
	item_data
item_tid (:0RitemTid
pos (:0Rpos
count (:0Rcount
state (:0Rstate"
expire_date (:0R
expireDate
content (Rcontent"E
	repu_info
repu_id (:0RrepuId
repu_inc (:0RrepuInc"�
player_reward_item_t
item_tid (:0RitemTid 

item_count (:0R	itemCount
bind (:0Rbind'
genitem_cfg_id (:0RgenitemCfgId
period (:0Rperiod
quality (:0Rquality"�
player_reward_t
tid (:0Rtid
	timestamp (:0R	timestamp"
auto_reward (:0R
autoReward
exp (:0Rexp
prof_exp (:0RprofExp 

bind_money (:0R	bindMoney"
trade_money (:0R
tradeMoney
	bind_cash (:0RbindCash
	item_bind	 (:0RitemBind0
reputations
 (2.PB.reputationRreputations.
items (2.PB.player_reward_item_tRitems
pos (2.PB.a3d_posRpos 

money_heap (:0R	moneyHeap$

stored_exp (:falseR	storedExp"
extra_param (:0R
extraParam"I
player_auto_reward_t
reward (:0Rreward
params (Rparams"�
db_player_enhance_data9
enhance_parts (2.PB.player_enhance_tRenhanceParts6
enhance_gfx_suit_level (:0RenhanceGfxSuitLevel.
gem_gfx_suit_level (:0RgemGfxSuitLevel"�
player_kotodama_t(
kotodama_point (:0RkotodamaPoint3
kotodama_data (2.PB.kotodama_tRkotodamaData;
combat_kotodama_selected (:0RcombatKotodamaSelected=
enhance_kotodama_selected (:0RenhanceKotodamaSelected"/
direct_lottery_t
	seek_data (RseekData"O
personal_photo_data_t
photoid (Rphotoid
	timestamp (R	timestamp".
roam_mafia_info_t
mafia_id (RmafiaId"�
roam_arena_group_info_t$
arena_group_id (RarenaGroupId*
arena_group_grade (RarenaGroupGrade(
arena_group_name (	RarenaGroupName"�
player_char_mode_t$
fashion_mode (:0RfashionMode
	dead_mode (:0RdeadMode$
revive_count (:0RreviveCount
	direction (:0R	direction-
death_in_instance (:0RdeathInInstance3
resurrect_delay_time (:0RresurrectDelayTime6
leave_new_player_scene (:0RleaveNewPlayerScene"8
skillprop_data_t
id (Rid
level (Rlevel"e
random_addon_data_t
index (Rindex
rand (Rrand
type (Rtype
tid (Rtid"�
addon_group_data_t
id (:0Rid(
basic_addon_data (RbasicAddonDataC
random_addon_data (2.PB.random_addon_data_tRrandomAddonData;
skillprop_data (2.PB.skillprop_data_tRskillpropData
quality (Rquality
actived (:falseRactived"�
equip_fixed_data_t
grade (Rgrade
quality (Rquality(
base_addon_count (RbaseAddonCount*
extra_addon_count (RextraAddonCount5
extra_skill_addon_count (RextraSkillAddonCount"�
equipment_essence_t
id (Rid3
	fixed_ess (2.PB.equip_fixed_data_tRfixedEss@
base_addon_group (2.PB.addon_group_data_tRbaseAddonGroupB
extra_addon_group (2.PB.addon_group_data_tRextraAddonGroupB
extra_skill_group (2.PB.addon_group_data_tRextraSkillGroup"'
general_essence_t
data (Rdata"A
delay_gift_essence_t)
create_timestamp (RcreateTimestamp"Y
monster_control_essence_t
version (Rversion"
can_use_count (RcanUseCount">
diamond_bag_essence_t%
diamond_amount (RdiamondAmount"�
chest_essence_t
	scene_tag (RsceneTag)
scene_difficulty (RsceneDifficulty%
treasure_level (RtreasureLevel.
treasure_lvlup_cash (RtreasureLvlupCash'
already_levelup (RalreadyLevelup"� 
child_bedge_essence_t
stage (Rstage*
mother (2.PB.player_id_nameRmother*
father (2.PB.player_id_nameRfather(
child (2.PB.player_id_nameRchild*
spouse (2.PB.player_id_nameRspouse7
spouse_parent (2.PB.player_id_nameRspouseParentO
fixed_ess_1 (2/.PB.child_bedge_essence_t.stage_1_fixed_essenceR	fixedEss1O
fixed_ess_2 (2/.PB.child_bedge_essence_t.stage_2_fixed_essenceR	fixedEss2O
fixed_ess_3	 (2/.PB.child_bedge_essence_t.stage_3_fixed_essenceR	fixedEss3O
fixed_ess_4
 (2/.PB.child_bedge_essence_t.stage_4_fixed_essenceR	fixedEss4�
data_ChildProp
maxHP (RmaxHP
	curPhyAtk (R	curPhyAtk
	curMagAtk (R	curMagAtk
	curPhyDef (R	curPhyDef
	curMagDef (R	curMagDef"
curCritLevel (RcurCritLevel(
curCritResLevel (RcurCritResLevel*
curElementalDmg1 (RcurElementalDmg1*
curElementalDmg2	 (RcurElementalDmg2*
curElementalDmg3
 (RcurElementalDmg3*
curElementalDmg4 (RcurElementalDmg4*
curElementalDef1 (RcurElementalDef1*
curElementalDef2 (RcurElementalDef2*
curElementalDef3 (RcurElementalDef3*
curElementalDef4 (RcurElementalDef4 
curPVPLevel (RcurPVPLevel&
curPVPResLevel (RcurPVPResLevel"
curCritRatio (RcurCritRatio(
curCritRatioRes (RcurCritRatioRes*
curBaseProperty1 (RcurBaseProperty1*
curBaseProperty2 (RcurBaseProperty2*
curBaseProperty3 (RcurBaseProperty3*
curBaseProperty4 (RcurBaseProperty4*
curBaseProperty5 (RcurBaseProperty5*
curBaseProperty6 (RcurBaseProperty6*
FightingCapacity (RFightingCapacity�
data_ChildAddPointProp.
plan1BaseProperty1 (Rplan1BaseProperty1.
plan1BaseProperty2 (Rplan1BaseProperty2.
plan1BaseProperty3 (Rplan1BaseProperty3.
plan1BaseProperty4 (Rplan1BaseProperty4.
plan1BaseProperty5 (Rplan1BaseProperty5.
plan1BaseProperty6 (Rplan1BaseProperty68
plan1UnusedBaseProperty (Rplan1UnusedBaseProperty�
child_aptitude_t
health (Rhealth

phy_attack (R	phyAttack!
magic_attack (RmagicAttack
phy_defence (R
phyDefence#
magic_defence (RmagicDefence
crit (Rcrit
crit_resist (R
critResist?
exp_per_day_t
exp (Rexp
	timestamp (R	timestampH
	destiny_t
tid (Rtid
exp (Rexp
level (:1Rlevel�
tour_info_t
event (Revent
state (Rstate*
single_tour_count (RsingleTourCount.
last_tour_timestamp (RlastTourTimestampc
gem_slot_info_t
tid (Rtid

star_level (R	starLevel
lucky_point (R
luckyPoint3
stage_1_fixed_essence
progress (Rprogress�
stage_2_fixed_essence
progress (Rprogress
food_factor (R
foodFactor
neat_factor (R
neatFactor%
emotion_factor (RemotionFactor!
tired_factor (RtiredFactor
is_sleeping (R
isSleeping*
last_culture_time (RlastCultureTime.
progress_check_time (RprogressCheckTime(
sleep_check_time	 (RsleepCheckTime�
stage_3_fixed_essence<
prop (2(.PB.child_bedge_essence_t.data_ChildPropRpropV
add_point_prop (20.PB.child_bedge_essence_t.data_ChildAddPointPropRaddPointPropF
aptitude (2*.PB.child_bedge_essence_t.child_aptitude_tRaptitude
level (Rlevel
exp (Rexp#
growth_factor (RgrowthFactor

fix_skills (R	fixSkills
talent_type (R
talentType-
use_prop_item_count	 (RusePropItemCountM
	prop_plan
 (20.PB.child_bedge_essence_t.data_ChildAddPointPropRpropPlanX
plus_point_prop (20.PB.child_bedge_essence_t.data_ChildAddPointPropRplusPointPropG
exp_per_day (2'.PB.child_bedge_essence_t.exp_per_day_tR	expPerDay=
destiny (2#.PB.child_bedge_essence_t.destiny_tRdestiny

fashion_id (R	fashionId#
emotion_level (RemotionLevel
honey_value (R
honeyValue 
talk_cd_time (R
talkCdTime5
last_check_emotion_time (RlastCheckEmotionTimeB
	tour_info (2%.PB.child_bedge_essence_t.tour_info_tRtourInfo�
stage_4_fixed_essence8
fashion_data (2.PB.baby_fashion_dataRfashionDataD
gem_slot (2).PB.child_bedge_essence_t.gem_slot_info_tRgemSlot"�
pet_bedge_essence_tL
	fixed_ess (2/.PB.pet_bedge_essence_t.pet_bedge_fixed_essenceRfixedEss
pet_name (	RpetNameC

inner_data (2$.PB.pet_bedge_essence_t.inner_data_tR	innerData�
data_PetProp
maxHP (RmaxHP
	curPhyAtk (R	curPhyAtk
	curMagAtk (R	curMagAtk
	curPhyDef (R	curPhyDef
	curMagDef (R	curMagDef"
curCritLevel (RcurCritLevel(
curCritResLevel (RcurCritResLevel*
curBaseProperty1 (RcurBaseProperty1*
curBaseProperty2	 (RcurBaseProperty2*
curBaseProperty3
 (RcurBaseProperty3*
curBaseProperty4 (RcurBaseProperty4*
curBaseProperty5 (RcurBaseProperty5*
curBaseProperty6 (RcurBaseProperty6*
FightingCapacity (RFightingCapacity�
data_PetAddPointProp.
plan1BaseProperty1 (Rplan1BaseProperty1.
plan1BaseProperty2 (Rplan1BaseProperty2.
plan1BaseProperty3 (Rplan1BaseProperty3.
plan1BaseProperty4 (Rplan1BaseProperty4.
plan1BaseProperty5 (Rplan1BaseProperty5.
plan1BaseProperty6 (Rplan1BaseProperty68
plan1UnusedBaseProperty (Rplan1UnusedBaseProperty5
pet_aptitude_t#
aptitude_list (RaptitudeList
skill_t
id (Rid:
inner_data_t
index (Rindex
value (Rvalue�
pet_bedge_fixed_essence
version (Rversion
origin (Rorigin0
can_active_timestamp (RcanActiveTimestamp8
prop (2$.PB.pet_bedge_essence_t.data_PetPropRpropR
add_point_prop (2,.PB.pet_bedge_essence_t.data_PetAddPointPropRaddPointProp
aptitude (Raptitude0
support_service_mask (RsupportServiceMask
level (Rlevel
exp	 (Rexp
cur_hp
 (RcurHp#
growth_factor (RgrowthFactor>

fix_skills (2.PB.pet_bedge_essence_t.skill_tR	fixSkills0
selected_skill_index (RselectedSkillIndex
talent_type (R
talentType-
use_prop_item_count (RusePropItemCountI
	prop_plan (2,.PB.pet_bedge_essence_t.data_PetAddPointPropRpropPlan
identity (RidentityT
plus_point_prop (2,.PB.pet_bedge_essence_t.data_PetAddPointPropRplusPointProp!
reborn_count (RrebornCount!
readan_level (RreadanLevel(
skill_lock_state (RskillLockState"-
lottery_essence_t
numbers (Rnumbers"6
pet_skill_essence_t
skill_level (R
skillLevel"�
guard_essence
tid (Rtid
level (Rlevel
exp (Rexp
phase (Rphase%
train_aptitude (RtrainAptitude
train_count (R
trainCount'
evolution_value (RevolutionValue"
used_skill_id (RusedSkillId
shape	 (Rshape
name
 (Rname#
train_counter (RtrainCounter"E
guard_slot_essence
level (Rlevel
guard_id (RguardId"�
guard_client_data+
essence (2.PB.guard_essenceRessence.
self_fight_capacity (RselfFightCapacity2
attach_fight_capacity (RattachFightCapacity
cur_prop (RcurProp
attach_prop (R
attachProp(
base_attach_prop (RbaseAttachProp;
base_attach_fight_capacity (RbaseAttachFightCapacity"b
retinue_fashion#
fashion_index (RfashionIndex*
active_color_plan (RactiveColorPlan"<
retinue_private_info
id (Rid
count (Rcount"9
retinue_gift_info
id (Rid
count (Rcount">
retinue_chat
chat_id (RchatId
msg_id (RmsgId"�
retinue_info

retinue_id (R	retinueId
level (Rlevel
exp (:0Rexp
quality (Rquality
count (:0Rcount%
relation_skill (RrelationSkill
taskid (:0Rtaskid
privates	 (Rprivates/
fashions (2.PB.retinue_fashionRfashions%
select_fashion (RselectFashion!
select_color (RselectColor
mood (Rmood%
mood_timestamp (RmoodTimestamp
amity (Ramity&
take_gift_level (RtakeGiftLevel$
chat (2.PB.retinue_chatRchat"�
retinue_chat_info

retinue_id (R	retinueId
level (Rlevel
quality (Rquality)
relation_retinue (RrelationRetinue
privates (Rprivates
prop (Rprop"@
title_chat_info
title_id (RtitleId
data (Rdata"�

role_brief
id (:0Rid
name (Rname
gender (:0Rgender
idphoto (:0Ridphoto!

profession (:0R
profession
level (:0Rlevel
account (	Raccount"�
collection_info
id (Rid
expire_time (R
expireTime
get_time (RgetTime
pos (:0Rpos
	direction (:0R	direction"�
plat_social_space_data

background (R
background
medal (Rmedal
frame (Rframe
ornament (Rornament
version (:0RversionF

style_info (2'.PB.plat_social_space_data.style_info_tR	styleInfoE
style_info_t
index (Rindex
expire_time (R
expireTime"�
	QQVipInfo
vip_flag (RvipFlag
is_vip (RisVip
is_year (RisYear
	is_luxury (RisLuxury
level (Rlevel)
expire_timestamp (RexpireTimestamp"�
GrcRoleInfo
roleid (Rroleid
rolename (	Rrolename
level (Rlevel
gender (Rgender

profession (R
profession
race (Rrace
reserved (Rreserved*
fightingcapacity (Rfightingcapacity
platform	 (Rplatform
zoneid
 (Rzoneid

extra_info (R	extraInfo"�
GrcUserProfileInfo
nickname (	Rnickname
gender (Rgender

figure_url (	R	figureUrl
province (	Rprovince
city (	Rcity"�
GrcUserInfo
openid (	Ropenid
channel (	Rchannel
nickname (	Rnickname
gender (Rgender

figure_url (	R	figureUrl
province (	Rprovince
city (	Rcity'
login_privilege (RloginPrivilege/
qq_vip_infos	 (2.PB.QQVipInfoR
qqVipInfos+
roleinfo
 (2.PB.GrcRoleInfoRroleinfo".
GrcSendGiftInfo
	to_openid (	RtoOpenid"z
GrcUserSendGiftInfo
	gift_type (RgiftTypeF
today_send_gift_infos (2.PB.GrcSendGiftInfoRtodaySendGiftInfos"j
GrcUserReceiveGiftTimesInfo
	gift_type (RgiftType.
today_receive_times (RtodayReceiveTimes"O
GrcUserReceiveGiftMetaInfo
	gift_type (RgiftType
onoff (Ronoff"�
GrcReceiveGiftInfo
	gift_type (RgiftType
serialid (Rserialid
from_openid (	R
fromOpenid#
from_nickname (	RfromNickname&
from_figure_url (	RfromFigureUrl

gift_count (R	giftCount
	timestamp (R	timestamp"�
GrcReceivedGiftInfos
	gift_type (RgiftType

gift_count (R	giftCount#
receive_times (RreceiveTimes
	serialids (R	serialids"�
GrcSimpleGiftInfo
	gift_type (RgiftType
serialid (Rserialid

gift_count (R	giftCount
	timestamp (R	timestamp"
GrcFriendWithGiftInfo0
friend_info (2.PB.GrcUserInfoR
friendInfo4

gift_infos (2.PB.GrcSimpleGiftInfoR	giftInfos"�
	hero_info
index (Rindex
tid (Rtid
property (Rproperty
quality (Rquality
exp (Rexp
level (Rlevel
prof_exp (RprofExp

prof_level (R	profLevel
is_summoned	 (R
isSummoned
is_flysword
 (R
isFlysword
train_level (R
trainLevel
	train_num (RtrainNum
cur_surface (R
curSurface3
surfaces (2.PB.gp_hero_dyn_surfaceRsurfaces*
fightingcapacity (Rfightingcapacity:
surface_info_set (2.PB.surface_infoRsurfaceInfoSet"h
npt_req_adventure_task_ratioH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REQ_ADVENTURE_TASK_RATIORtype"�
npt_rep_adventure_task_ratioH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REP_ADVENTURE_TASK_RATIORtype?
task (2+.PB.npt_rep_adventure_task_ratio.task_ratioRtask#
created_roles (RcreatedRolesG

task_ratio
taskid (Rtaskid!
finish_count (RfinishCount"d
npt_get_openid:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_OPENIDRtype
roleid (Rroleid"�
npt_get_openid_re=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_OPENID_RERtype
roleid (Rroleid
openid (	Ropenid"d
npt_get_server_debug_stateF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_SERVER_DEBUG_STATERtype"�
npt_get_server_debug_state_reI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_SERVER_DEBUG_STATE_RERtype 

debug_mode (:0R	debugMode"�
npt_team_chatroom_op@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_CHATROOM_OPRtype
roleid (Rroleid3
op (2#.PB.npt_team_chatroom_op.TC_OP_TYPERop
index (Rindex"'

TC_OP_TYPE
OP_JOIN
OP_LEAVE"�
npt_team_chatroom_notifyD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_CHATROOM_NOTIFYRtypeF
members (2,.PB.npt_team_chatroom_notify.chatroom_memberRmembers?
chatroom_member
roleid (Rroleid
index (Rindex"�
npt_team_chatroom_inviteD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_CHATROOM_INVITERtype
inviter (Rinviter
invitee (Rinvitee
reply (Rreply"�
npt_corps_search_detail_infoH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_SEARCH_DETAIL_INFORtype
corps_id (RcorpsId-
master (2.PB.corps_member_infoRmaster 
recruitment (Rrecruitment"x
npt_get_player_team_infoD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_PLAYER_TEAM_INFORtype
roleid (Rroleid"�
npt_get_player_team_info_reG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_PLAYER_TEAM_INFO_RERtype
roleid (Rroleid
teamid (:0Rteamid"�
npt_player_search_briefC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_SEARCH_BRIEFRtype
search_type (R
searchType,

name_ruids (2.PB.name_ruidR	nameRuids"�
npt_player_search_brief_reF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_SEARCH_BRIEF_RERtype
search_type (R
searchType/
role_briefs (2.PB.role_briefR
roleBriefs"�
npt_secret_love_op>
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SECRET_LOVE_OPRtype5
op (2%.PB.npt_secret_love_op.SECRET_LOVE_OPRop
roleid (Rroleid
msg (Rmsg"P
SECRET_LOVE_OP

SECRET_ADD

SECRET_DEL

SECRET_MSG

SECRET_GET"�
npt_secret_love_notifyB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SECRET_LOVE_NOTIFYRtypeE
notify (2-.PB.npt_secret_love_notify.SECRET_LOVE_NOTIFYRnotify+
lover (2.PB.secret_lover_infoRlover
msg (Rmsg"k
SECRET_LOVE_NOTIFY
	BOTH_LOVE
	ADD_LOVER
	DEL_LOVER
SEND_MSG
GET_INFO
RECV_MSG"�
npt_golden_intimate_inviteF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GOLDEN_INTIMATE_INVITERtype
name (RnameK
invite_type (2*.PB.npt_golden_intimate_invite.INVITE_TYPER
inviteType
intimate_id (R
intimateId#
intimate_name (RintimateName
txnid (Rtxnid":
INVITE_TYPE
	IT_CREATE
	IT_DELETE
	IT_RENAME"�
npt_golden_intimate_operateG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GOLDEN_INTIMATE_OPERATERtype+
op (2.PB.GOLDEN_INTIMATE_OPERATERop"
targetroleid (Rtargetroleid
self_msg (RselfMsg
award_level (R
awardLevel#
intimate_name (RintimateName
theme_id (RthemeId"�
player_golden_intimate
roleid (Rroleid
name (Rname
value (Rvalue
	leave_msg (RleaveMsg
self_msg (RselfMsg&
red_award_level (RredAwardLevel!
select_theme (RselectTheme
logout_time (R
logoutTime"�
"npt_golden_intimate_operate_resultJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GOLDEN_INTIMATE_OPERATE_RERtype
retcode (Rretcode
roleid (Rroleid+
op (2.PB.GOLDEN_INTIMATE_OPERATERop"
targetroleid (Rtargetroleid8
	intimates (2.PB.player_golden_intimateR	intimates"�
npt_sync_func_switch@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SYNC_FUNC_SWITCHRtypeE
switch_list (2$.PB.npt_sync_func_switch.func_switchR
switchListM
func_switch
	func_type (RfuncType!
switch_value (RswitchValue"`
npt_get_recommend_friendD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_RECOMMEND_FRIENDRtype"�
recommend_friend
roleid (Rroleid
name (Rname
gender (Rgender
idphoto (Ridphoto

profession (R
profession
level (Rlevel"�
npt_get_recommend_friend_reG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_RECOMMEND_FRIEND_RERtype;
mutual_friends (2.PB.recommend_friendRmutualFriends7
mutual_corps (2.PB.recommend_friendRmutualCorps7
online_males (2.PB.recommend_friendRonlineMales;
online_females (2.PB.recommend_friendRonlineFemales"�
npt_friend_group_changeC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FRIEND_GROUP_CHANGERtype=
change_type (2.PB.FRIEND_GROUP_CHANGE_TYPER
changeType
group_id (RgroupId

group_name (R	groupName$
sort_group_ids (RsortGroupIds"�
npt_friend_group_change_reF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FRIEND_GROUP_CHANGE_RERtype=
change_type (2.PB.FRIEND_GROUP_CHANGE_TYPER
changeType
err_code (RerrCode
group_id (RgroupId

group_name (R	groupName$
sort_group_ids (RsortGroupIds"�
npt_friend_group_moveA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FRIEND_GROUP_MOVERtype
group_id (RgroupId
	friendids (R	friendids"�
npt_social_space_token_checkH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SOCIAL_SPACE_TOKEN_CHECKRtype
token (Rtoken'
token_timestamp (RtokenTimestamp)
expire_timestamp (RexpireTimestamp
userid (Ruserid"l
npt_player_constellation_beginJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONSTELLATION_BEGINRtype"�
constellation_question/
constellation_index (RconstellationIndex
	tip_index (RtipIndex
options (Roptions"�
"npt_player_constellation_questionsN
type (2.PB.NET_PROTOCBUF_TYPE:"NPT_PLAYER_CONSTELLATION_QUESTIONSRtype

start_time (R	startTime8
	questions (2.PB.constellation_questionR	questions"�
npt_get_h5_game_rewardB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_H5_GAME_REWARDRtype
roleid (Rroleid
day (Rday
gear (Rgear"x
npt_refresh_h5_game_repuD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REFRESH_H5_GAME_REPURtype
roleid (Rroleid"�
npt_player_constellation_answerK
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONSTELLATION_ANSWERRtype
answer (Ranswer
index (Rindex"�
npt_player_constellation_changeK
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONSTELLATION_CHANGERtype
month (Rmonth
day (Rday"�
npt_player_constellation_infoI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONSTELLATION_INFORtype
month (Rmonth
day (Rday
ret (Rret(
next_change_time (RnextChangeTime";
CONSTELLATION_INFO_RET
RET_SUCCESS 
RET_COOLDOWN"�
 npt_corps_server_battle_swap_posL
type (2.PB.NET_PROTOCBUF_TYPE: NPT_CORPS_SERVER_BATTLE_SWAP_POSRtype
	from_city (RfromCity
from_pos (RfromPos
to_city (RtoCity
to_pos (RtoPos"�
#npt_corps_server_battle_swap_pos_reO
type (2.PB.NET_PROTOCBUF_TYPE:#NPT_CORPS_SERVER_BATTLE_SWAP_POS_RERtype
ret (Rret"�
npt_corps_center_battle_beginI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_CENTER_BATTLE_BEGINRtype
battle_type (R
battleType"}
npt_forbid_stranger_chatD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FORBID_STRANGER_CHATRtype
	is_forbid (RisForbid"v
npt_gm_shutdown_serverB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GM_SHUTDOWN_SERVERRtype
speakid (Rspeakid"�
rename_inform_data_t
roleid (Rroleid
rolename (Rrolename
reason (Rreason!
old_rolename (RoldRolename"�
npt_rename_inform=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_RENAME_INFORMRtypeF
rename_inform_data (2.PB.rename_inform_data_tRrenameInformData"s
npt_get_global_data?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_GLOBAL_DATARtype
	data_type (RdataType"X
global_data_element
key (Rkey
	data_type (RdataType
data (Rdata"�
npt_get_global_data_reB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_GLOBAL_DATA_RERtype:
global_datas (2.PB.global_data_elementRglobalDatas
all_send (RallSend"�
npt_global_data_sync_playerG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GLOBAL_DATA_SYNC_PLAYERRtype4
	sync_data (2.PB.global_data_elementRsyncData"�
npt_parading_begin_notifyE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PARADING_BEGIN_NOTIFYRtype
	applicant (R	applicant
spouse (Rspouse
	mirror_id (RmirrorId
	scene_tag (RsceneTag
parade_type (R
paradeType"�
npt_wedding_begin_notifyD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_WEDDING_BEGIN_NOTIFYRtype
retcode (Rretcode
	applicant (R	applicant
target (Rtarget
	scene_tag (RsceneTag
	mirror_id (RmirrorId"v
npt_select_sect_titleA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SELECT_SECT_TITLERtype
disciple (Rdisciple"�
npt_inform_sect_info@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_INFORM_SECT_INFORtype
mentor (Rmentor
	disciples (R	disciples!
new_disciple (RnewDisciple

new_mentor (R	newMentor"�
npt_sect_apply_requestB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SECT_APPLY_REQUESTRtype
snsinfo (Rsnsinfo
mentor (Rmentor
disciple (Rdisciple
level (Rlevel"�
npt_sect_invite_requestC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SECT_INVITE_REQUESTRtype
snsinfo (Rsnsinfo
mentor (Rmentor
disciple (Rdisciple
level (Rlevel"�
npt_arena_group_inviteB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_GROUP_INVITERtype
name (Rname-

arenagroup (2.PB.name_ruidR
arenagroup"z
npt_sect_quit9
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SECT_QUITRtype
roleid (Rroleid
mentor (Rmentor"�
npt_sect_expel:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SECT_EXPELRtype
roleid (Rroleid
disciple (Rdisciple"v
npt_check_sect_graduateC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CHECK_SECT_GRADUATERtype
roleid (Rroleid"�
npt_check_sect_graduate_resultJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CHECK_SECT_GRADUATE_RESULTRtype
level_valid (R
levelValid

time_valid (R	timeValid
amity_valid (R
amityValid"�
npt_check_sect_premiseB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CHECK_SECT_PREMISERtype
master (Rmaster
disciple (Rdisciple"�
npt_check_sect_premise_resultI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CHECK_SECT_PREMISE_RESULTRtype
	no_master (RnoMaster
	dis_count (RdisCount

sect_amity (R	sectAmity#
sect_cooldown (RsectCooldown"�
npt_get_tp_addon<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_TP_ADDONRtype
tp_id (RtpId
roleid (Rroleid
rank (Rrank"�
npt_get_tp_addon_resultC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_TP_ADDON_RESULTRtype
tp_id (RtpId
roleid (Rroleid
addon (Raddon
rank (Rrank"�
battle_player_info
roleid (Rroleid
level (Rlevel
prof (Rprof
fight (Rfight
idphoto (Ridphoto
name (Rname"�
npt_revenge_battle_resultE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REVENGE_BATTLE_RESULTRtype
is_draw (RisDraw.
winner (2.PB.battle_player_infoRwinner,
loser (2.PB.battle_player_infoRloser"@
npt_test4
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TESTRtype"�
npt_response8
type (2.PB.NET_PROTOCBUF_TYPE:NPT_RESPONSERtype
retcode (Rretcode!
request_type (RrequestType
param1 (Rparam1
param2 (Rparam2
param3 (Rparam3"�
npt_group_op8
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GROUP_OPRtype.
op (2.PB.npt_group_op.GROUP_OP_TYPERop
src_id (RsrcId
src_name (RsrcName
dst_id (RdstId
group_id (RgroupId
name (Rname
	manifesto (R	manifesto
msg	 (Rmsg
	blacklist
 (R	blacklist"�
GROUP_OP_TYPE
GROUP_CREATE
GROUP_DISMISS
GROUP_INVITE

GROUP_QUIT

GROUP_CHAT
GROUP_MANIFESTO
GROUP_PROFILE
GROUP_MEMBER_LIST

GROUP_LIST	

GROUP_NAME

GROUP_KICKOUT
GROUP_LEAVE_MESSAGE"�
npt_group_notify<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GROUP_NOTIFYRtype>
notify (2&.PB.npt_group_notify.GROUP_NOTIFY_TYPERnotify
group_id (RgroupId+
profile (2.PB.group_profileRprofile6
member_list (2.PB.group_member_dataR
memberList

group_list (R	groupList
src_id (RsrcId
dst_id (RdstId"�
GROUP_NOTIFY_TYPE
GROUP_PROFILE
GROUP_MEMBER_LIST

GROUP_JOIN
GROUP_KICKOUT

GROUP_QUIT

GROUP_NAME
GROUP_MANIFESTO

GROUP_LIST
GROUP_DISMISS	
GROUP_CREATE
"�
corps_runner_status
idphoto (Ridphoto
pos (Rpos
speed (Rspeed
name (Rname
counter (Rcounter"�
npt_corps_race_randomeventF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_RANDOMEVENTRtypeL
	eventtype (2..PB.npt_corps_race_randomevent.RACE_EVENT_TYPER	eventtype!
runner_index (RrunnerIndex
eventid (Reventid"O
RACE_EVENT_TYPE
EVENTTYPE_RUNNER 
EVENTTYPE_JUDGE
EVENTTYPE_NPC"L
corps_race_runner_pos!
runner_index (RrunnerIndex
pos (Rpos"�
npt_corps_race_runnerposE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_RUNNER_POSRtype3
posinfo (2.PB.corps_race_runner_posRposinfo"�
npt_corpsrace_get_awardD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_GET_AWARDRtype
corpsid (Rcorpsid
roleid (Rroleid"
npt_corpsrace_get_award_retH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_GET_AWARD_RETRtype
result (Rresult"h
corps_race_champaign_info%
race_timestamp (RraceTimestamp$
champaignname (Rchampaignname"�
npt_corpsrace_get_historyP
type (2.PB.NET_PROTOCBUF_TYPE:$NPT_CORPS_RACE_GET_CHAMPAIGN_HISTORYRtype
corpsid (Rcorpsid
roleid (Rroleid"�
npt_corpsrace_get_history_retT
type (2.PB.NET_PROTOCBUF_TYPE:(NPT_CORPS_RACE_GET_CHAMPAIGN_HISTORY_RETRtypeH
champaign_record (2.PB.corps_race_champaign_infoRchampaignRecord"�
npt_corps_race_statusA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_STATUSRtype=
status (2%.PB.npt_corps_race_status.RACE_STATUSRstatus
champion (Rchampion
	timestamp (R	timestamp1
runners (2.PB.corps_runner_statusRrunners
judgeids (Rjudgeids"G
RACE_STATUS

RS_PREPARE
RS_START

RS_ARRIVED

RS_END"�
npt_corps_race_cheer@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_CHEERRtype
retcode (Rretcode
id (Rid
roleid (Rroleid
choice (Rchoice"�
npt_corps_race_boos?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_RACE_BOOSRtype
retcode (Rretcode
id (Rid
roleid (Rroleid
choice (Rchoice"�
npt_corps_get_race_infoC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_GET_RACE_INFORtype
retcode (Rretcode
id (Rid
roleid (Rroleid
choice (Rchoice
	timestamp (R	timestamp
status (Rstatus
champion (Rchampion1
runners	 (2.PB.corps_runner_statusRrunners
judgeids
 (Rjudgeids
hasaward (Rhasaward
	awardtype (R	awardtype"@
corps_fighter
id (Rid
fighter_pos (R
fighterPos"n
corps_race_bet_award_detail
task_id (RtaskId
bet_type (RbetType
	bet_award (RbetAward"�
corps_race_bet_award
roleid (Rroleid
isvalid (:0Risvalid
	timestamp (R	timestampA
detailaward (2.PB.corps_race_bet_award_detailRdetailaward"�
corps_member_info
roleid (Rroleid
pos (:0Rpos
	join_time (RjoinTime$
lastweek_pos (:0RlastweekPos'
contri_cumulate (RcontriCumulate&
contri_cur_week (RcontriCurWeek

leave_time (R	leaveTime*
online_time_today (RonlineTimeToday(
boss_feed_reward	 (RbossFeedReward
elixir
 (Relixir$
fightcapacity (Rfightcapacity$

merge_flag (:falseR	mergeFlag"
fighter_pos (:0R
fighterPos
level (RlevelC
corpsrace_reward (2.PB.corps_race_bet_awardRcorpsraceRewardA
spec_pos (2.PB.CORPS_SPECIAL_POSITION:
CSP_NORMALRspecPos"I
simple_list
key (Rkey
value (Rvalue
name (Rname"G
	corp_repu
key (Rkey
value (Rvalue
time (Rtime"4
player_id_name
id (Rid
name (Rname"�
corp_chariot
level (Rlevel9
chariots (2.PB.corp_chariot.chariot_infoRchariots�
chariot_info
exp (Rexp*
driver (2.PB.player_id_nameRdriver%
rent_timestamp (RrentTimestamp
level (Rlevel
tid (Rtid

is_upgrade (R	isUpgrade"�
corp_attributeA

skill_room (2".PB.corp_attribute.corp_skill_roomR	skillRoom 

shop_level (:1R	shopLevel*
	corp_repu (2.PB.corp_repuRcorpRepu;
boss_feed_toplist (2.PB.simple_listRbossFeedToplist*
chariot (2.PB.corp_chariotRchariot)
bonus_last_week (:0RbonusLastWeek&
coffers_level (:1RcoffersLevel
pos_type (:0RposType,
set_pos_type_time	 (:0RsetPosTypeTime$
support_side
 (:0RsupportSide4
set_support_side_time (:0RsetSupportSideTime 

misc_index (:0R	miscIndex(
skill_room_level (RskillRoomLevel"
group_open_id (RgroupOpenId2
corp_skill_room
addon_level (R
addonLevel"�
npt_corps_appoint=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_APPOINTRtype
dst (Rdst6
op (2&.PB.npt_corps_appoint.CORPS_APPOINT_OPRop
pos (Rpos
announce (Rannounce
srcname (Rsrcname
dstname (Rdstname
src (Rsrc
	corpsname	 (R	corpsname-
member
 (2.PB.corps_member_infoRmember!
contri_money (RcontriMoney
	cur_money (RcurMoney
skill_level (R
skillLevel
skill_index (R
skillIndex
corps_id (RcorpsId&
attr (2.PB.corp_attributeRattr
tid (Rtid
index (Rindex
money (Rmoney#
online_member (RonlineMember
src_pos (RsrcPos
bonus (Rbonus
race_choice (R
raceChoice
	bet_index (RbetIndex-
fighters (2.PB.corps_fighterRfighters 
recruitment (Rrecruitment
badge (Rbadge!
support_side (RsupportSide"
group_open_id (RgroupOpenId"�
CORPS_APPOINT_OP
	CORPS_ADD
	CORPS_DEL
CORPS_APPOINT
CORPS_DISMISS

CORPS_QUIT
CORPS_ANNOUNCE
CORPS_UPGRADE
CORPS_ABDICATE
CORPS_ACTIVITY	
CORPS_INVITE

CORPS_REJECT
CORPS_AGREE
CORPS_GETDATA
CORPS_BRIEF
CORPS_CONTRI
CORPS_UPGRADE_SKILL
CORPS_UPGRADE_SHOP
CORPS_UPGRADE_SKILL2

CORPS_ATTR
CORPS_SUMMON

CORPS_FIRE
CORPS_CHARIOT
CORPS_UP_CHA
CORPS_RECL_CHA
CORPS_INNER_DEL
CORPS_INNER_APPOINT
CORPS_INNER_ABDICATE
CORPS_UPGRADE_COFFERS
CORPS_SET_MANIFESTO
CORPS_BEGIN_RACE
CORPS_GUESS_RACE
CORPS_SET_FIGHTER 
CORPS_SET_POS_TYPE!
CORPS_SET_RECRUITMENT"
CORPS_SET_SUPPORT_SIDE#
CORPS_SET_BADGE$
CORPS_INVITE_APPLY%
CORPS_GET_MANAGER_BONUS&
CORPS_GROUP_OPEN_ID'
CORPS_SPECIAL_APPOINT("�
corps_apply_t
roleid (Rroleid
name (Rname

apply_time (R	applyTime
level (Rlevel
prof (Rprof"�
npt_apply_corps;
type (2.PB.NET_PROTOCBUF_TYPE:NPT_APPLY_CORPSRtype
id (Rid'
apply (2.PB.corps_apply_tRapply"�
corps_brief
id (Rid
name (Rname3
master_info (2.PB.player_id_nameR
masterInfo
activity (Ractivity!
member_count (RmemberCount
level (Rlevel"�
corps_merge_t>

merge_type (2.PB.MERGE_TYPE:MERGE_TYPE_NULLR	mergeType-

merge_role (2.PB.MERGE_ROLER	mergeRole
	applytime (R	applytime.

corps_info (2.PB.corps_briefR	corpsInfo
	mergetime (R	mergetime
retcode (Rretcode"�
npt_corps_merge_op>
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_MERGE_OPRtype5
op (2%.PB.npt_corps_merge_op.CORPS_MERGE_OPRop0

merge_info (2.PB.corps_merge_tR	mergeInfo
corps_id (RcorpsId"�
CORPS_MERGE_OP
CORPS_MERGE_REQ
CORPS_MERGE_RPL
CORPS_MERGE_CSL
CORPS_MERGE_RECV_REQ
CORPS_MERGE_RECV_RPL
CORPS_MERGE_NOTIFY
CORPS_MERGE_CANCEL_NOTIFY"�
npt_fake_auction_listA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FAKE_AUCTION_LISTRtype
category (Rcategory
	min_level (RminLevel
	max_level (RmaxLevel
pageid (Rpageid
itemname (Ritemname!
quality_type (RqualityType
search_type (R
searchType"�
npt_fake_auction_sellinfo
	auctionid (R	auctionid
itemid (Ritemid
price (Rprice
count (Rcount
category (Rcategory"�
npt_fake_auction_list_resultH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FAKE_AUCTION_LIST_RESULTRtype
retcode (Rretcode
pageid (Rpageid!
total_amount (RtotalAmount3
items (2.PB.npt_fake_auction_sellinfoRitems!
refresh_time (RrefreshTime
	show_self (RshowSelf"�
npt_fake_auction_priceB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FAKE_AUCTION_PRICERtype
itemid (Ritemid!
auction_type (RauctionType"�
npt_fake_auction_price_resultI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FAKE_AUCTION_PRICE_RESULTRtype
retcode (Rretcode
itemid (Ritemid
price (Rprice!
auction_type (RauctionType"�
group_profile
id (Rid
creator (Rcreator
name (Rname
	manifesto (R	manifesto
idphotos (Ridphotos
member_size (R
memberSize
online_size (R
onlineSize"*
group_member_common_data
id (Rid"
group_member_data0
data (2.PB.group_member_common_dataRdata
	join_time (RjoinTime
	is_online (RisOnline"x
group_struct_data+
profile (2.PB.group_profileRprofile6
member_list (2.PB.group_member_dataR
memberList"�
corps_history;
event (2%.PB.corps_history.CORPS_HISTORY_EVENTRevent
	timestamp (R	timestamp
roleid_1 (Rroleid1
roleid_2 (Rroleid2
addon (Raddon"�
CORPS_HISTORY_EVENT
CORPS_HISTORY_JOIN
CORPS_HISTORY_QUIT
CORPS_HISTORY_APPOINT
CORPS_HISTORY_CREATE
CORPS_HISTORY_MAINTENANCE
CORPS_HISTORY_UPGRADE
CORPS_HISTORY_MERGE
CORPS_HISTORY_SET_POS_TYPE
CORPS_HISTORY_RENAME	
CORPS_HISTORY_BADGE

CORPS_HISTORY_SUPPORT_SIDE!
CORPS_HISTORY_SPECIAL_APPOINT"�
corps_manifesto
	timestamp (R	timestamp
rolename (Rrolename
content (Rcontente
manifesto_type (2'.PB.corps_manifesto.CORPS_MANIFEST_TYPE:CORPS_MANIFEST_NORMALRmanifestoType
pos (Rpos
idphoto (Ridphoto"J
CORPS_MANIFEST_TYPE
CORPS_MANIFEST_NORMAL
CORPS_MANIFEST_MERGE"�
corps_activ_contri_info
roleid (Rroleid%
week_timestamp (RweekTimestamp'
activity_contri (RactivityContri"�

corps_struct_data
id (Rid
name (Rname
create_time (R
createTime
level (Rlevel
status (Rstatus
announce (Rannounce&
announce_time (:0RannounceTime
contri (:0Rcontri*
contri_cumulate	 (:0RcontriCumulate
money
 (:0Rmoney'
apply (2.PB.corps_apply_tRapply(
last_resume_time (RlastResumeTime&
attr (2.PB.corp_attributeRattr

corp_power (R	corpPower

login_time (R	loginTime
bonus (:0Rbonus)
bonus_last_week (:0RbonusLastWeek/
	histories (2.PB.corps_historyR	histories3

manifestos (2.PB.corps_manifestoR
manifestos

merge_time (R	mergeTime;
recv_merge_apply (2.PB.corps_merge_tRrecvMergeApply;
send_merge_apply (2.PB.corps_merge_tRsendMergeApply=
ready_merge_apply (2.PB.corps_merge_tRreadyMergeApply=
reply_merge_apply (2.PB.corps_merge_tRreplyMergeApply/
members (2.PB.corps_member_infoRmembers
pos_type (:0RposType,
set_pos_type_time (:0RsetPosTypeTime 
recruitment (Rrecruitment
badge (Rbadge'
set_badge_time  (:0RsetBadgeTimeC
week_contri_set! (2.PB.corps_activ_contri_infoRweekContriSetS
race_champaign_records" (2.PB.corps_race_champaign_infoRraceChampaignRecords"�
npt_corps_data:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_DATARtype)
data (2.PB.corps_struct_dataRdata/
members (2.PB.corps_member_infoRmembers#
members_count (RmembersCount)
apprentice_count (RapprenticeCount!
online_count (RonlineCount"�
npt_corps_history=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_HISTORYRtype
id (Rid+
history (2.PB.corps_historyRhistory"�
npt_corps_manifesto?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CORPS_MANIFESTORtype
id (Rid1
	manifesto (2.PB.corps_manifestoR	manifesto"x
npt_get_blessing_historyD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_BLESSING_HISTORYRtype
roleid (Rroleid"�
blessing_history_t
roleid (Rroleid
count (Rcount
	timestamp (R	timestamp"
client_cfg_id (RclientCfgId"�
npt_blessing_history_resultG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_BLESSING_HISTORY_RESULTRtype.
result (2.PB.blessing_history_tRresult"�
npt_get_amity_info>
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_AMITY_INFORtype
roleid (Rroleid
friendid (Rfriendid"�
$npt_notify_client_corps_battle_beginP
type (2.PB.NET_PROTOCBUF_TYPE:$NPT_NOTIFY_CLIENT_CORPS_BATTLE_BEGINRtype

battle_tid (R	battleTid
	battle_id (RbattleId"�
npt_player_enter_corps_battleI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_ENTER_CORPS_BATTLERtype
battle_type (R
battleType
param1 (:0Rparam1
param2 (:0Rparam2"�
$npt_player_enter_corps_battle_resultP
type (2.PB.NET_PROTOCBUF_TYPE:$NPT_PLAYER_ENTER_CORPS_BATTLE_RESULTRtype
battle_type (R
battleType
result (Rresult"�
#npt_player_search_corps_battle_listO
type (2.PB.NET_PROTOCBUF_TYPE:#NPT_PLAYER_SEARCH_CORPS_BATTLE_LISTRtype
battle_type (R
battleType*
battle_begin_time (RbattleBeginTime
corps_id (RcorpsId
page (Rpage^
search_battle_type (2#.PB.SEARCH_CORPS_SERVER_BATTLE_TYPE:SCSBT_BRIEFRsearchBattleType-
search_city_index (:0RsearchCityIndex"�
corps_city_battle_player
roleid (Rroleid
name (Rname!

profession (:0R
profession
level (:0Rlevel
index (:0Rindex
star (:0Rstar!
succeed_name (RsucceedName"�
corps_tower_battle_player
roleid (Rroleid
name (Rname
tower_level (R
towerLevel
corps_index (R
corpsIndex
score (Rscore!
obtain_award (RobtainAward'
challange_count (RchallangeCount"�
corps_battle_record!
target_index (RtargetIndex
self_id (:0RselfId
	self_name (RselfName 

self_level (:0R	selfLevel
	target_id (:0RtargetId
target_name (R
targetName$
target_level (:0RtargetLevel
battle_time (R
battleTime
result	 (:-1RresultB
city_players_1
 (2.PB.corps_city_battle_playerRcityPlayers1B
city_players_2 (2.PB.corps_city_battle_playerRcityPlayers2B
tower_players (2.PB.corps_tower_battle_playerRtowerPlayers:
corps_tower_battle_score_1 (RcorpsTowerBattleScore1:
corps_tower_battle_score_2 (RcorpsTowerBattleScore2"�
#corps_server_battle_change_pos_info$
src_city_index (RsrcCityIndex 
src_city_pos (R
srcCityPos$
dst_city_index (RdstCityIndex 
dst_city_pos (R
dstCityPos
name (Rname
	timestamp (R	timestamp

be_changed (R	beChanged"�
corps_server_battle_city_player
roleid (Rroleid
name (Rname
level (Rlevel*
fightingcapacity (RfightingcapacityQ
change_pos_infos (2'.PB.corps_server_battle_change_pos_infoRchangePosInfos"�
corps_server_battle_city=
players (2#.PB.corps_server_battle_city_playerRplayers#
battle_time (:-1R
battleTime
result (:-1Rresult"�
corps_server_battle_city_briefA
	castellan (2#.PB.corps_server_battle_city_playerR	castellan0
castellan_corps_name (RcastellanCorpsName
battle_time (R
battleTime#
battle_result (RbattleResult"�
*npt_player_search_corps_battle_list_resultV
type (2.PB.NET_PROTOCBUF_TYPE:*NPT_PLAYER_SEARCH_CORPS_BATTLE_LIST_RESULTRtype
battle_type (R
battleType
page (Rpage!
battle_level (RbattleLevel
total_count (R
totalCount
	win_count (RwinCount*
battle_begin_time (RbattleBeginTime1
records (2.PB.corps_battle_recordRrecords-
battle_apply_time	 (:0RbattleApplyTime8
brief
 (2".PB.corps_server_battle_city_briefRbriefQ
change_pos_infos (2'.PB.corps_server_battle_change_pos_infoRchangePosInfos*
self_city_index (:-1RselfCityIndex(
self_pos_index (:-1RselfPosIndex`
search_battle_brief (2#.PB.SEARCH_CORPS_SERVER_BATTLE_TYPE:SCSBT_BRIEFRsearchBattleBrief-
search_city_index (:0RsearchCityIndex=
players (2#.PB.corps_server_battle_city_playerRplayers#
target_zoneid (RtargetZoneid.
server_battle_state (RserverBattleState9
server_battle_award_state (RserverBattleAwardState"�
npt_server_level_notifyC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SERVER_LEVEL_NOTIFYRtype!
server_level (RserverLevel"r
npt_use_gift_card=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_USE_GIFT_CARDRtype

cardnumber (R
cardnumber"�
npt_use_gift_card_resultD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_USE_GIFT_CARD_RESULTRtype

cardnumber (R
cardnumber
retcode (Rretcode"�
npt_common_search=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_COMMON_SEARCHRtype
pos (Rpos
len (RlenC
	corps_key (2&.PB.npt_common_search.search_corps_keyRcorpsKey
retcode (Rretcode
total_count (R
totalCount(
corps (2.PB.npt_corps_dataRcorpsF

player_key	 (2'.PB.npt_common_search.search_player_keyR	playerKey'
players
 (2.PB.name_ruidRplayers
search_time (R
searchTimeF

player_ret (2'.PB.npt_common_search.search_player_retR	playerRet�
search_corps_key4
key_type (2.PB.SEARCH_CORPS_KEY_TYPERkeyType
name (Rname"
	can_merge (:falseRcanMergeD
search_player_key
name (Rname
	is_online (RisOnline�
search_player_ret
roleid (Rroleid
name (Rname
gender (Rgender
idphoto (Ridphoto

profession (R
profession
level (Rlevel"�
zhaojiling_info
src (Rsrc
src_name (RsrcName
reason (Rreason

delay_time (R	delayTime
	scene_tag (RsceneTag
pos (2.PB.a3d_posRpos#
random_radius (RrandomRadius#
zhaojiling_id (RzhaojilingId*
src_corp_position	 (RsrcCorpPosition.
src_nation_position
 (RsrcNationPosition
	mirror_id (RmirrorId3
type (2.PB.zhaojiling_info.ZHAOJI_TYPERtype
param1 (Rparam1
inst_tid (RinstTid"Z
ZHAOJI_TYPE
ZT_ITEM 
ZT_NATION_WAR_START
ZT_NATION_WAR_ROLE
	ZT_SUCCOR"}
npt_zhaojiling:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ZHAOJILINGRtype/
baseinfo (2.PB.zhaojiling_infoRbaseinfo"v
ipt_mirror_info
mirrorid (Rmirrorid&
state (2.PB.MIRROR_STATERstate
create_time (R
createTime"�
npt_sync_mirror_info@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SYNC_MIRROR_INFORtype)
infos (2.PB.ipt_mirror_infoRinfos

state_mask (R	stateMask"E
MIRROR_STATE_MASK
MS_FORBIT_MIRROR_STATE
MS_IN_NATION_WAR"�
npt_sync_nation_war_infoD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SYNC_NATION_WAR_INFORtype 
war_end_time (R
warEndTimeE
npc_info (2*.PB.npt_sync_nation_war_info.nwar_npc_infoRnpcInfo8
wangcheng_revive_faction (RwangchengReviveFactiony
nwar_npc_info
npc_tid (RnpcTid

npc_nation (R	npcNation
npc_hp (RnpcHp
npc_born (RnpcBorn"�
npt_get_corp_name=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_CORP_NAMERtype
corp_id (RcorpId
name (Rname
badge (Rbadge!
support_side (RsupportSide

misc_index (R	miscIndex"�
npt_nation_war_operateB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_WAR_OPERATERtype
	oper_type (RoperType
oper_param1 (R
operParam1"}
npt_nation_war_operate_infoG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_WAR_OPERATE_INFORtype
war_id (RwarId"�
npt_nation_war_operate_info_reJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_WAR_OPERATE_INFO_RERtype#
attack_zhaoji (RattackZhaoji#
defend_zhaoji (RdefendZhaoji#
attack_yanshi (RattackYanshi#
defend_jiaxue (RdefendJiaxue!
attack_jihuo (RattackJihuo!
defend_jihuo (RdefendJihuo%
quick_transfer (RquickTransfer"�
npt_team_invite;
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_INVITERtype
name (Rname
assign_rule (R
assignRule

auto_admit (R	autoAdmit
open_invite (R
openInvite
level (Rlevel

profession (R
profession
lineid (Rlineid#
center_zoneid	 (RcenterZoneid"�
npt_friend_invite=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FRIEND_INVITERtype
src_name (RsrcName
	src_level (RsrcLevel%
src_profession (RsrcProfession 
src_group_id (R
srcGroupId

src_gender (R	srcGender
src_idphoto (R
srcIdphoto
linksid (Rlinksid
dst_name	 (RdstName"�
npt_team_info9
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_INFORtype
teamids (Rteamids1
teams (2.PB.npt_team_info.team_infoRteams�
	team_info
teamid (Rteamid
leader (Rleader
name (Rname
level (Rlevel
capacity (Rcapacity
prof (Rprof
members (Rmembers"�
npt_platform_registerA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_REGISTERRtype
roleid (Rroleid
category (Rcategory
tid (Rtid
teamid (Rteamid
	min_level (RminLevel
	max_level (RmaxLevel"�
npt_platform_register_resultH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_REGISTER_RESULTRtype
retcode (Rretcode
roleid (Rroleid
category (Rcategory
tid (Rtid
teamid (Rteamid
errrole (Rerrrole
param1 (Rparam1"�
platformmember_info
roleid (Rroleid
name (Rname
level (Rlevel
gender (Rgender
idphoto (Ridphoto

profession (R
profession"�
npt_platform_team_inviteD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_TEAM_INVITERtype
roleid (Rroleid
category (Rcategory
tid (Rtid

waitteamid (R
waitteamid
timeout (Rtimeout+
team (2.PB.platformmember_infoRteam"�
npt_platform_invite_statusF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_INVITE_STATUSRtype
roleid (Rroleid
category (Rcategory
tid (Rtid

waitteamid (R
waitteamid
accept_list (R
acceptList
	wait_list (RwaitList"�
npt_platform_close_notifyE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_CLOSE_NOTIFYRtype
roleid (Rroleid
category (Rcategory
tid (Rtid"�
npt_platform_reject_notifyF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_REJECT_NOTIFYRtype
roleid (Rroleid
category (Rcategory
tid (Rtid

waitteamid (R
waitteamid"�
npt_platform_cancel?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_CANCELRtype
roleid (Rroleid
category (Rcategory
tid (Rtid
teamid (Rteamid"�
npt_platform_cancel_resultF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_CANCEL_RESULTRtype
retcode (Rretcode
category (Rcategory
tid (Rtid"�
npt_platform_rejoin_notifyF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_REJOIN_NOTIFYRtype
roleid (Rroleid
teamid (Rteamid
category (Rcategory
tid (Rtid"|
npt_platform_register_infoF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_REGISTER_INFORtype
roleid (Rroleid"=
register_info
category (Rcategory
tid (Rtid"�
npt_platform_reginfo_resultG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_REGINFO_RESULTRtype
roleid (Rroleid-
infolist (2.PB.register_infoRinfolist"�
npt_platform_success_notifyG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_SUCCESS_NOTIFYRtype
leader (Rleader
teamid (Rteamid
category (Rcategory
tid (Rtid"�
npt_platform_get_team_listF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_GET_TEAM_LISTRtype
roleid (Rroleid
category (Rcategory
tid (Rtid"�
platform_team_info,
leader (2.PB.name_roleid_pairRleader!
leader_level (RleaderLevel
leader_prof (R
leaderProf
teamid (Rteamid
cur_size (RcurSize
max_size (RmaxSize
	min_level (RminLevel
	max_level (RmaxLevel
category	 (Rcategory
tid
 (RtidE
members (2+.PB.platform_team_info.platform_team_memberRmembersX
platform_team_member
roleid (Rroleid
level (Rlevel
prof (Rprof"�
npt_platform_team_listB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_TEAM_LISTRtype
roleid (Rroleid
category (Rcategory
tid (Rtid

team_count (R	teamCount)
individual_count (RindividualCount3
	team_list (2.PB.platform_team_infoRteamList"�
npt_platform_join_teamB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_JOIN_TEAMRtype
roleid (Rroleid
category (Rcategory
tid (Rtid
teamid (Rteamid"�
npt_platform_join_team_resultI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_JOIN_TEAM_RESULTRtype
retcode (Rretcode
category (Rcategory
tid (Rtid
teamid (Rteamid"�
npt_platform_add_member_notifyJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_ADD_MEMBER_NOTIFYRtype,
member (2.PB.name_roleid_pairRmember
cur_size (RcurSize"�
npt_platform_team_reach_minG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_TEAM_REACH_MINRtype
category (Rcategory
tid (Rtid"�
npt_platform_join_team_notifyI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_JOIN_TEAM_NOTIFYRtype
teamid (Rteamid
category (Rcategory
tid (Rtid"o
npt_arena_apply;
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_APPLYRtype
battle_type (R
battleType"�
npt_arena_apply_resultB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_APPLY_RESULTRtype
battle_type (R
battleType
roleid (Rroleid
result (Rresult"�
match_player_info(
role (2.PB.name_roleid_pairRrole
score (Rscore
prof (Rprof
gender (Rgender
idphoto (Ridphoto
level (Rlevel!
support_side (RsupportSide"�
npt_arena_match_resultB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_MATCH_RESULTRtype
battle_type (R
battleType)
info (2.PB.match_player_infoRinfo
result (Rresult"m
npt_arena_quit:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_QUITRtype
battle_type (R
battleType"�
npt_arena_quit_resultA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_QUIT_RESULTRtype
battle_type (R
battleType
result (Rresult"�
npt_arena_search_listA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_SEARCH_LISTRtype
battle_type (R
battleType
page (Rpage"�
arena_search_info(
role (2.PB.name_roleid_pairRrole
idphoto (:0Ridphoto!

profession (:0R
profession
rank (:0Rrank$
support_side (:0RsupportSide
score (:0Rscore"
total_count (:0R
totalCount
	win_count (:0RwinCount5
consecutive_win_count	 (:0RconsecutiveWinCount9
max_consecutive_win_count
 (RmaxConsecutiveWinCount,
total_count_reward (RtotalCountReward(
win_count_reward (RwinCountReward?
consecutive_win_count_reward (RconsecutiveWinCountReward
rank_reward (R
rankReward"�
npt_arena_search_list_resultH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_SEARCH_LIST_RESULTRtype
battle_type (R
battleType.
my_info (2.PB.arena_search_infoRmyInfo)
list (2.PB.arena_search_infoRlist"�
npt_arena_get_award?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_GET_AWARDRtype
battle_type (R
battleType3

award_type (2.PB.ARENA_AWARD_TYPER	awardType
count (Rcount"�
npt_arena_get_award_resultF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ARENA_GET_AWARD_RESULTRtype
battle_type (R
battleType3

award_type (2.PB.ARENA_AWARD_TYPER	awardType
count (Rcount
result (Rresult"{
npt_player_contest_beginD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONTEST_BEGINRtype
sub_type (RsubType"�
npt_player_contset_begin_resultK
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONTEST_BEGIN_RESULTRtype
retcode (Rretcode
sub_type (RsubType"^
npt_player_contest_exitC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONTEST_EXITRtype"�
npt_player_contest_answerE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONTEST_ANSWERRtype

questionid (R
questionid
answer (Ranswer"�
npt_player_contest_for_helpG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONTEST_FOR_HELPRtype
retcode (Rretcode

questionid (R
questionid%
update_counter (RupdateCounter"n
npt_player_contest_faction_helpK
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_CONTEST_FACTION_HELPRtype"�
&npt_player_contest_faction_help_resultR
type (2.PB.NET_PROTOCBUF_TYPE:&NPT_PLAYER_CONTEST_FACTION_HELP_RESULTRtype
retcode (Rretcode"�
npt_faction_task_callfor_helpI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FACTION_TASK_CALLFOR_HELPRtype
roleid (Rroleid
taskid (Rtaskid
	task_type (RtaskType"5
	TASK_TYPE
TASK_TYPE_NORMAL 
TASK_TYPE_TEAM"�
npt_faction_task_help_msgE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FACTION_TASK_HELP_MSGRtype
roleid (Rroleid
name (Rname
taskid (Rtaskid"�
npt_platform_team_reach_maxG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLATFORM_TEAM_REACH_MAXRtype
category (Rcategory
tid (Rtid"�
facebook_str
id (:0Rid
pos (Rpos
param (Rparam
name (Rname
name2 (Rname2(
leave_corps_time (RleaveCorpsTime
level (Rlevel$

merge_flag (:falseR	mergeFlag
spec_pos	 (:0RspecPos"�
other_player_profile1
pb_facebook (2.PB.facebook_strR
pbFacebook
nation (Rnation

nation_pos (R	nationPos

duke_level (R	dukeLevel
fashion (Rfashion
faceid (Rfaceid
	body_size (RbodySize
hair (Rhair
clothes	 (Rclothes
weapon
 (Rweapon
spouse_name (R
spouseName+
star_gfx_suit_star (RstarGfxSuitStar
fashions (Rfashions

appearance (R
appearance"x
other_player_profile_from_gs+
achievement_grade (RachievementGrade+
achievement_count (RachievementCount"�
idip_role_info
money (Rmoney
vip_exp (RvipExp
	vip_level (RvipLevel
role_exp (RroleExp
ride_exp (RrideExp

ride_level (R	rideLevel
pk_value (RpkValue
online_time (R
onlineTime#
friends_count	 (RfriendsCount
	cash_used
 (RcashUsed
	cash_bind (RcashBind",
role_list_custom
fashion (Rfashion"�
role_mutable_data
	vip_level (RvipLevel$
fightcapacity (Rfightcapacity#
show_property (RshowProperty2
pro (2 .PB.other_player_profile_from_gsRpro/
	idip_info (2.PB.idip_role_infoRidipInfo,
custom (2.PB.role_list_customRcustom
hide_vip (RhideVip(
has_hometown (:falseRhasHometown"
hide_vip_name	 (RhideVipName$
bubble_index
 (:0RbubbleIndex 

font_index (:0R	fontIndex
	cur_title (RcurTitle

title_data (R	titleData"v
role_complicated_data_guard,
slots (2.PB.guard_slot_essenceRslots)
data (2.PB.guard_client_dataRdata"�
role_complicated_data_retinue

retinue_id (R	retinueId
quality (Rquality
level (Rlevel%
fight_capacity (RfightCapacity
prop (Rprop#
fashion_index (RfashionIndex
color_index (R
colorIndex"C
role_complicated_data_flysword!
hero (2.PB.hero_infoRhero"�
role_complicated_data_intimateD
data (20.PB.role_complicated_data_intimate.intimate_dataRdataf
intimate_data
targetid (Rtargetid#
intimate_name (RintimateName
value (Rvalue"x
 role_complicated_data_collection
total (Rtotal>
show_collections (2.PB.collection_infoRshowCollections"�
role_complicated_data5
guard (2.PB.role_complicated_data_guardRguard;
retinue (2!.PB.role_complicated_data_retinueRretinue>
flysword (2".PB.role_complicated_data_flyswordRflysword>
intimate (2".PB.role_complicated_data_intimateRintimate4
enhance (2.PB.db_player_enhance_dataRenhanceD

collection (2$.PB.role_complicated_data_collectionR
collection"�
npt_get_player_profileB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_PLAYER_PROFILERtype
roleid (Rroleid+
get_profile_mask (:0RgetProfileMask
snsinfo (Rsnsinfo1
property (2.PB.role_mutable_dataRproperty

equipments (R
equipments0
others (2.PB.other_player_profileRothers
online (Ronline
children	 (RchildrenD
complicated_data
 (2.PB.role_complicated_dataRcomplicatedData"�
GET_PROFILE_MASK
GET_ALLFILE 
GET_SNS
	GET_EQUIP
GET_PROPERTY
GET_CHILDREN
	GET_OTHER
	GET_GUARD 
GET_RETINUE@
GET_INTIMATE�
GET_FLYSWORD�
GET_ENHANCE�
GET_COLLECTION�"�
blessing_info
src (Rsrc
src_name (RsrcName
dst (Rdst
dst_name (RdstName
repu_id (RrepuId

repu_count (R	repuCount
item_tid (RitemTid

item_count (R	itemCount
txnid	 (Rtxnid
result
 (Rresult
msg (Rmsg
	anonymous (R	anonymous
speak_id (RspeakId"
client_cfg_id (RclientCfgId"�
npt_blessing_info=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_BLESSING_INFORtype%
info (2.PB.blessing_infoRinfo

tanksgiver (R
tanksgiver"�
npt_nationwar_event?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATIONWAR_EVENTRtype?

event_type (2 .PB.npt_nationwar_event.NNE_TYPER	eventType
event_param (R
eventParam"Z
NNE_TYPE
NATION_WAR_KILL_COUNT
NATION_WAR_JIHUO
NATION_WAR_JIHUO_COOLDOWN"q
npt_nation_shutup=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_SHUTUPRtype

dst_roleid (R	dstRoleid"�
npt_nation_shutup_infoB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_SHUTUP_INFORtype
	use_times (RuseTimes!
remain_times (RremainTimes"�
npt_nation_commander_appointH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_COMMANDER_APPOINTRtype

dst_roleid (R	dstRoleid"�
npt_nation_commander_notifyG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_COMMANDER_NOTIFYRtype
dst_name (RdstName"�
nation_war_history
finish_time (R
finishTime#
nation_attack (RnationAttack#
nation_defend (RnationDefend
result (Rresult"�
npt_nation_war_historyB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_WAR_HISTORYRtype2
historys (2.PB.nation_war_historyRhistorys"�
npt_request_info<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REQUEST_INFORtypeA
request_type (2.PB.npt_request_info.INFO_TYPERrequestType"#
	INFO_TYPE
NATION_WAR_HISTORY"�
npt_nation_shutup_replyC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NATION_SHUTUP_REPLYRtype?
result (2'.PB.npt_nation_shutup_reply.RESULT_TYPERresult
dst_name (RdstName"P
RESULT_TYPE
RESULT_SUCCEED
RESULT_OFFLINE
RESULT_ALREADY_SHUTUP"�
greeting_info3
type (2.PB.greeting_info.GREETING_TYPERtype
info_seq (:0RinfoSeq
roleid (Rroleid
name (Rname
param_1 (Rparam1
param_2 (Rparam2
param_3 (Rparam3
	greet_tid (RgreetTid"M
GREETING_TYPE
GREETING_INVITE
GREETING_GRANT
GREETING_THANKS"�
npt_greeting_info_syncB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GREETING_INFO_SYNCRtypeA
	sync_type (2$.PB.npt_greeting_info_sync.SYNC_TYPERsyncType6
greeting_list (2.PB.greeting_infoRgreetingList"5
	SYNC_TYPE
SYNC_ALL
SYNC_ADD
SYNC_DEL"�
npt_greeting_reply>
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GREETING_REPLYRtype
info_seq (RinfoSeq

dst_roleid (R	dstRoleid

reply_type (R	replyType
reply_param (R
replyParam"�
ask_help_info
src (Rsrc
src_name (RsrcName
	scene_tag (RsceneTag
	mirror_id (RmirrorId
pos (2.PB.a3d_posRpos#
end_timestamp (RendTimestamp3
	info_type (2.PB.ask_help_info_typeRinfoType;
relation (2.PB.ask_help_info.relation_typeRrelation 
certificate	 (Rcertificate

src_nation
 (R	srcNation
corp_id (RcorpId"<
relation_type
RT_STRANGER 
RT_CORP
	RT_NATION"�
npt_ask_help8
type (2.PB.NET_PROTOCBUF_TYPE:NPT_ASK_HELPRtype%
info (2.PB.ask_help_infoRinfo)
response_retcode (RresponseRetcode"�
npt_send_tencent_secure_infoH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SEND_TENCENT_SECURE_INFORtype
secure_data (R
secureData"q
npt_officer_gift_get@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_OFFICER_GIFT_GETRtype
gift_id (RgiftId"w
npt_officer_gift_notifyC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_OFFICER_GIFT_NOTIFYRtype
gift_id (RgiftId"y
npt_weak_nation_gift_getD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_WEAK_NATION_GIFT_GETRtype
gift_id (RgiftId"
npt_weak_nation_gift_notifyG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_WEAK_NATION_GIFT_NOTIFYRtype
gift_id (RgiftId"�
npt_server_info_notifyB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SERVER_INFO_NOTIFYRtype"
free_war_left (RfreeWarLeft"�
npt_team_recruit_operateD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_RECRUIT_OPERATERtype!
operate_type (RoperateType
param1 (Rparam1
param2 (Rparam2
param3 (Rparam3"{
OPERATE_TYPE

CREATE
LIST
PUBLISH

CANCEL
REQ_COOLDOWN

MODIFY
REQ_LEADER_POS

GATHER"�
recruit_info
	min_level (RminLevel
inst_tid (RinstTid
team_number (R
teamNumber
leader_name (R
leaderName
	leader_id (RleaderId"�
npt_team_recruit_operate_replyJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TEAM_RECRUIT_OPERATE_REPLYRtype!
operate_type (RoperateType%
operate_result (RoperateResult
param1 (Rparam1
param2 (Rparam2
param3 (Rparam33
recruit_list (2.PB.recruit_infoRrecruitList
param4 (Rparam4(
	param_pos	 (2.PB.a3d_posRparamPos"_
RESULT
SUCCEED 
MAX_SIZE
	MIN_LEVEL
COOLDOWN
TIME_OUT
IN_INSTANCE"V
npt_update_grc_info?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_UPDATE_GRC_INFORtype"�
npt_grc_send_gift=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_SEND_GIFTRtype
	to_openid (	RtoOpenid
	gift_type (RgiftType

gift_count (R	giftCount
retcode (Rretcode"�
npt_grc_rcv_gift<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_RCV_GIFTRtype
	gift_type (RgiftType
serialid (Rserialid
retcode (Rretcode

gift_count (R	giftCount#
receive_times (RreceiveTimes"�
npt_grc_rcv_all_gift@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_RCV_ALL_GIFTRtype
	gift_type (RgiftType
retcode (RretcodeF
receive_gift_infos (2.PB.GrcReceivedGiftInfosRreceiveGiftInfos"�
npt_grc_send_all_giftA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_SEND_ALL_GIFTRtype
	gift_type (RgiftType
retcode (Rretcode?
send_gift_infos (2.PB.GrcUserSendGiftInfoRsendGiftInfos"�
'npt_grc_get_user_receive_gift_info_listS
type (2.PB.NET_PROTOCBUF_TYPE:'NPT_GRC_GET_USER_RECEIVE_GIFT_INFO_LISTRtype
account (	Raccount
page_idx (RpageIdx
retcode (Rretcode7
receive_gift_total_count (RreceiveGiftTotalCounta
user_receive_gift_times_infos (2.PB.GrcUserReceiveGiftTimesInfoRuserReceiveGiftTimesInfos^
user_receive_gift_meta_infos (2.PB.GrcUserReceiveGiftMetaInfoRuserReceiveGiftMetaInfosD
receive_gift_infos (2.PB.GrcReceiveGiftInfoRreceiveGiftInfos"�
grc_friend_info
openid (Ropenid
nickname (Rnickname

figure_url (R	figureUrl
roleid (Rroleid
rolename (Rrolename
level (Rlevel
gender (Rgender

profession (R
profession
nation	 (Rnation*
fightingcapacity
 (Rfightingcapacity

sameserver (R
sameserver
vipinfo (Rvipinfo"�
npt_grc_friend_list?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_FRIEND_LISTRtype
retcode (Rretcode,
total_friend_count (RtotalFriendCount+
friend (2.PB.grc_friend_infoRfriend"�
grc_gift_info
	gift_type (RgiftType
from (Rfrom
to (Rto
count (Rcount
flag (Rflag
	timestamp (R	timestamp
serialid (Rserialid
nickname (Rnickname"�
grc_user_gift_info
	gift_type (RgiftType-
giftsend (2.PB.grc_gift_infoRgiftsend+
giftreceive_times (RgiftreceiveTimes3
giftreceive (2.PB.grc_gift_infoRgiftreceive"~
npt_grc_gift_list=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_GIFT_LISTRtype*
gift (2.PB.grc_user_gift_infoRgift"�
npt_grc_send_gift_re@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_SEND_GIFT_RERtype
to (Rto
retcode (Rretcode"�
npt_grc_rcv_gift_re?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_RCV_GIFT_RERtype
from (Rfrom
retcode (Rretcode"�
npt_grc_turn_on_off?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_TURN_ON_OFFRtype
	gift_type (RgiftType
on_off (RonOff
retcode (Rretcode"�
npt_grc_turn_on_off_reB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_TURN_ON_OFF_RERtype
retcode (Rretcode
onoff (Ronoff"�
npt_grc_user_login_infoC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_USER_LOGIN_INFORtype/
qq_vip_infos (2.PB.QQVipInfoR
qqVipInfos,
friend_total_count (RfriendTotalCount)
friends (2.PB.GrcUserInfoRfriendsH
user_send_gift_infos (2.PB.GrcUserSendGiftInfoRuserSendGiftInfosa
user_receive_gift_times_infos (2.PB.GrcUserReceiveGiftTimesInfoRuserReceiveGiftTimesInfos7
receive_gift_total_count (RreceiveGiftTotalCountD
receive_gift_infos	 (2.PB.GrcReceiveGiftInfoRreceiveGiftInfos9
profile_info
 (2.PB.GrcUserProfileInfoRprofileInfo"x
npt_recharge_activity_getE
type (2.PB.NET_PROTOCBUF_TYPE:NPT_RECHARGE_ACTIVITY_GETRtype
param (Rparam"�
npt_recharge_activity_get_reH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_RECHARGE_ACTIVITY_GET_RERtype
param (Rparam#
activity_info (RactivityInfo"m
npt_die_elite_tid=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_DIE_ELITE_TIDRtype
tid_list (RtidList"\
npt_query_die_elite_tidA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_DIE_ELITE_TIDRtype"S
grc_passed_friend_info
nickname (Rnickname

figure_url (R	figureUrl"�
npt_grc_exceed_friend_listF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_EXCEED_FRIEND_LISTRtype
res (Rres

level_type (R	levelType4
friends (2.PB.grc_passed_friend_infoRfriends"�
npt_top_reward:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_TOP_REWARDRtype3
players (2.PB.npt_top_reward.playerRplayers
	self_rank (RselfRank

has_reward (R	hasReward*
activity_end_time (RactivityEndTime

get_reward (R	getReward
tid (Rtid
actived_tid (R
activedTid.
activity_start_time	 (RactivityStartTime4
player
roleid (Rroleid
name (Rname"o
npt_friend_bless<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FRIEND_BLESSRtype

dst_roleid (R	dstRoleid"
npt_friend_bless_notifyC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_FRIEND_BLESS_NOTIFYRtype
from_roleid (R
fromRoleid"r
npt_get_marriage_infoA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_MARRIAGE_INFORtype
roleid (Rroleid"b
npt_marriage_msg<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_MARRIAGE_MSGRtype
msg (Rmsg"�
secret_lover_info
roleid (Rroleid
	timestamp (R	timestamp
	both_love (RbothLove
	isblessed (R	isblessed"�
npt_get_marriage_info_resultH
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_MARRIAGE_INFO_RESULTRtype
retcode (Rretcode
groom_id (RgroomId

groom_name (R	groomName
bride_id (RbrideId

bride_name (R	brideName
	timestamp (R	timestamp!
marriage_seq (RmarriageSeq
	groom_msg	 (RgroomMsg
	bride_msg
 (RbrideMsg6
groom_lover (2.PB.secret_lover_infoR
groomLover6
bride_lover (2.PB.secret_lover_infoR
brideLover"�
npt_player_search_red_envelopeJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_PLAYER_SEARCH_RED_ENVELOPERtype
id (Rid
page (Rpage(
redenvelopetype (Rredenvelopetype"u
player_search_redenvelope_info8
red_envelope (2.PB.red_envelope_infoRredEnvelope
can_draw (RcanDraw"�
%npt_player_search_red_envelope_resultQ
type (2.PB.NET_PROTOCBUF_TYPE:%NPT_PLAYER_SEARCH_RED_ENVELOPE_RESULTRtype
retcode (Rretcode
id (Rid
page (Rpage
total (RtotalG
red_envelopes (2".PB.player_search_redenvelope_infoRredEnvelopes"�
npt_get_amity_max=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_AMITY_MAXRtype
roleid (Rroleid
utype (Rutype
teamid (Rteamid
members (Rmembers"�
npt_get_amity_max_resultD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_AMITY_MAX_RESULTRtype
utype (Rutype
	amity_max (RamityMax"�
npt_get_activity_awardB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_ACTIVITY_AWARDRtype#
activity_type (RactivityType
retcode (Rretcode"p
 npt_get_corps_tower_battle_awardL
type (2.PB.NET_PROTOCBUF_TYPE: NPT_GET_CORPS_TOWER_BATTLE_AWARDRtype"�
'npt_get_corps_tower_battle_award_resultS
type (2.PB.NET_PROTOCBUF_TYPE:'NPT_GET_CORPS_TOWER_BATTLE_AWARD_RESULTRtype
result (Rresult"�
npt_sync_bind_mobile_phone_infoK
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SYNC_BIND_MOBILE_PHONE_INFORtype*
bind_mobile_flags (RbindMobileFlags
award_times (R
awardTimes"'
FLAGS
CAN_BIND
ALREADY_BIND"�
time_limit_info

config_tid (R	configTid0
time_limit_timestamp (RtimeLimitTimestamp&
time_limit_type (RtimeLimitType-
time_limit_award_id (RtimeLimitAwardId"_
multi_recharge_info!
recharge_tid (RrechargeTid%
recharge_count (RrechargeCount"�
db_recharge_check_order
offset (:0Roffset
sn (:0RsnT
check_status (2(.PB.db_recharge_check_order.CHECK_STATUS:ST_IDLERcheckStatus
orders (	Rorders
	magic_num (RmagicNum3
cash_present_confirm (:0RcashPresentConfirm-
cash_used_confirm (:0RcashUsedConfirm<
bind_cash_present_confirm (:0RbindCashPresentConfirm6
bind_cash_used_confirm	 (:0RbindCashUsedConfirm"<
CHECK_STATUS
ST_IDLE

ST_PREPARE
ST_CHECKING"�
db_recharge_tss_sum_info

service_id (	R	serviceId'
first_buy_time (:0RfirstBuyTime 

begin_time (:0R	beginTime
end_time (:0RendTime2
grandtotal_opendays (:0RgrandtotalOpendays8
grandtotal_presentdays (:0RgrandtotalPresentdays
paychan (	Rpaychan

paysubchan (	R
paysubchan 
autopaychan	 (	Rautopaychan&
autopaysubchan
 (	Rautopaysubchan"S
db_recharge_user_tss_sum_info2
infos (2.PB.db_recharge_tss_sum_infoRinfos"�
db_check_order
billno (Rbillno7
status (2.PB.db_check_order.CHECK_STATUSRstatus3
flags (2.PB.db_check_order.ORDER_FLAGRflags
account (	Raccount
cost (:0Rcost
	cost_bind (:0RcostBind
present (:0Rpresent$
present_bind (:0RpresentBind"
create_time	 (:0R
createTime3
confirm_success_time
 (:0RconfirmSuccessTime
reason (:0Rreason 

sub_reason (:0R	subReason"`
CHECK_STATUS
ST_WAIT_AUANY_CONFIRM
ST_AUANY_CONFIRMING
ST_AUANY_CONFIRM_SUCCESS"#

ORDER_FLAG
COST
PRESENT"=
db_check_orders*
orders (2.PB.db_check_orderRorders"�
db_recharge_info_base
save_amt (:0RsaveAmt#
save_amt_fix (:0R
saveAmtFix
cash_add (:0RcashAdd$
cash_present (:0RcashPresent
	cash_used (:0RcashUsed-
bind_cash_present (:0RbindCashPresent'
bind_cash_used (:0RbindCashUsed 

add_serial (:0R	addSerial 

use_serial	 (:0R	useSerial%
vip_get_level
 (:0RvipGetLevel

last_order (R	lastOrder6
check_orders (2.PB.db_check_ordersRcheckOrders"�
db_recharge_infoG
recharge_info_base (2.PB.db_recharge_info_baseRrechargeInfoBaseM
recharge_check_order (2.PB.db_recharge_check_orderRrechargeCheckOrderO
recharge_tss_infos (2!.PB.db_recharge_user_tss_sum_infoRrechargeTssInfos'
today_cash_add (:0RtodayCashAdd6
cash_add_day_timestamp (:0RcashAddDayTimestamp!
fund_actived (RfundActived5
month_card_activity_tid (RmonthCardActivityTid7
month_card_end_timestamp (RmonthCardEndTimestamp2

time_limit (2.PB.time_limit_infoR	timeLimit3
first_recharge_award (:0RfirstRechargeAward'
month_cash_add (:0RmonthCashAdd:
month_cash_add_timestamp (:0RmonthCashAddTimestamp2
first_recharge_double (RfirstRechargeDouble
vip_exp (:0RvipExp)
second_cash_add (:0RsecondCashAdd7
first_recharge_version (:0RfirstRechargeVersion<
db_first_recharge_version (:0RdbFirstRechargeVersion<
time_limit_recharge_total (:0RtimeLimitRechargeTotalK
!time_limit_recharge_total_version (:0RtimeLimitRechargeTotalVersionP
$db_time_limit_recharge_total_version (:0RdbTimeLimitRechargeTotalVersion>
multi_recharge (2.PB.multi_recharge_infoRmultiRechargeN
#db_time_limit_consume_total_version (:0RdbTimeLimitConsumeTotalVersionE
old_player_back_recharge_total (:0RoldPlayerBackRechargeTotal:
old_player_back_deadline (:0RoldPlayerBackDeadline8
old_player_back_offtime  (:0RoldPlayerBackOfftime"�
npt_sync_recharge_infoB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SYNC_RECHARGE_INFORtype9
recharge_info (2.PB.db_recharge_infoRrechargeInfo"�
npt_update_login_statusC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_UPDATE_LOGIN_STATUSRtype
openkey (	Ropenkey
paytoken (	Rpaytoken
pf (	Rpf
pfkey (	Rpfkey
retcode (Rretcode"�
npt_notify_recharge?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NOTIFY_RECHARGERtype
appid (	Rappid
retcode (Rretcode"�
ui_control_info4
ui_type (2.PB.ui_control_info.UI_TYPERuiType
control (Rcontrol"%
UI_TYPE

UI_WENJUAN 

UI_MAX"u
npt_ui_control:
type (2.PB.NET_PROTOCBUF_TYPE:NPT_UI_CONTROLRtype'
info (2.PB.ui_control_infoRinfo"O
zoneid_info!
merge_zoneid (RmergeZoneid

cur_zoneid (R	curZoneid"�
npt_notify_zoneid_list@
type (2.PB.NET_PROTOCBUF_TYPE:NPT_NOTIFY_ZONE_LISTRtype0
zoneid_list (2.PB.zoneid_infoR
zoneidList"n
npt_get_friend_list?
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_FRIEND_LISTRtype
roleid (Rroleid"�
npt_change_zone_responseD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CHANGE_ZONE_RESPONSERtype
dst_zone (RdstZone
retcode (Rretcode"�
npt_center_battle_player_applyJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CENTER_BATTLE_PLAYER_APPLYRtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType"�
!npt_center_battle_player_apply_reM
type (2.PB.NET_PROTOCBUF_TYPE:!NPT_CENTER_BATTLE_PLAYER_APPLY_RERtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType*
average_wait_time (RaverageWaitTime
ret (Rret"�
$npt_center_battle_player_search_listP
type (2.PB.NET_PROTOCBUF_TYPE:$NPT_CENTER_BATTLE_PLAYER_SEARCH_LISTRtype,
center_battle_type (RcenterBattleType"�
player_search_list_info
battle_type (R
battleType

apply_time (R	applyTime*
average_wait_time (RaverageWaitTime

agree_join (R	agreeJoin"�
'npt_center_battle_player_search_list_reS
type (2.PB.NET_PROTOCBUF_TYPE:'NPT_CENTER_BATTLE_PLAYER_SEARCH_LIST_RERtype,
center_battle_type (RcenterBattleType1
infos (2.PB.player_search_list_infoRinfos"�
npt_center_battle_player_quitI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CENTER_BATTLE_PLAYER_QUITRtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType"�
 npt_center_battle_player_quit_reL
type (2.PB.NET_PROTOCBUF_TYPE: NPT_CENTER_BATTLE_PLAYER_QUIT_RERtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType
ret (Rret"�
!npt_center_battle_player_punishedM
type (2.PB.NET_PROTOCBUF_TYPE:!NPT_CENTER_BATTLE_PLAYER_PUNISHEDRtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType
punish_time (R
punishTime"�
npt_center_battle_join_askF
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CENTER_BATTLE_JOIN_ASKRtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType"�
npt_center_battle_join_answerI
type (2.PB.NET_PROTOCBUF_TYPE:NPT_CENTER_BATTLE_JOIN_ANSWERRtype,
center_battle_type (RcenterBattleType
battle_type (R
battleType
agree (Ragree"x
npt_hometown_farm_updateD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_HOMETOWN_FARM_UPDATERtype
result (Rresult"�
"npt_social_space_roam_friend_applyN
type (2.PB.NET_PROTOCBUF_TYPE:"NPT_SOCIAL_SPACE_ROAM_FRIEND_APPLYRtype
roleid (Rroleid"�
%npt_social_space_roam_friend_apply_reQ
type (2.PB.NET_PROTOCBUF_TYPE:%NPT_SOCIAL_SPACE_ROAM_FRIEND_APPLY_RERtype
ret (Rret"i
gp_center_battle_commanderK
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CENTER_BATTLE_COMMANDERRtype"g
gp_center_battle_teleportJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CENTER_BATTLE_TELEPORTRtype"c
gp_fake_auction_refreshH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKE_AUCTION_REFRESHRtype"�
gp_longyu_unlockA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_LONGYU_UNLOCKRtype
location (Rlocation
index (Rindex"�
gp_longyu_activateC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_LONGYU_ACTIVATERtype
equip_index (R
equipIndex
	longyu_id (RlongyuId"�
gp_weather_forecastD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_WEATHER_FORECASTRtype
scene (Rscene
mask (Rmask"?
WEATHER_MASK

WM_ALL 
WM_CUR_WEATHER
MS_FORECAST"�
gp_runinto_npc?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RUNINTO_NPCRtype
npc_id (RnpcId
effect (Reffect"v
gp_roll_item_requestE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ROLL_ITEM_REQUESTRtype
roll_id (RrollId"z
gp_matter_pause_moveE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MATTER_PAUSE_MOVERtype
	matter_id (RmatterId"_
gp_wish_tree_irrigateF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_WISH_TREE_IRRIGATERtype"]
gp_wish_tree_harvestE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_WISH_TREE_HARVESTRtype"�
gp_collection_opA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_COLLECTION_OPRtype&
op (2.PB.collection_op_typeRopD
collects (2(.PB.gp_collection_op.collection_pos_infoRcollectsj
collection_pos_info#
collection_id (RcollectionId
pos (Rpos
	direction (R	direction"�
gp_set_wechat_switchE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SET_WECHAT_SWITCHRtype!
switch_index (RswitchIndex"�
gp_take_photo>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_TAKE_PHOTORtype
op (Rop
	action_id (RactionId
action_time (R
actionTime"�
gp_intimate_op?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_INTIMATE_OPRtype+
oper (2.PB.GS_INTIMATE_OP_TYPERoper
target (Rtarget
name (Rname

need_money (R	needMoney"a
gp_fauction_store_openG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAUCTION_STORE_OPENRtype"g
gp_fauction_store_levelupJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAUCTION_STORE_LEVELUPRtype"�
gp_guard_rename@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_RENAMERtype
guard_id (RguardId
name (Rname"�
gp_guard_change_shapeF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_CHANGE_SHAPERtype
guard_id (RguardId
shape (Rshape"�
gp_guard_summon@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_SUMMONRtype
guard_id (RguardId
shape (Rshape"�
gp_guard_train?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_TRAINRtype
guard_id (RguardId
train_count (R
trainCount"�
gp_guard_change_skillF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_CHANGE_SKILLRtype
guard_id (RguardId

skill_slot (R	skillSlot

item_index (R	itemIndex"�
gp_guard_putin_slotD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_PUTIN_SLOTRtype
element (Relement
guard_id (RguardId"y
gp_guard_takeout_slotF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_TAKEOUT_SLOTRtype
element (Relement"y
gp_guard_slot_levelupF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_SLOT_LEVELUPRtype
element (Relement"�
gp_guard_add_expA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUARD_ADD_EXPRtype
guard_id (RguardId
item_id (RitemId
item_num (RitemNum"i
gp_role_set_nameA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ROLE_SET_NAMERtype
name (Rname"�
gp_chatbox_selectB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHATBOX_SELECTRtype
	chat_type (RchatType
index (Rindex"�
gp_friend_add_email_inviteK
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FRIEND_ADD_EMAIL_INVITERtype\
handle_type (2;.PB.gp_friend_add_email_invite.FRIEND_ADD_EMAIL_INVITE_TYPER
handleType
	email_ids (RemailIds"o
FRIEND_ADD_EMAIL_INVITE_TYPE&
"FRIEND_ADD_EMAIL_INVITE_TYPE_AGREE'
#FRIEND_ADD_EMAIL_INVITE_TYPE_REJECT"w
gp_instance_lucky_drawG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_INSTANCE_LUCKY_DRAWRtype
index (Rindex"�
gp_kotodama_op?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_KOTODAMA_OPRtype(
event (2.PB.KOTODAMA_EVENTRevent%
kotodama_index (RkotodamaIndex"x
gp_equip_enhanceA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_ENHANCERtype!
enhance_part (RenhancePart"h
gp_enter_pelt>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ENTER_PELTRtype
enter (:0Renter"n
gp_task_find_npcA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_TASK_FIND_NPCRtype
npc_tid (RnpcTid"�
social_space_content
target (Rtarget$
give_gift_type (RgiveGiftType-
give_gift_item_type (RgiveGiftItemType

money_cost (R	moneyCost
	cash_cost (RcashCost
gift_msg (RgiftMsg(
place_gift_count (RplaceGiftCount

style_type (R	styleType
style_id	 (RstyleId3
give_gift_item_loction
 (RgiveGiftItemLoction/
give_gift_item_index (RgiveGiftItemIndex(
give_gift_itemid (RgiveGiftItemid/
give_gift_item_count (RgiveGiftItemCount
	anonymous (R	anonymous
gfx_type (RgfxType
album_id (RalbumId%
present_amount (RpresentAmount!
comes_amount (RcomesAmount"�
gp_social_space_opC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SOCIAL_SPACE_OPRtype(
op (2.PB.SOCIAL_SPACE_OP_TYPERop2
content (2.PB.social_space_contentRcontent"�
gp_use_item_with_countG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_USE_ITEM_WITH_COUNTRtype
location (Rlocation
index (Rindex
tid (Rtid
count (:1Rcount"�
gp_player_slave_opC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PLAYER_SLAVE_OPRtype/
op (2.PB.gp_player_slave_op.SLAVE_OPRop
roleid (Rroleid
slave_index (R
slaveIndex
param1 (Rparam1"Y
SLAVE_OP
SLAVE_OP_BLESS
SLAVE_OP_WORK
SLAVE_OP_GIFT
SLAVE_OP_UPDATE"�
gp_refence_operateC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_REFENCE_OPERATERtype1
op (2!.PB.gp_refence_operate.REFENCE_OPRop"6

REFENCE_OP
REFENCE_OP_GIFT
REFENCE_OP_CASH"m
gp_corps_server_battle_awardM
type (2.PB.C2S_GS_PROTOC_TYPE:!GPROTOC_CORPS_SERVER_BATTLE_AWARDRtype"x
GP_Begin;
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_BEGIN_TRtype
value (:0Rvalue
value2 (Rvalue2"T
equip_location
location (Rlocation
index (Rindex
tid (Rtid"�
gp_child_divorce_opD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_DIVORCE_OPRtype/
op (2.PB.gp_child_divorce_op.OP_TYPERop
reply (Rreply
	init_guid (RinitGuid
	initiator (R	initiator"
OP_TYPE
REPLY_INVITE"�
gp_child_marriage_select_titleO
type (2.PB.C2S_GS_PROTOC_TYPE:#GPROTOC_CHILD_MARRIAGE_SELECT_TITLERtype
guid (Rguid
	inlaws_id (RinlawsId"�
gp_child_marriage_confirmJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_MARRIAGE_CONFIRMRtype

is_confirm (R	isConfirm
	use_money (RuseMoney"�
gp_child_marriage_invite_replyO
type (2.PB.C2S_GS_PROTOC_TYPE:#GPROTOC_CHILD_MARRIAGE_INVITE_REPLYRtype
	initiator (R	initiator
reply (Rreply"�
gp_child_marriage_put_childL
type (2.PB.C2S_GS_PROTOC_TYPE: GPROTOC_CHILD_MARRIAGE_PUT_CHILDRtype
location (Rlocation
index (Rindex
tid (Rtid
guid (Rguid"�
gp_check_wedding_snatch_condM
type (2.PB.C2S_GS_PROTOC_TYPE:!GPROTOC_CHECK_WEDDING_SNATCH_CONDRtype#
target_roleid (RtargetRoleid"�
gp_marriage_snatchingF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MARRIAGE_SNATCHINGRtype?
s_type (2(.PB.gp_marriage_snatching.snatching_typeRsType#
target_roleid (RtargetRoleid"7
snatching_type
ST_IN_WEDDING
ST_IN_PARADING"�
gp_illegal_reportB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ILLEGAL_REPORTRtype#
target_roleid (RtargetRoleid

illegal_id (R	illegalId"x
gp_use_rename_itemC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_USE_RENAME_ITEMRtype

item_index (R	itemIndex"�
gp_set_photo_id@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SET_ID_PHOTORtype
photoid (Rphotoid9
op (2.PB.gp_set_photo_id.op_type:SET_PHOTO_IDRop"/
op_type
SET_PHOTO_ID 
SET_PHOTO_DECO"�
gp_use_lottery_one_buttonJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_USE_LOTTERY_ONE_BUTTONRtype
location (Rlocation
index (Rindex
item_id (RitemId
	use_count (RuseCount"�
gp_use_dice<
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_USE_DICERtype
location (Rlocation
index (Rindex?
op (2.PB.gp_use_dice.use_op_type:USE_DICE_TYPE_DICERop
dice_id (RdiceId"H
use_op_type
USE_DICE_TYPE_DICE !
USE_DICE_TYPE_GET_ROUND_AWARD"v
gp_interact_matterC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_INTERACT_MATTERRtype
	matter_id (RmatterId"�
gp_interact_speedupD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_INTERACT_SPEEDUPRtype$

is_speedup (:falseR	isSpeedup"�
gp_interact_operationF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_INTERACT_OPERATIONRtype;
oper (2'.PB.gp_interact_operation.interact_typeRoper"&
interact_type
INTER_TYPE_ACTIVE"D
interact_player_info
roleid (Rroleid
index (Rindex"�
gp_matter_interact_infoA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_interact_infoRtype
	matter_id (RmatterId
all_info (RallInfo
op_type (RopType.
infos (2.PB.interact_player_infoRinfos
speed (Rspeed
active_time (R
activeTime"N
INTERACT_OP_TYPE
INTERACT_JOIN
INTERACT_LEAVE
INTERACT_ACTIVE"�
gp_get_lottery_one_buttonJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_LOTTERY_ONE_BUTTONRtype
item_id (RitemId
location (Rlocation
index (Rindex"r
gp_duel_invite_cA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_DUEL_INVITE_CRtype
	player_id (RplayerId"�
gp_duel_invite_reply_cG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_DUEL_INVITE_REPLY_CRtype
	player_id (RplayerId
reply (Rreply"�
gp_child_pregnate_replyH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_PREGNATE_REPLYRtype
	player_id (RplayerId
reply (Rreply
use_item (RuseItem"�
gp_child_delivery_replyH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_DELIVERY_REPLYRtype
	player_id (RplayerId
reply (Rreply"�
gp_child_cultureA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_CULTURERtype
child_index (R
childIndex
culture_way (R
cultureWay"�
gp_child_sleep?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_SLEEPRtype
op_type (RopType
child_index (R
childIndex"�
gp_equip_grade_and_quality_upN
type (2.PB.C2S_GS_PROTOC_TYPE:"GPROTOC_EQUIP_GRADE_AND_QUALITY_UPRtype=

money_type (2.PB.use_money_type:
USE_M_BINDR	moneyType(
equip (2.PB.equip_locationRequip"
one_button_up (RoneButtonUp"�
gp_create_corps@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CREATE_CORPSRtype

corps_name (R	corpsName:
corps_recruitment_manfest (RcorpsRecruitmentManfest
corps_badge (R
corpsBadge,
corps_support_side (RcorpsSupportSide"�
gp_equip_attach_gemD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_ATTACH_GEMRtype
equip_index (R
equipIndex

hole_index (R	holeIndex
	gem_index (RgemIndex
gem_tid (RgemTid"�
gp_equip_detach_gemD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_DETACH_GEMRtype
equip_index (R
equipIndex

hole_index (R	holeIndex
gem_tid (RgemTid"�
gp_child_attach_gemD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_ATTACH_GEMRtype
index (Rindex
	gem_index (RgemIndex"�
gp_child_detach_gemD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_DETACH_GEMRtype
index (Rindex
slot (Rslot"�
gp_child_lianxingB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_LIANXINGRtype
index (Rindex
slot (Rslot"�
gp_extend_fashion_color_limitN
type (2.PB.C2S_GS_PROTOC_TYPE:"GPROTOC_EXTEND_FASHION_COLOR_LIMITRtype#
fashion_index (RfashionIndex
color_limit (R
colorLimit"�
gp_equip_attach_gem_upgradeL
type (2.PB.C2S_GS_PROTOC_TYPE: GPROTOC_EQUIP_ATTACH_GEM_UPGRADERtype
equip_index (R
equipIndex

hole_index (R	holeIndex
gem_tid (RgemTid"�
gp_equip_open_addonD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_OPEN_ADDONRtype
location (Rlocation
equip_index (R
equipIndex
	equip_tid (RequipTid
addon_index (R
addonIndex!
nobind_money (RnobindMoney"6
tool_bind_info
bind (Rbind
tid (Rtid"�
gp_equip_lianxingB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_LIANXINGRtype
location (Rlocation
equip_index (R
equipIndex
	equip_tid (RequipTid(
tools (2.PB.tool_bind_infoRtools

bind_money (R	bindMoney
	use_baodi (RuseBaodi!
nobind_baodi (RnobindBaodi"�
gp_equip_train?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_TRAINRtype

prop_index (R	propIndex"
cur_train_num (RcurTrainNum"
one_button_up (RoneButtonUp"�
gp_equip_switch_surfaceH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_SWITCH_SURFACERtype
surface_tid (R
surfaceTid
index (Rindex"�
gp_equip_skill_transferH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_SKILL_TRANSFERRtype!
src_location (RsrcLocation
	src_index (RsrcIndex
src_tid (RsrcTid!
dst_location (RdstLocation
	dst_index (RdstIndex
dst_tid (RdstTid"�
gp_equip_affixes_transferJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_AFFIXES_TRANSFERRtype!
src_location (RsrcLocation
	src_index (RsrcIndex
src_tid (RsrcTid*
src_affixes_index (RsrcAffixesIndex!
dst_location (RdstLocation
	dst_index (RdstIndex
dst_tid (RdstTid*
dst_affixes_index	 (RdstAffixesIndex"�
gp_equip_transferB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_TRANSFERRtype!
src_location (RsrcLocation
	src_index (RsrcIndex
src_tid (RsrcTid!
dst_location (RdstLocation
	dst_index (RdstIndex
dst_tid (RdstTid#
transfer_star (RtransferStar%
transfer_addon	 (RtransferAddon#
transfer_gems
 (RtransferGems"�
gp_equip_refine@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_REFINERtype#
main_location (RmainLocation

main_index (R	mainIndex
main_tid (RmainTid+
material_location (RmaterialLocation%
material_index (RmaterialIndex!
material_tid (RmaterialTid

attr_index (R	attrIndex

bind_money	 (R	bindMoney"�
gp_equip_baseattr_refineI
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_BASEATTR_REFINERtype
location (Rlocation
index (Rindex
tid (Rtid"�
gp_equip_extraattr_recastJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_EXTRAATTR_RECASTRtype
location (Rlocation
equip_index (R
equipIndex
	equip_tid (RequipTid
save (Rsave
tool_tid (RtoolTid
reply (Rreply
addons	 (Raddons;
minimum_guarantee_mode
 (:falseRminimumGuaranteeMode"r
gp_get_exp_lotteryC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_EXP_LOTTERYRtype
is_free (RisFree"�
gp_get_lottery_expC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_LOTTERY_EXPRtype
roleid (Rroleid
forced (Rforced"�
gp_item_combine_advanceH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ITEM_COMBINE_ADVANCERtype:
slot (2&.PB.gp_item_combine_advance.inner_itemRslot?
items (2).PB.gp_item_combine_advance.backpack_itemRitems
money (Rmoney
stamp (Rstamp
target (Rtarget#

bind_money (:trueR	bindMoney
money2 (Rmoney2V

inner_item
location (:11Rlocation
index (Rindex
slot (Rslot;
backpack_item
index (Rindex
count (Rcount"�
gp_item_combine@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ITEM_COMBINERtype
item_tid (RitemTid

item_count (R	itemCount

bind_money (R	bindMoney
stamp (Rstamp"�
gp_nation_escort_opD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_NATION_ESCORT_OPRtype
npcid (Rnpcid
mount (Rmount"~
gp_escort_change_speedG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ESCORT_CHANGE_SPEEDRtype
	old_state (RoldState"�
gp_minigame_operateD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MINIGAME_OPERATERtype
oper (Roper
param1 (Rparam1
param2 (Rparam2
data (Rdata"C
	card_info
card_id (RcardId

card_count (R	cardCount"�
gp_card_operate@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CARD_OPERATERtype/
oper (2.PB.gp_card_operate.OP_TYPERoper#
cards (2.PB.card_infoRcards
param1 (Rparam1
param2 (Rparam2"a
OP_TYPE
OT_ACTIVE_SUIT
OT_DECOMPOSE_CARD
OT_DECOMPOSE_QUICK
OT_COMPOSE_CARD"W
talisman_info
talisman_id (R
talismanId%
talisman_count (RtalismanCount"�
gp_talisman_operateD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_TALISMAN_OPERATERtype3
oper (2.PB.gp_talisman_operate.OP_TYPERoper/
	talismans (2.PB.talisman_infoR	talismans
param1 (Rparam1
param2 (Rparam2"=
OP_TYPE

OT_COMPOSE
OT_DECOMPOSE
OT_COMBINATE"]
retinue_formation_info$
formation_id (:0RformationId

retinue_id (R	retinueId"�
retinue_task_info
taskid (:0Rtaskid
	timestamp (:0R	timestamp

retinue_id (R	retinueId
grade (Rgrade"�
gp_retinue_operateC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RETINUE_OPERATERtype2
oper (2.PB.gp_retinue_operate.OP_TYPERoper8
	formation (2.PB.retinue_formation_infoR	formation

retinue_id (R	retinueId)
task (2.PB.retinue_task_infoRtask
param1 (Rparam1
param2 (Rparam2F
	decompose (2(.PB.gp_retinue_operate.decompose_retinueR	decomposeH
decompose_retinue

retinue_id (R	retinueId
count (Rcount"�
OP_TYPE
OT_COMPOSE_GROUP
OT_UPGRADE_QUALITY
OT_ADD_TASK
OT_RECEIVE_TASK_AWARD
OT_DECOMPOSE
OT_ACTIVE_PRIVATE	
OT_FASHION_SELECT

OT_COMBAT_SELECT
OT_FASHION_ACTIVE

OT_RECRUIT
OT_SEND_GIFT
OT_TAKE_GIFT
OT_CHAT"�
gp_player_practiceC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PLAYER_PRACTICERtype2
oper (2.PB.gp_player_practice.PP_TYPERoper
skill_id (RskillId"U
PP_TYPE
PT_PRACTICE_ONCE
PT_PRACTICE_TEN_TIMES
PT_PRACTICE_WITH_TOOL"�
gp_player_practice_once_a_levelF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PLAYER_PRACTICE_SURtype
skill_id (RskillId"|
gp_get_broadcast_buffF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_BROADCAST_BUFFRtype
	object_id (RobjectId"v
gp_set_pk_settingB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SET_PK_SETTINGRtype

pk_setting (R	pkSetting"x
gp_nation_donateA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_NATION_DONATERtype!
donate_index (RdonateIndex"�
gp_auto_reward_opB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_AUTO_REWARD_OPRtype
reward_type (R
rewardType;
	operation (2.PB.gp_auto_reward_op.OP_TYPER	operation
value (Rvalue"�
OP_TYPE
OP_GET_REWARD
OP_BUY_FUND
OP_BUY_SPECAIL_FUND1
OP_BUY_SPECAIL_FUND2
OP_BUY_SPECAIL_FUND3
OP_REFRESH_STAT"�
gp_get_achievement_awardI
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_ACHIEVEMENT_AWARDRtype'
achievement_tid (RachievementTid"�
gp_mount_operationC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MOUNT_OPERATIONRtype.
op (2.PB.gp_mount_operation.OP_TYPERop'
summon_location (RsummonLocation
value (Rvalue
value_2 (Rvalue2
value_3 (Rvalue3
value_4 (Rvalue4
value_5 (Rvalue5"�
OP_TYPE
	MOT_MOUNT

MOT_SUMMON
MOT_INC_EXP
MOT_FLYSWORD

MOT_CANCEL
MOT_PRACTICE
MOT_SWITCHSURFACE
MOT_ACTIVE_COLOR
MOT_PAINT_SURFACE	
MOT_TRAIN_SURFACE

MOT_PRACTICE_EXCHANGE
MOT_SURFACE_SELECT
MOT_ACTIVE_FASHION
MOT_SELECT_FASHION
MOT_SWITCH_FLYMODE
MOT_SWITCH_SKILL"�
gp_fly_sword_operationG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FLY_SWORD_OPERATIONRtype2
op (2".PB.gp_fly_sword_operation.OP_TYPERop"
OP_TYPE
	FOT_MOUNT"�
gp_guaji9
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUAJIRtype
start_guaji (R
startGuaji
	quit_mode (RquitMode$
half_quit_mode (RhalfQuitMode"�
gp_vip_operationA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_VIP_OPERATIONRtype,
op (2.PB.gp_vip_operation.OP_TYPERop
value (Rvalue?
exp_type (2$.PB.gp_vip_operation.LOGOUT_EXP_TYPERexpType"J
OP_TYPE
VOT_GET_LOGOUT_EXP
VOT_HIDE_VIP
VOT_HIDE_VIP_NAME"H
LOGOUT_EXP_TYPE
LET_FREE
LET_BIND_MONEY
LET_TRADE_MONEY"�
auto_combat_config
radius (Rradius"
aid_hp_pesent (RaidHpPesent 
aid_item_tid (R
aidItemTid"
auto_buy_item (RautoBuyItem 
auto_pick_up (R
autoPickUp

pick_grade (R	pickGrade
range_skill (R
rangeSkill"
last_bot_time (RlastBotTime 
last_bot_exp	 (R
lastBotExp"�
gp_auto_combat_config_c2sF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_AUTO_COMBAT_CONFIGRtype.
config (2.PB.auto_combat_configRconfig"�
gp_upgrade_bingfaB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_UPGRADE_BINGFARtype
location (Rlocation
	bingfa_id (RbingfaId

from_level (R	fromLevel
to_level (RtoLevel
	use_money (RuseMoney"�
gp_start_attack_loopE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_START_ATTACK_LOOPRtype
active (Ractive"
object_new_id (RobjectNewId
skill (Rskill"|
gp_cg_player_op@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CG_PLAYER_OPRtype
skip (Rskip
cg_id (RcgId"S
gp_wine_sitdown@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_WINE_SITDOWNRtype"�
gp_cancel_actionA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CANCEL_ACTIONRtype

perform_id (R	performId
pos (2.PB.a3d_posRpos"�
gp_stunt_config@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_STUNT_CONFIGRtype%
op (2.PB.STUNT_CONFIG_TYPERop$
rune_set_index (RruneSetIndex

skill_id_0 (RskillId0

skill_id_1 (RskillId1

skill_id_2 (RskillId2

skill_id_3 (RskillId3

skill_id_4 (RskillId4

skill_id_5	 (RskillId5

skill_id_6
 (RskillId6

skill_id_7 (RskillId7
name (Rname"�
gp_black_shop_commandF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_BLACK_SHOP_COMMANDRtype@
cmd_type (2%.PB.gp_black_shop_command.BS_CMD_TYPERcmdType
shop_id (RshopId
store_id (RstoreId
goods_id (RgoodsId
goods_index (R
goodsIndex";
BS_CMD_TYPE
BS_CMD_TYPE_REFRESH
BS_CMD_TYPE_BUY"�
gp_climbtower_shop_commandK
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CLIMBTOWER_SHOP_COMMANDRtypeE
cmd_type (2*.PB.gp_climbtower_shop_command.BS_CMD_TYPERcmdType
shop_id (RshopId
store_id (RstoreId
goods_id (RgoodsId
goods_index (R
goodsIndex
level (Rlevel";
BS_CMD_TYPE
BS_CMD_TYPE_REFRESH
BS_CMD_TYPE_BUY"�
gp_duke_command@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_DUKE_COMMANDRtype<
cmd_type (2!.PB.gp_duke_command.DUKE_CMD_TYPERcmdType
param1 (Rparam1"Y
DUKE_CMD_TYPE
DUKE_CMD_GET_REWARD
DUKE_CMD_UPGRADE
DUKE_CMD_ACTIVE_SKILL"�
gp_hero_trial_command>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HERO_TRIALRtype4
cmd (2".PB.gp_hero_trial_command.CMD_TYPERcmd"
CMD_TYPE
CT_REFRESH_HERO"�
player_corps_attr_config_t%
active_level (:-1RactiveLevel%
active_index (:-1RactiveIndex

chariot_hp (R	chariotHp
chariot_tid (R
chariotTid#
chariot_level (RchariotLevel"�
gp_corp_config?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CORP_CONFIGRtype6
config (2.PB.player_corps_attr_config_tRconfig"�
create_instance_transfer_info.
recommend_min_level (RrecommendMinLevel.
recommend_max_level (RrecommendMaxLevel
	max_level (RmaxLevel"%
camp_members_list
ids (Rids"�
enter_instance_config?
client (2'.PB.enter_instance_config.client_configRclient?
server (2'.PB.enter_instance_config.server_configRserver
uuid (Ruuid+
camps (2.PB.camp_members_listRcamps#
creator_count (RcreatorCountF
transfer_info (2!.PB.create_instance_transfer_infoRtransferInfoZ
client_config
mafia_id (RmafiaId
name (Rname
password (Rpassword�
server_config
params (Rparams
pos (2.PB.a3d_posRpos
data (Rdata%
hometown_owner (RhometownOwner
	jump_type (RjumpType

ran_center (R	ranCenter"�
gp_enter_instanceB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ENTER_INSTANCERtype
	object_id (RobjectId
inst_tid (RinstTid
inst_id (RinstId
mode (Rmode1
config (2.PB.enter_instance_configRconfig"n
gp_buy_instance@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_BUY_INSTANCERtype
inst_tid (RinstTid"�
gp_buy_backpack@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_BUY_BACKPACKRtype
location (Rlocation
inc_size (RincSize"�
gp_jieyun_skill@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_JIEYUN_SKILLRtype
skill_id (RskillId
pos (2.PB.a3d_posRpos"�
gp_corp_farm_op@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CORP_FARM_OPRtype+
op (2.PB.gp_corp_farm_op.FARM_OPRop%
farmland_index (RfarmlandIndex
crop_tid (RcropTid"5
FARM_OP
FO_PLANT
FO_SPEED

FO_HARVEST"�
gp_climb_tower_opB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CLIMB_TOWER_OPRtypeA
operate_type (2.PB.gp_climb_tower_op.CLIMB_OPRoperateType
param1 (Rparam1"�
CLIMB_OP
CO_MANUAL_FIGHT
CO_AUTO_FIGHT_BEGIN
CO_AUTO_FIGHT_RUSH
CO_AUTO_FIGHT_STOP
CO_AUTO_FIGHT_AWARD
CO_RESET"�

gp_gs_ping;
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GS_PINGRtype(
client_send_time (RclientSendTime&
client_last_ttl (RclientLastTtl"v
gp_get_list_infoA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_LIST_INFORtype
player_list (R
playerList"j
gp_camp_fire=
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CAMP_FIRERtype
	fire_type (RfireType"�
gp_ask_help<
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ASK_HELPRtype3
	info_type (2.PB.ask_help_info_typeRinfoType"�
gp_find_way<
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FIND_WAYRtype
reason (Rreason$
dst_pos (2.PB.a3d_posRdstPos"g
gp_change_mirrorA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHANGE_MIRRORRtype
mid (Rmid"j
gp_buy_force=
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_BUY_FORCERtype
	force_num (RforceNum"�
gp_plant9
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PLANTRtype%
op (2.PB.gp_plant.PLANT_OPRop
seed_tid (RseedTid
sow_time (RsowTime":
PLANT_OP
FO_PLANT
FO_IMMEDIATE

FO_HARVEST"�
gp_equip_chaijieA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_CHAIJIERtype
location (Rlocation
index (Rindex
tid (Rtid"s
gp_client_print_infoE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CLIENT_PRINT_INFORtype
infos (Rinfos"�
gp_retrieve_op?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RETRIEVE_OPRtype
activity_id (R
activityId
tid (Rtid#
retrieve_type (RretrieveType"g
gp_fashion_mode@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FASHION_MODERtype
mode (Rmode"�
gp_fashion_title_upgradeI
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FASHION_TITLE_UPGRADERtype
title_id (RtitleId"r
gp_sweep_instanceB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SWEEP_INSTANCERtype
inst_tid (RinstTid"i
gp_get_red_packetB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_RED_PACKETRtype
tid (Rtid"�
gp_common_operationD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_COMMON_OPERATIONRtype1
op (2!.PB.gp_common_operation.COMMON_OPRop
param (Rparam"$
	COMMON_OP
CO_END_REFUSE_FIGHT"�
gp_compensation@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_COMPENSATIONRtype
cid (Rcid
tid (Rtid
level (Rlevel
repu (Rrepu"�
gp_upgrade_chariotC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_UPGRADE_CHARIOTRtype
tid (Rtid
index (Rindex<
up_type (2#.PB.gp_upgrade_chariot.UPGRADE_TYPERupType"9
UPGRADE_TYPE

UT_DIAMOND
UT_REPU
UT_MONEY"{
gp_rent_chariot@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RENT_CHARIOTRtype
tid (Rtid
index (Rindex"u
gp_wuhun_open>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_WUHUN_OPENRtype$
wuhun_group_id (RwuhunGroupId"�
gp_wuhun_upgradeA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_WUHUN_UPGRADERtype
node_id (RnodeId
level (Rlevel!
upgrade_type (RupgradeType"i
WUHUN_UPGRADE_TYPE
WUHUN_UP_EXP
WUHUN_UP_DIAMOND
WUHUN_UP_TASK
WUHUN_UP_TASK_FINISH"�
gp_add_property@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ADD_PROPERTYRtype
	property1 (:0R	property1
	property2 (:0R	property2
	property3 (:0R	property3
	property4 (:0R	property4
	property5 (:0R	property5
	property6 (:0R	property6"p
gp_clear_propertyB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CLEAR_PROPERTYRtype
index (:0Rindex"�
gp_child_set_property_planK
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_SET_PROPERTY_PLANRtype"
child_index (:0R
childIndex
	property1 (:0R	property1
	property2 (:0R	property2
	property3 (:0R	property3
	property4 (:0R	property4
	property5 (:0R	property5
	property6 (:0R	property6"�
gp_child_add_propertyF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_ADD_PROPERTYRtype"
child_index (:0R
childIndex
	property1 (:0R	property1
	property2 (:0R	property2
	property3 (:0R	property3
	property4 (:0R	property4
	property5 (:0R	property5
	property6 (:0R	property6"�
gp_child_clear_propertyH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_CLEAR_PROPERTYRtype"
child_index (:0R
childIndex
index (:0Rindex"�
gp_pet_set_property_planI
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_SET_PROPERTY_PLANRtype
	pet_index (:0RpetIndex
	property1 (:0R	property1
	property2 (:0R	property2
	property3 (:0R	property3
	property4 (:0R	property4
	property5 (:0R	property5
	property6 (:0R	property6"�
gp_pet_add_propertyD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_ADD_PROPERTYRtype
	pet_index (:0RpetIndex
	property1 (:0R	property1
	property2 (:0R	property2
	property3 (:0R	property3
	property4 (:0R	property4
	property5 (:0R	property5
	property6 (:0R	property6"�
gp_pet_clear_propertyF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_CLEAR_PROPERTYRtype
	pet_index (:0RpetIndex
index (:0Rindex"�
gp_pet_select_skillD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_SELECT_SKILLRtype
	pet_index (:0RpetIndex"
skill_index (:0R
skillIndex"�
gp_pet_skill_lockB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_SKILL_LOCKRtype
	pet_index (:0RpetIndex"
skill_index (:0R
skillIndex
lock (:trueRlock"�
gp_pet_upgrade?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_UPGRADERtype
	pet_index (:0RpetIndex
retcode (:0Rretcode"�
gp_pet_levelup_readan_skillF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_LEVELUP_READANRtype
	pet_index (:0RpetIndex$
readan_index (:0RreadanIndex"�
gp_pet_clear_readan_skillD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_CLEAR_READANRtype
	pet_index (:0RpetIndex$
readan_index (:0RreadanIndex"�
gp_change_faceidA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHANGE_FACEIDRtype
faceid (Rfaceid
earid (:0Rearid
tailid (:0Rtailid"z
gp_change_property_planH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHANGE_PROPERTY_PLANRtype
plan (:0Rplan"�
gp_produce_skill_level_upJ
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PRODUCE_SKILL_LEVEL_UPRtype
	skill_tid (RskillTid
to_level (RtoLevel"�
gp_produce_item@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PRODUCE_ITEMRtype

recipe_tid (R	recipeTid$
lucky_item_tid (RluckyItemTid"�
gp_change_equip_suitE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHANGE_EQUIP_SUITRtype%
equip_location (RequipLocation*
transfer_gems (:falseRtransferGems"
	swap_star (:falseRswapStar"j
gp_hide_fashion@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HIDE_FASHIONRtype
mask (:0Rmask"�
gp_select_fashionB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SELECT_FASHIONRtype#
fashion_index (RfashionIndex

baby_index (R	babyIndex"�
gp_active_fashion_colorH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ACTIVE_FASHION_COLORRtype#
fashion_index (RfashionIndex
active_cost (R
activeCost"�
gp_paint_fashionA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PAINT_FASHIONRtype#
fashion_index (RfashionIndex!
fashion_part (RfashionPart
color_index (R
colorIndex

paint_cost (R	paintCost
	color_hsv (RcolorHsv

toner_cost (R	tonerCost
bright_cost (R
brightCost"�
gp_devour_destinyB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_DEVOUR_DESTINYRtype.
devourer_location (:0RdevourerLocation(
devourer_index (:0RdevourerIndex

food_index (R	foodIndex
op_index (:0RopIndex"�
gp_move_destiny@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MOVE_DESTINYRtype&
from_location (:0RfromLocation 

from_index (:0R	fromIndex"
to_location (:0R
toLocation
to_index (:0RtoIndex
op_index (:0RopIndex"w
gp_refresh_destinyC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_REFRESH_DESTINYRtype
op_index (:0RopIndex"l
gp_summon_child@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SUMMON_CHILDRtype
index (:0Rindex"l
gp_recall_child@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RECALL_CHILDRtype
index (:0Rindex"�
gp_child_learn_skillE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_LEARN_SKILLRtype
index (:0Rindex 

book_index (:0R	bookIndex"
skill_index (:0R
skillIndex"h
gp_summon_pet>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SUMMON_PETRtype
index (:0Rindex"h
gp_recall_pet>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RECALL_PETRtype
index (:0Rindex"v
gp_activity_exchangeE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ACTIVITY_EXCHANGERtype
index (:0Rindex"w
gp_child_reborn@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_REBORNRtype"
child_index (:0R
childIndex"o
gp_pet_reborn>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_REBORNRtype
	pet_index (:0RpetIndex"�
gp_pet_mixis=
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_MIXISRtype 

pet1_index (:0R	pet1Index 

pet2_index (:0R	pet2Index"�
gp_child_rename@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_RENAMERtype"
child_index (:0R
childIndex
name (Rname"�
gp_child_tour>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_TOURRtype"
child_index (:0R
childIndexE
	tour_type (2.PB.gp_child_tour.TOUR_TYPE:SINGLE_TOURRtourType"a
	TOUR_TYPE
SINGLE_TOUR 
LEAD_COUPLE_TOUR
AGREE_COUPLE_TOUR
REJECT_COUPLE_TOUR"�
gp_child_tour_get_awardH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_TOUR_GET_AWARDRtype"
child_index (:0R
childIndex"�
gp_pet_rename>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_RENAMERtype
	pet_index (:0RpetIndex
name (Rname"M
gp_pet_train=
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PET_TRAINRtype"�
gp_hunt_dragon_point_switchL
type (2.PB.C2S_GS_PROTOC_TYPE: GPROTOC_HUNT_DRAGON_POINT_SWITCHRtype
state (Rstate"p
gp_kasi_restore@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_KASI_RESTORERtype
nothing (:0Rnothing"�
gp_xyxw_invite?
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_XYXW_INVITERtype
	object_id (RobjectId
	xyxw_type (RxyxwType

emotion_id (R	emotionId"�
gp_xyxw_invite_replyE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_XYXW_INVITE_REPLYRtype
	object_id (RobjectId
	xyxw_type (RxyxwType
reply (Rreply

emotion_id (R	emotionId"�
gp_xyxw_request@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_XYXW_REQUESTRtype
	object_id (RobjectId
	xyxw_type (RxyxwType"�
gp_xyxw_request_replyF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_XYXW_REQUEST_REPLYRtype
	object_id (RobjectId
	xyxw_type (RxyxwType
reply (Rreply"O
gp_xyxw_leave>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_XYXW_LEAVERtype"�
gp_hometown_show_objectH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_SHOW_OBJECTRtype
index (Rindex
	pos_index (RposIndex
x (Rx
y (Ry
dir (Rdir"�
gp_hometown_hide_objectH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_HIDE_OBJECTRtype
index (Rindex
	pos_index (RposIndex"o
gp_hometown_enterB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_ENTERRtype
roleid (Rroleid"r
gp_hometown_buy@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_BUYRtype

object_tid (R	objectTid"�
gp_hometown_sellA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_SELLRtype
index (Rindex

object_tid (R	objectTid
count (Rcount"�
gp_hometown_benefitD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_BENEFITRtype4
	operation (2.PB.HOMETOWN_OPERATIONR	operation"�
gp_hometown_farm_opD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_HOMETOWN_FARM_OPRtype
farmid (Rfarmid
objectid (Robjectid"
op (2.PB.PLAYER_FARM_OPRop"e
gp_second_hometown_enterI
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SECOND_HOMETOWN_ENTERRtype"W
gp_enter_afk_modeB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_ENTER_AFK_MODERtype"W
gp_leave_afk_modeB
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_LEAVE_AFK_MODERtype"i
gp_exchange_cashA
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EXCHANGE_CASHRtype
cash (Rcash"~
gp_apply_revenge_battleH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_APPLY_REVENGE_BATTLERtype
enemy_id (RenemyId"�
gp_response_revenge_battleK
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_RESPONSE_REVENGE_BATTLERtypeB
result (2*.PB.gp_response_revenge_battle.RESULT_TYPERresult
enemy_id (RenemyId"%
RESULT_TYPE

ACCEPT 

REJECT"|
gp_fake_auction_selfshopH
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKEAUCTION_SELFSHOPRtype
pageid (Rpageid"�
gp_fake_auction_openD
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKEAUCTION_OPENRtype
item_id (RitemId#
item_location (RitemLocation
item_pos (RitemPos
item_num (RitemNum
price (Rprice!
recomm_price (RrecommPrice"�
gp_fake_auction_reopenF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKEAUCTION_REOPENRtype
	auctionid (R	auctionid
price (Rprice"|
gp_fake_auction_closeE
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKEAUCTION_CLOSERtype
	auctionid (R	auctionid"�
gp_fake_auction_buyC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKEAUCTION_BUYRtype
	auctionid (R	auctionid
price (Rprice
count (Rcount
item_id (RitemId"�
gp_fake_auction_get_moneyI
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FAKEAUCTION_GET_MONEYRtype
all (Rall
	auctionid (R	auctionid"a
gp_friend_bless_rewardG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_FRIEND_BLESS_REWARDRtype"�
#gp_player_search_corps_battle_scoreT
type (2.PB.C2S_GS_PROTOC_TYPE:(GPROTOC_PLAYER_SEARCH_CORPS_BATTLE_SCORERtype
detail (:0Rdetail"�
gp_player_send_redenvelopeL
type (2.PB.C2S_GS_PROTOC_TYPE: GPROTOC_PLAYER_SEND_RED_ENVELOPERtype(
redenvelopetype (Rredenvelopetype0
receiver (2.PB.name_roleid_pairRreceiver
content (Rcontent
item_id (RitemId#
item_location (RitemLocation
item_pos (RitemPos"z
gp_player_draw_redenvelopeL
type (2.PB.C2S_GS_PROTOC_TYPE: GPROTOC_PLAYER_DRAW_RED_ENVELOPERtype
id (Rid"{
gp_set_corps_manifestoG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SET_CORPS_MANIFESTORtype
content (Rcontent"�
gp_equip_item>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EQUIP_ITEMRtype
	location1 (R	location1
	location2 (R	location2
index1 (Rindex1
index2 (Rindex2*
transfer_gems (:falseRtransferGems"
	swap_star (:falseRswapStar"q
gp_get_fortune_catC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GET_FORTUNE_CATRtype
roleid (Rroleid"w
gp_extend_fortune_catF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_EXTEND_FORTUNE_CATRtype
roleid (Rroleid"q
gp_get_bind_mobile_phone_awardO
type (2.PB.C2S_GS_PROTOC_TYPE:#GPROTOC_GET_BIND_MOBILE_PHONE_AWARDRtype"�
gp_player_search_rank_infoK
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_PLAYER_SEARCH_RANK_INFORtype
detail (:0Rdetail"�
gp_select_gfx_suitC
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_SELECT_GFX_SUITRtype
gfx_type (RgfxType
level (Rlevel"q
gp_center_faction_battle_awardO
type (2.PB.C2S_GS_PROTOC_TYPE:#GPROTOC_CENTER_FACTION_BATTLE_AWARDRtype"�
gp_child_talk>
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_CHILD_TALKRtype
child_index (R
childIndex
talk_id (RtalkId"k
gp_guaji_fengmo@
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_GUAJI_FENGMORtype
active (Ractive"w
!gp_center_battle_commander_notifyR
type (2.PB.S2C_GS_PROTOC_TYPE:&type_gp_center_battle_commander_notifyRtype"�
gp_adventure_task_listG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_adventure_task_listRtype8
task (2$.PB.gp_adventure_task_list.task_dataRtaskD
	task_data
taskid (Rtaskid
finish_time (R
finishTime"�
gp_adventure_task_finishI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_adventure_task_finishRtype
taskid (Rtaskid
finish_time (R
finishTime"�
gp_battle_try_close_notifyK
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_battle_try_close_notifyRtype
roleid (Rroleid

battle_tid (R	battleTid
	battle_id (RbattleId"�
gp_battle_damage_notifyH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_battle_damage_notifyRtype
roleid (Rroleid

battle_tid (R	battleTid
	battle_id (RbattleId
damage (Rdamage

max_damage (R	maxDamage"�
gp_receive_reward_failG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_receive_reward_failRtype
index (Rindex
tid (Rtid
extra_param (R
extraParam"�
gp_fauction_refresh_resultK
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_refresh_resultRtype
result (Rresult"�
gp_longyu_collectionE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_longyu_collectionRtype:
part (2&.PB.gp_longyu_collection.longyu_part_tRpart,
longyu_part_t
	longyu_id (RlongyuId"�
gp_longyu_unlock_notifyH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_longyu_unlock_notifyRtype
location (Rlocation
index (Rindex
equip_index (R
equipIndex
	longyu_id (RlongyuId"�
gp_longyu_activate_notifyJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_longyu_activate_notifyRtype
equip_index (R
equipIndex
	longyu_id (RlongyuId"�
weather_info
weather (Rweather
temp_min (RtempMin
temp_max (RtempMax
hum_min (RhumMin
hum_max (RhumMax"�
gp_weather_forecast_reD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_weather_forecastRtype
scene (RsceneO
forecast_info (2*.PB.gp_weather_forecast_re.forecast_info_tRforecastInfo
mask (RmaskV
forecast_info_t

begin_time (R	beginTime$
info (2.PB.weather_infoRinfo"�
gp_runinto_npc_res?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_runinto_npcRtype
npc_id (RnpcId
effect (Reffect"�
gp_roll_item_resultD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_roll_item_resultRtype
roll_id (RrollId
item_id (RitemId
	player_id (RplayerId
eva_id (RevaId
point (Rpoint"�
gp_roll_item_responseF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_roll_item_responseRtype
roll_id (RrollId
item_id (RitemId
	player_id (RplayerId
eva_id (RevaId
point (Rpoint
	use_limit (RuseLimit"�
gp_notice_roll_itemsE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_notice_roll_itemsRtype6
item (2".PB.gp_notice_roll_items.roll_itemRitem
pos (2.PB.a3d_posRpos
can_roll (RcanRoll=
	roll_item
roll_id (RrollId
item_id (RitemId"�
gp_matter_enter_sliceF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_matter_enter_sliceRtype3
matter_info (2.PB.gp_matter_infoR
matterInfo"�
gp_wish_tree_irrigate_resultM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_wish_tree_irrigate_resultRtype
result (Rresult
fruit (Rfruit%
irrigate_count (RirrigateCount"�
gp_wish_tree_harvest_resultL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_wish_tree_harvest_resultRtype
result (Rresult"�
gp_wish_tree_dataB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_wish_tree_dataRtype%
irrigate_count (RirrigateCount
fruits (Rfruits
	harvested (R	harvested"�
gp_collection_op_reD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_collection_op_reRtype&
op (2.PB.collection_op_typeRop
retcode (Rretcode3

collection (2.PB.collection_infoR
collection"�
gp_collection_notifyE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_collection_notifyRtype?
op (2/.PB.gp_collection_notify.collection_notify_typeRop5
collections (2.PB.collection_infoRcollections"Y
collection_notify_type
CNT_LIST
CNT_ADD
CNT_TIME_END
CNT_TIME_RENEW"�
gp_set_dir_and_cameraF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_set_dir_and_cameraRtype
dir (Rdir
yaw (Ryaw
pitch (Rpitch"�
gp_duel_info=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_infoRtype
duel_target (R
duelTarget
	duel_type (RduelType
	duel_mode (RduelMode!
duel_timeout (RduelTimeout&
flag_pos (2.PB.a3d_posRflagPos"�
gp_scene_weatherA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_scene_weatherRtype
sceneid (Rsceneid 
weathertype (Rweathertype"�
gp_take_photo_retB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_take_photo_retRtype
op (Rop
	action_id (RactionId
action_time (R
actionTime
role_id (RroleId
ret_code (RretCode"�
gp_intimate_op_reB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_intimate_op_reRtype'
op (2.PB.GS_INTIMATE_OP_TYPERop
targetid (Rtargetid
retcode (Rretcode#
intimate_name (RintimateName"�
gp_fauction_store_updateI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_store_updateRtype
level (Rlevel
exp (Rexp%
total_turnover (RtotalTurnover%
daily_turnover (RdailyTurnover+
yestoday_turnover (RyestodayTurnover#
timeout_count (RtimeoutCount
	daily_exp (RdailyExp#
cansell_count	 (RcansellCount"�
gp_fauction_store_open_resultN
type (2.PB.S2C_GS_PROTOC_TYPE:"type_gp_fauction_store_open_resultRtype
result (Rresult"�
 gp_fauction_store_levelup_resultQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_fauction_store_levelup_resultRtype
result (Rresult
newlevel (Rnewlevel"t
gp_instance_finishC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_instance_finishRtype
result (:0Rresult"�
gp_retinue_cast_skillF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_retinue_cast_skillRtype
	player_id (RplayerId@
retinue (2&.PB.gp_retinue_cast_skill.retinue_infoRretinue
skill_id (RskillId!
skill_target (RskillTarget

skill_type (R	skillTypeg
retinue_info

retinue_id (R	retinueId

fashion_id (R	fashionId
color_id (RcolorId"�
gp_role_set_name_resultH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_role_set_name_resultRtype
name (Rname
errcode (Rerrcode"�
gp_guard_data>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_dataRtype)
data (2.PB.guard_client_dataRdata,
slots (2.PB.guard_slot_essenceRslots"x
gp_guard_get=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_getRtype)
data (2.PB.guard_client_dataRdata"�
gp_guard_name_changeE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_name_changeRtype
	player_id (RplayerId
guard_id (RguardId
name (Rname"�
gp_guard_change_shape_noticeM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_guard_change_shape_noticeRtype
	player_id (RplayerId
guard_id (RguardId
shape (Rshape"�
gp_guard_summon_noticeG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_summon_noticeRtype
	player_id (RplayerId
guard_id (RguardId
guard_shape (R
guardShape

guard_name (R	guardName"�
gp_guard_train_resultF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_train_resultRtype
guard_id (RguardId
phase (Rphase%
train_aptitude (RtrainAptitude
train_count (R
trainCount'
evolution_value (RevolutionValue.
self_fight_capacity (RselfFightCapacity2
attach_fight_capacity (RattachFightCapacity
cur_prop	 (RcurProp
attach_prop
 (R
attachProp.
add_evolution_value (RaddEvolutionValue(
base_attach_prop (RbaseAttachProp;
base_attach_fight_capacity (RbaseAttachFightCapacity"�
gp_guard_change_skill_resultM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_guard_change_skill_resultRtype
retcode (Rretcode
guard_id (RguardId

skill_slot (R	skillSlot
skill_id (RskillId"�
gp_guard_prop_updateE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_prop_updateRtype
guard_id (RguardId.
self_fight_capacity (RselfFightCapacity2
attach_fight_capacity (RattachFightCapacity
cur_prop (RcurProp
attach_prop (R
attachProp(
base_attach_prop (RbaseAttachProp;
base_attach_fight_capacity (RbaseAttachFightCapacity"�
gp_guard_slot_updateE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_slot_updateRtype
element (Relement
guard_id (RguardId
level (Rlevel"�
gp_guard_add_exp_resultH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_add_exp_resultRtype
retcode (Rretcode
guard_id (RguardId
guard_level (R
guardLevel
	guard_exp (RguardExp"�
gp_guard_cast_skillD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_guard_cast_skillRtype
	player_id (RplayerId
guard_id (RguardId
guard_shape (R
guardShape
skill_id (RskillId!
skill_target (RskillTarget
guard_phase (R
guardPhase"J
activated_info
type (Rtype
index (Rindex
tm (Rtm"�
player_chatboxB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_chatboxRtype0
	activated (2.PB.activated_infoR	activated'
selected_bubble (RselectedBubble#
selected_font (RselectedFont/
selected_background (RselectedBackground"�
buff_t
buff_id (RbuffId

buff_level (R	buffLevel!
buff_endtime (RbuffEndtime
	buff_data (RbuffData
	buff_from (RbuffFrom"�
gp_object_buff?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_object_buffRtype
id (:0Rid'
	buff_list (2
.PB.buff_tRbuffList+
mode (2.PB.gp_object_buff.MODERmode"
MODE
ALL 
ONE"�
gp_instance_finished_infoJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_instance_finished_infoRtype!
instance_tid (RinstanceTid
	cost_time (RcostTime

death_time (R	deathTime
score (Rscore
evaluate (Revaluate
reward (Rreward"�
lottery_prize_item_t
tid (Rtid
count (Rcount
bind (Rbind
period (Rperiod
quality (Rquality"�
lottery_reward_t
	bind_cash (RbindCash
trade_money (R
tradeMoney

bind_money (R	bindMoney
exp (Rexp
prof_exp (RprofExp
vp (Rvp
vip_exp (RvipExp.
items (2.PB.lottery_prize_item_tRitems
repus	 (Rrepus
awards
 (RawardsF
global_prize (2#.PB.lottery_reward_t.global_prize_tRglobalPrizeC
world_award (2".PB.lottery_reward_t.world_award_tR
worldAward~
global_prize_t&
global_prize_id (RglobalPrizeId"
can_get_prize (RcanGetPrize 
add_pool_num (R
addPoolNum�
world_award_t&
world_award_tid (RworldAwardTid*
world_award_count (RworldAwardCount/
world_award_speak_id (RworldAwardSpeakId"�
instance_lottery_result
roleid (Rroleid
name (Rname
idphoto (Ridphoto
index (Rindex,
reward (2.PB.lottery_reward_tRreward"�
gp_instance_lucky_draw_infoL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_instance_lucky_draw_infoRtype
brand_count (R
brandCount
timeout (Rtimeout
lottery_tid (R
lotteryTid3
result (2.PB.instance_lottery_resultRresult">

kotodama_t
level (:0Rlevel
index (:0Rindex"�
gp_kotodama_op_resultF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_kotodama_op_resultRtype(
event (2.PB.KOTODAMA_EVENTRevent%
kotodama_index (RkotodamaIndex
result (:0Rresult%
kotodama_point (RkotodamaPoint3
kotodama_data (2.PB.kotodama_tRkotodamaData;
combat_kotodama_selected (:0RcombatKotodamaSelected=
enhance_kotodama_selected (:0RenhanceKotodamaSelected"�
player_enhance_t
type (Rtype
level (Rlevel
cur_exp (RcurExp
gems (Rgems
longyu (Rlongyu"�
gp_enhance_info@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_enhance_infoRtype9
enhance_parts (2.PB.player_enhance_tRenhanceParts7
update (2.PB.gp_enhance_info.UPDATE_TYPERupdate"%
UPDATE_TYPE

UT_ALL

UT_GEM"�
gp_enhance_resultB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_enhance_resultRtype!
enhance_part (RenhancePart
retcode (Rretcode%
direct_levelup (RdirectLevelup
levelup (Rlevelup"G
buff_box_fx_info
roleid (Rroleid
	cost_time (RcostTime"�
gp_buff_box_fx?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_buff_box_fxRtype
	matter_id (RmatterId-
fx_info (2.PB.buff_box_fx_infoRfxInfo"�
gp_enter_pelt_resultE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_enter_pelt_resultRtype
roleid (Rroleid
enter (:0Renter"B
switch_data
switch_type (R
switchType
data (Rdata"v
gp_switch_data?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_switch_dataRtype#
data (2.PB.switch_dataRdata"�
gp_switch_changeA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_switch_changeRtype
switch_type (R
switchType
index (Rindex
open (Ropen"�
gp_npc_play_soundB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_play_soundRtype
npc_id (RnpcId
sound_id (RsoundId"�
gp_npc_affix_data_chgF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_affix_data_chgRtype
newtype (Rnewtype

affix_data (R	affixData":

photo_info
id (Rid
	timestamp (R	timestamp"�
gp_photo_info>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_photo_infoRtype+
	photo_ids (2.PB.photo_infoRphotoIds/
photo_decos (2.PB.photo_infoR
photoDecos!
partial_flag (RpartialFlag"g
gp_counter_data@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_counter_dataRtype
data (Rdata"�
gp_counter_changeB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_counter_changeRtype
index (Rindex
value (Rvalue"�
gp_task_find_npc_resultH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_task_find_npc_resultRtype
result (Rresult
npc_tid (RnpcTid
pos (2.PB.a3d_posRpos"�
gp_social_space_op_reF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_social_space_op_reRtype(
op (2.PB.SOCIAL_SPACE_OP_TYPERop
retcode (Rretcode.
data (2.PB.plat_social_space_dataRdata

popularity (R
popularity

gift_count (R	giftCount

step_award (R	stepAward
target (Rtarget"�
gp_social_space_notifyG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_social_space_notifyRtypeK
notify (23.PB.gp_social_space_notify.SOCIAL_SPACE_NOTIFY_TYPERnotify

style_type (R	styleType
style_id (RstyleId
expire_time (R
expireTime"^
SOCIAL_SPACE_NOTIFY_TYPE
SSNT_STYLE_UNLOCK
SSNT_STYLE_EXPIRE
SSNT_STYLE_RENEW"�
gp_player_refence_infoG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_refence_infoRtype#
referral_code (RreferralCode#
gift_received (RgiftReceived#
cash_received (RcashReceived

gift_total (R	giftTotal"n
bless_record
	timestamp (R	timestamp
roleid (Rroleid
name (Rname
index (Rindex"�
player_slave
index (Rindex
bless (Rbless
favor (Rfavor
mood (Rmood
active (Ractive%
mood_timestamp (RmoodTimestamp"f
player_slave_t(
slaves (2.PB.player_slaveRslaves*
records (2.PB.bless_recordRrecords"�
gp_player_slave_op_reF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_slave_op_reRtype2
op (2".PB.gp_player_slave_op_re.SLAVE_OPRop
roleid (Rroleid
slave_index (R
slaveIndex
ret (Rret&
data (2.PB.player_slave_tRdata"Y
SLAVE_OP
SLAVE_OP_BLESS
SLAVE_OP_WORK
SLAVE_OP_GIFT
SLAVE_OP_UPDATE"�
SLAVE_OP_RE
SUCCESS 
SLAVE_BLESS_FULL
OWNER_BLESS_LIMIT
GUEST_BLESS_LIMIT
GUEST_START_WORK_ERR
SLAVE_BLESS_NOT_FULL
GUEST_GIFT_ERR
	GIFT_LESS
WORK_REWARD_ERR
SLAVE_ALREADY_WORK	
HIGH_BLESS_LESS_CASH

SLAVE_FAVOR_FULL

GIFT_LIMIT
OP_PARAM_ERR"�
$gp_extend_fashion_color_limit_resultU
type (2.PB.S2C_GS_PROTOC_TYPE:)type_gp_extend_fashion_color_limit_resultRtype#
fashion_index (RfashionIndex
color_limit (R
colorLimit
result (Rresult"�
gp_child_divorce_notifyH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_divorce_notifyRtype6
cmd (2$.PB.gp_child_divorce_notify.CMD_TYPERcmd
	initiator (R	initiator
	init_guid (RinitGuid
reply (Rreply
guid (Rguid
retcode (Rretcode"X
CMD_TYPE
DIVORCE_INVITE
DIVORCE_REPLY
DIVORCE_FORCE
DIVORCE_RESULT"�
gp_child_marriage_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_marriage_resultRtype
result (Rresult
	initiator (R	initiator
	responder (R	responder
	init_guid (RinitGuid
	resp_guid (RrespGuid"�
!gp_child_marriage_send_invite_resR
type (2.PB.S2C_GS_PROTOC_TYPE:&type_gp_child_marriage_send_invite_resRtype
result (Rresult"_
inlaws_type
guid (Rguid
	inlaws_id (RinlawsId
inlaws_name (R
inlawsName"�
gp_child_marriage_inlaws_infoN
type (2.PB.S2C_GS_PROTOC_TYPE:"type_gp_child_marriage_inlaws_infoRtype0
inlaws_list (2.PB.inlaws_typeR
inlawsList"�
child_stub_type
tid (Rtid
guid (Rguid
	mother_id (RmotherId
	father_id (RfatherId
part (Rpart
name (Rname
fight (Rfight"�
gp_child_marriage_stateH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_marriage_stateRtype
	initiator (R	initiator
	responder (R	responder
state (Rstate
match_value (R
matchValue/
children (2.PB.child_stub_typeRchildren"�
gp_child_marriage_startH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_marriage_startRtype
	initiator (R	initiator
	responder (R	responder"�
gp_child_marriage_invite_resultP
type (2.PB.S2C_GS_PROTOC_TYPE:$type_gp_child_marriage_invite_resultRtype
result (Rresult"�
gp_child_marriage_inviteI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_marriage_inviteRtype
	initiator (R	initiator%
initiator_name (RinitiatorName"�
gp_wedding_snatch_notifyI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_wedding_snatch_notifyRtype
rival_id (RrivalId

rival_name (R	rivalName"�
 gp_check_wedding_snatch_cond_resQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_check_wedding_snatch_cond_resRtype
retcode (Rretcode#
target_roleid (RtargetRoleid

groom_name (R	groomName

bride_name (R	brideName
	timestamp (R	timestamp"�
gp_center_battle_prizeG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_center_battle_prizeRtype

prize_type (R	prizeType
prize_value (R
prizeValue"�
gp_illegal_report_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_illegal_report_resultRtype
result (Rresult#
target_roleid (RtargetRoleid"�
gp_rename_informA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_rename_informRtypeF
rename_inform_data (2.PB.rename_inform_data_tRrenameInformData"�
gp_update_roam_battle_kill_infoP
type (2.PB.S2C_GS_PROTOC_TYPE:$type_gp_update_roam_battle_kill_infoRtype)
consecutive_kill (RconsecutiveKill
first_blood (R
firstBlood0
end_consecutive_kill (RendConsecutiveKill"�
gp_active_surface_color_resultO
type (2.PB.S2C_GS_PROTOC_TYPE:#type_gp_active_surface_color_resultRtype
result (Rresult
surface_tid (R
surfaceTid!
surface_part (RsurfacePart
color_index (R
colorIndex"�
gp_paint_surface_color_resultN
type (2.PB.S2C_GS_PROTOC_TYPE:"type_gp_paint_surface_color_resultRtype
result (Rresult
surface_tid (R
surfaceTid!
surface_part (RsurfacePart
color_index (R
colorIndex/
color_concentration (RcolorConcentration"
gp_change_photoid_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_change_photoid_resultRtype
photoid (Rphotoid"�
gp_dice_result?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_dice_resultRtype
candice (Rcandice
step (Rstep
realstep (Rrealstep
newpos (Rnewpos
	missionid (R	missionid
dicetype (Rdicetype"G
lottery_prize
item_id (RitemId

item_count (R	itemCount"�
gp_lottery_one_button_resultM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_lottery_one_button_resultRtype
item_id (RitemId
location (Rlocation
index (Rindex)
prizes (2.PB.lottery_prizeRprizes"�
gp_player_guaji_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_guaji_resultRtype
start_guaji (R
startGuaji
	quit_mode (RquitMode$
half_quit_mode (RhalfQuitMode"�
gp_center_battle_endE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_center_battle_endRtype#
battle_result (RbattleResult
players (Rplayers"�
!gp_player_search_rank_info_resultR
type (2.PB.S2C_GS_PROTOC_TYPE:&type_gp_player_search_rank_info_resultRtype
detail (:0Rdetail
data (Rdata 

self_score (:0R	selfScore-
self_gather_score (:0RselfGatherScore
	self_rank (:0RselfRank
	self_kill (:0RselfKill5
self_secondary_attack (:0RselfSecondaryAttack'
self_do_damage	 (:0RselfDoDamage#
self_be_hurt
 (:0R
selfBeHurt#
self_do_heal (:0R
selfDoHeal"�
gp_sect_mentor_title_notifyL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_sect_mentor_title_notifyRtype
roleid (Rroleid
title (Rtitle"�
 gp_revenge_battle_enter_instanceQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_revenge_battle_enter_instanceRtype"
wait_time_out (RwaitTimeOut

is_inviter (R	isInviter"�
!gp_revenge_battle_response_resultR
type (2.PB.S2C_GS_PROTOC_TYPE:&type_gp_revenge_battle_response_resultRtype
result (Rresult
enemy_id (RenemyId"{
gp_revenge_battle_infoG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_revenge_battle_infoRtype
timeout (Rtimeout"�
gp_revenge_battle_letterI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_revenge_battle_letterRtype,
enemy (2.PB.battle_player_infoRenemy
timeout (Rtimeout"�
gp_revenge_battle_apply_resultO
type (2.PB.S2C_GS_PROTOC_TYPE:#type_gp_revenge_battle_apply_resultRtype
retcode (Rretcode"n
gp_duel_invite?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_inviteRtype
	player_id (RplayerId"�
gp_duel_invite_replyE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_invite_replyRtype
	player_id (RplayerId
reply (Rreply"�
gp_duel_prepare@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_prepareRtype
	player_id (RplayerId
delay (Rdelay"n
gp_duel_cancel?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_cancelRtype
	player_id (RplayerId"�
gp_duel_start>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_startRtype
	player_id (RplayerId
	target_id (RtargetId
	duel_type (RduelType&
flag_pos (2.PB.a3d_posRflagPos"�
gp_duel_stop=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_stopRtype
	player_id (RplayerId
	target_id (RtargetId"|
gp_duel_out_of_rangeE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_out_of_rangeRtype

count_down (R	countDown"�
gp_duel_result?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_duel_resultRtype
id1 (Rid1
id2 (Rid2
result (Rresult
name1 (Rname1
name2 (Rname2"�
gp_child_request_pregnateJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_request_pregnateRtype
	player_id (RplayerId
use_item (RuseItem"�
 gp_child_request_pregnate_resultQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_child_request_pregnate_resultRtype
id1 (Rid1
id2 (Rid2
result (Rresult"�
PREGNATE_ERROR_CODE
SUCCESS 

CHILD_FULL
SPOUSE_REFUSED

FAILED
ALREADY_PREGNATED
ACTIVITY_NOT_ENGOUH
IN_COOL_DOWN
TWO_FAR_AWAY"�
gp_child_request_deliveryJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_request_deliveryRtype
	player_id (RplayerId"�
 gp_child_request_delivery_resultQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_child_request_delivery_resultRtype
id1 (Rid1
id2 (Rid2
result (Rresult"�
DELIVERY_ERROR_CODE
SUCCESS 

CHILD_FULL
SPOUSE_REFUSED
NOT_PREGNATED
ACTIVITY_NOT_ENGOUH
BABY_PROGRESS_NOT_FULL"�
gp_child_adoption_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_adoption_resultRtype
result (Rresult"�
ADOPTION_ERROR_CODE
SUCCESS 

CHILD_FULL
SILVER_NOT_ENOUGH
YUANBAO_NOT_ENOUGH
ALREADY_ADOPT_TODAY
LEVEL_NOT_ENGOUH"y
gp_marriage_anniversaryH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_marriage_anniversaryRtype
index (Rindex"�
gp_fortune_cat_infoD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fortune_cat_infoRtype
count (Rcount

begin_time (R	beginTime
end_time	 (RendTime*
extend_money_type
 (RextendMoneyType*
extend_money_cost (RextendMoneyCost"w
gp_fortune_cat_resultF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fortune_cat_resultRtype
result (Rresult"�
gp_self_enter_worldD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_self_enter_worldRtype
	world_tid (RworldTid
line_id (RlineId
world_id (RworldId
gmt (Rgmt"�
gp_scene_info>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_scene_infoRtype
	mirror_id	 (RmirrorId
all_mirrors (R
allMirrors1
params (2.PB.gp_scene_info.param_tRparams
lineid (Rlineid
mask (Rmask
	scene_tid (RsceneTid

server_mod (R	serverMod

time_still (R	timeStill,
close_warning_time
 (RcloseWarningTime&
close_wait_time (RcloseWaitTime5
param_t
index (Rindex
param (Rparam"�
gp_creature_base_info
	clothesid (R	clothesid
idphoto (Ridphoto
faceid (Rfaceid
hairid (Rhairid
	haircolor (R	haircolor
	skincolor (R	skincolor
beardid (Rbeardid
tattoo (Rtattoo
sharp	 (Rsharp
level
 (Rlevel
prof (Rprof

prof_level (R	profLevel%
appearance_crc (RappearanceCrc
earid (Rearid
tailid (Rtailid"�
gp_player_extend_state!
extend_state (RextendState#
visual_effect (RvisualEffect
action_type (R
actionType

action_arg (R	actionArg&
body_size_scale (RbodySizeScale)
player_signature (RplayerSignature
mafia_id (RmafiaId

mafia_rank	 (R	mafiaRank!
mafia_domain
 (RmafiaDomain
mafia_owner (R
mafiaOwner
title_id (RtitleId
tilte (Rtilte
	spouse_id (RspouseId
teamid (Rteamid
pos_in_xyxw (R	posInXyxw
xyxw_id (RxyxwId#
transform_tid (RtransformTid#
native_zoneid (RnativeZoneid"�
gp_equip_starup_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_equip_starup_resultRtype
location (Rlocation
index (Rindex
result (Rresult"�
gp_child_starup_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_starup_resultRtype
index (Rindex
slot (Rslot
result (Rresult":

addon_info
addonid (Raddonid
rand (Rrand"�
gp_equip_baseattr_refine_resultP
type (2.PB.S2C_GS_PROTOC_TYPE:$type_gp_equip_baseattr_refine_resultRtype
location (Rlocation
index (Rindex
result (Rresult&
addons (2.PB.addon_infoRaddons"�
 gp_equip_extraattr_recast_resultQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_equip_extraattr_recast_resultRtype
location (Rlocation
index (Rindex
reply (Rreply"�
gp_exp_lottery_resultF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_exp_lottery_resultRtype
can_free (RcanFree
can_get (RcanGet
new_lottery (R
newLottery"�
fake_auction_info
aid (Raid
create_time (R
createTime
end_time (RendTime
itemid (Ritemid
price (Rprice!
recomm_price (RrecommPrice!
normal_count (RnormalCount

bind_count (R	bindCount
sellout	 (Rsellout"�
gp_fauction_open_resultH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_open_resultRtype
retcode (Rretcode)
info (2.PB.fake_auction_infoRinfo"�
gp_fauction_reopen_resultJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_reopen_resultRtype
retcode (Rretcode)
info (2.PB.fake_auction_infoRinfo"�
gp_fauction_get_money_resultM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_fauction_get_money_resultRtype
retcode (Rretcode

auction_id (R	auctionId"�
gp_fauction_buy_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_buy_resultRtype
retcode (Rretcode

auction_id (R	auctionId"�
gp_fauction_close_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_close_resultRtype
retcode (Rretcode

auction_id (R	auctionId"�
gp_fauction_selflist_resultL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_fauction_selflist_resultRtype
pageid (Rpageid!
total_amount (RtotalAmount+
items (2.PB.fake_auction_infoRitems"�
gp_fauction_state_notifyI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fauction_state_notifyRtype
state (Rstate

auction_id (R	auctionId
tid (Rtid
price (Rprice"t
gp_gain_surface@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_gain_surfaceRtype
surface_tid (R
surfaceTid"�
gp_revive_times_infoE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_revive_times_infoRtype,
stand_revive_times (RstandReviveTimes9
stand_revive_times_lianxu (RstandReviveTimesLianxu0
perfect_revive_times (RperfectReviveTimes=
perfect_revive_times_lianxu (RperfectReviveTimesLianxu"�
gp_item_combine_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_item_combine_resultRtype
stamp (Rstamp
result (Rresult"�
gp_nation_escort_mountG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_nation_escort_mountRtype
roleid (Rroleid
npcid (Rnpcid
mount (Rmount"�
gp_nation_escort_locateH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_nation_escort_locateRtype
npcid (Rnpcid
	scene_tag (RsceneTag
mirrorid (Rmirrorid
pos (2.PB.a3d_posRpos"u
gp_escort_speed_stateF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_escort_speed_stateRtype
state (Rstate"�
gp_transform_stateC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_transform_stateRtype
roleid (Rroleid
tid (Rtid"�
gp_broadcast_value_changeJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_broadcast_value_changeRtype
roleid (RroleidG

value_type (2(.PB.gp_broadcast_value_change.VALUE_TYPER	valueType
	new_value (RnewValue"

VALUE_TYPE
VT_DUKE_LEVEL"�
gp_object_change_factionI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_object_change_factionRtype
id (Rid
faction (Rfaction"�
gp_wine_info=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_wine_infoRtype$
wine_config_id (RwineConfigId*
wine_remain_drink (RwineRemainDrink"�
gp_minigame_operate_notifyK
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_minigame_operate_notifyRtype
oper_man (RoperMan
oper (Roper
param1 (Rparam1
param2 (Rparam2
data (Rdata"�
gp_equip_transfer_reulstI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_equip_transfer_resultRtype!
src_location (RsrcLocation
	src_index (RsrcIndex!
dst_location (RdstLocation
	dst_index (RdstIndex
result (Rresult"�
gp_equip_refine_reulstG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_equip_refine_resultRtype#
main_location (RmainLocation

main_index (R	mainIndex+
material_location (RmaterialLocation%
material_index (RmaterialIndex
result (Rresult!
random_index (RrandomIndex"�
gp_equip_attach_gem_resultK
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_equip_attach_gem_resultRtype
equip_index (R
equipIndex

hole_index (R	holeIndex
result (Rresult"�
gp_equip_detach_gem_resultK
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_equip_detach_gem_resultRtype
equip_index (R
equipIndex

hole_index (R	holeIndex
result (Rresult"�
"gp_equip_attach_gem_upgrade_resultS
type (2.PB.S2C_GS_PROTOC_TYPE:'type_gp_equip_attach_gem_upgrade_resultRtype
location (Rlocation
equip_index (R
equipIndex

hole_index (R	holeIndex
result (Rresult"�
gp_equip_skill_transfer_resL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_equip_skill_transfer_resRtype
src_tid (RsrcTid"
src_skill_tid (RsrcSkillTid
dst_tid (RdstTid"
dst_skill_tid (RdstSkillTid"�
gp_equip_affixes_transfer_resN
type (2.PB.S2C_GS_PROTOC_TYPE:"type_gp_equip_affixes_transfer_resRtype
src_tid (RsrcTid
src_type (RsrcType
	src_value (RsrcValue
dst_tid (RdstTid
dst_type (RdstType
	dst_value (RdstValue"�
	gp_pk_man:
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pk_manRtype
roleid (Rroleid
pk_level (RpkLevel

pk_setting (R	pkSetting
	pvp_state (RpvpState"�
gp_fight_back_listC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fight_back_listRtype
insert (Rinsert
roleid (Rroleid"�
gp_auto_reward_listD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_auto_reward_listRtype:
entry (2$.PB.gp_auto_reward_list.reward_entryRentryN
reward_entry
type (Rtype
reward (Rreward
data (Rdata"�
gp_vip_info<
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_vip_infoRtype"
cur_vip_level (RcurVipLevel(
reward_vip_level (RrewardVipLevel-
reward_vip_end_time (RrewardVipEndTime
new_id (RnewId(
system_close (:falseRsystemClose1
buy_force_times_today (RbuyForceTimesToday3
online (2.PB.gp_vip_info.online_infoRonline(
vip_upgrade_time	 (RvipUpgradeTime
vip_exp
 (RvipExp
hide (:falseRhide"
	hide_name (:falseRhideName�
online_info
save_second (R
saveSecond%
offline_second (RofflineSecond&
logout_exp_last (RlogoutExpLast'
login_timestamp (RloginTimestamp#
online_second (RonlineSecond"3
relation_member
pos (Rpos
id (Rid"�
player_extend_state!
extend_state (RextendState.
extend_state_params (RextendStateParams
action_type (R
actionType

action_arg (R	actionArg2
interact_target_newid (RinteractTargetNewid
mafia_id (RmafiaId

mafia_rank (R	mafiaRank
title_id (RtitleId

title_data	 (R	titleData
	spouse_id
 (RspouseId
team_id (RteamId.
nation_escort_mount (RnationEscortMount(
nation_escort_id (RnationEscortId
	mount_tid (RmountTid#
mount_quality (RmountQuality#
transform_tid (RtransformTid)
flysword_surface (RflyswordSurfaceQ
flysword_surface_color_parts (2.PB.color_info_tRflyswordSurfaceColorPartsC
flysword_surface_fashion_parts (RflyswordSurfaceFashionParts4
flysword_surface_level (RflyswordSurfaceLevel
pos_in_xyxw (R	posInXyxw6
xyxw_members (2.PB.relation_memberRxyxwMembers
	xyxw_type (RxyxwType
xyxw_param1 (R
xyxwParam1 
star_suit_id (R
starSuitId
parade_type (R
paradeType#
parade_spouse (RparadeSpouse<
snatch_protected_timestamp (RsnatchProtectedTimestamp
	child_tid (RchildTid
child_stage (R
childStage

child_name (R	childName

child_hair  (R	childHair#
child_clothes! (RchildClothes!
child_weapon" (RchildWeapon*
child_spouse_guid# (RchildSpouseGuid*
child_spouse_name$ (RchildSpouseName
bind_param1% (R
bindParam1
bind_param2& (R
bindParam2
bind_param3' (R
bindParam3
pos_in_bind( (R	posInBind6
bind_members) (2.PB.relation_memberRbindMembers
guard_id* (RguardId
guard_shape+ (R
guardShape

guard_name, (R	guardName&
flysword_is_fly- (RflyswordIsFly
xyxw_param2. (R
xyxwParam2$
mafia_pos_type/ (RmafiaPosType,
mafia_support_side0 (RmafiaSupportSide(
mafia_misc_index1 (RmafiaMiscIndex/
take_photo_action_id2 (RtakePhotoActionId3
take_photo_action_time3 (RtakePhotoActionTime&
mafia_spec_rank4 (RmafiaSpecRank"i
region_notify_data

percent_hp (R	percentHp

percent_sp (R	percentSp
inc_sp (RincSp"�
player_definite_info_common
roleid (Rroleid
newobjid (Rnewobjid
	use_alias (RuseAlias
player_name (R
playerName!
player_alias (RplayerAlias
idphoto (Ridphoto
race (Rrace
gender (Rgender
level	 (Rlevel
prof
 (Rprof
pos (2.PB.a3d_posRpos
dir (Rdir
faction (Rfaction
pk_level (RpkLevel
crc (Rcrc%
appearance_crc (RappearanceCrc!
combat_state (RcombatState#
control_state (RcontrolState7
prop_notify (2.PB.region_notify_dataR
propNotify
	nation_id (RnationId
face_id (RfaceId
body_physic (R
bodyPhysic
	weapon_id (RweaponId
weapon_star (R
weaponStar
wing_id (RwingId
	vip_level (RvipLevel

duke_level (R	dukeLevel
fashions (Rfashions%
gem_gfx_suit_id (RgemGfxSuitId"
fake_roleid (:0R
fakeRoleid

pk_setting (R	pkSetting"�
gp_self_definite_infoF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_self_definite_infoRtype
roleid (Rroleid
newobjid (Rnewobjid
level (Rlevel
prof (Rprof
exp (Rexp
force (Rforce

stored_exp (R	storedExp
pos	 (2.PB.a3d_posRpos
dir
 (Rdir
faction (Rfaction
pk_level (RpkLevel
crc (Rcrc!
combat_state (RcombatState"
cur_target_id (RcurTargetId#
control_state (RcontrolState
	nation_id (RnationId
face_id (RfaceId
body_physic (R
bodyPhysic
	weapon_id (RweaponId
weapon_star (R
weaponStar
	vip_level (RvipLevel

duke_level (R	dukeLevel
state (Rstate
fashions (Rfashions:
extend_state (2.PB.player_extend_stateRextendState%
gem_gfx_suit_id (RgemGfxSuitId"�
gp_player_definite_infoH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_definite_infoRtype
state (Rstate@
common_info (2.PB.player_definite_info_commonR
commonInfo:
extend_state (2.PB.player_extend_stateRextendState"�
gp_player_definite_info_listM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_player_definite_info_listRtype8
	info_list (2.PB.gp_player_definite_infoRinfoList"�
gp_player_enter_sceneF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_enter_sceneRtype/
data (2.PB.gp_player_definite_infoRdata"�
gp_player_enter_sliceF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_enter_sliceRtype/
data (2.PB.gp_player_definite_infoRdata"�
npc_definite_info_common
id (Rid
newobjid (Rnewobjid
level (Rlevel
vis_tid (RvisTid
pos (2.PB.a3d_posRpos
faction (Rfaction
crc (Rcrc
dir (Rdir!
combat_state	 (RcombatState#
control_state
 (RcontrolState7
prop_notify (2.PB.region_notify_dataR
propNotify)
can_not_be_attack (RcanNotBeAttack'
	buff_list (2
.PB.buff_tRbuffList"�
npc_extend_state.
nation_escort_mount (RnationEscortMount
	master_id (RmasterId
player_name (R
playerName
owner_id (RownerId"
owner_team_id (RownerTeamId#
transform_tid (RtransformTid
talent_type (R
talentType
npc_name (RnpcName

affix_data	 (R	affixData
bind_param1
 (R
bindParam1
bind_param2 (R
bindParam2
bind_param3 (R
bindParam3
pos_in_bind (R	posInBind6
bind_members (2.PB.relation_memberRbindMembers,
interact_target_id (RinteractTargetId%
interact_speed (RinteractSpeed0
interact_active_time (RinteractActiveTime"�
gp_npc_definite_infoE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_definite_infoRtype
state (Rstate
tid (RtidG
player_com_info (2.PB.player_definite_info_commonRplayerComInfoA
player_ext_state (2.PB.player_extend_stateRplayerExtState>
npc_com_info (2.PB.npc_definite_info_commonR
npcComInfo8
npc_ext_state (2.PB.npc_extend_stateRnpcExtState"�
gp_npc_definite_info_listJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_definite_info_listRtype5
	info_list (2.PB.gp_npc_definite_infoRinfoList"�
gp_npc_enter_sliceC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_enter_sliceRtype9
protoc_data (2.PB.gp_npc_definite_infoR
protocData"�
gp_npc_enter_sceneC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_enter_sceneRtype
init_action (R
initAction9
protoc_data (2.PB.gp_npc_definite_infoR
protocData"�
marriage_lock_t
timeout (Rtimeout
groom_id (RgroomId

groom_name (R	groomName
bride_id (RbrideId

bride_name (R	brideName"�
gp_matter_info?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_matter_infoRtype
id (Rid
newobjid (Rnewobjid
tid (Rtid
pos (2.PB.a3d_posRpos
dir1 (Rdir1
dir (Rdir
rad (Rrad
status	 (Rstatus
matter_type
 (R
matterType(
can_gather_count (RcanGatherCount
tick (Rtick
from_who (RfromWho!
loot_quality (RlootQuality!
loot_special (RlootSpecial

owner_type (R	ownerType
owner_id (RownerId
	dyn_scale (RdynScale
bind_tid (RbindTid
money_count (R
moneyCount

money_type (R	moneyType8
marriage_lock (2.PB.marriage_lock_tRmarriageLock,
interact_target_id (RinteractTargetId%
interact_speed (RinteractSpeed0
interact_active_time (RinteractActiveTime"�
gp_matter_info_listD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_matter_info_listRtype/
	info_list (2.PB.gp_matter_infoRinfoList"�
gp_matter_enter_sceneF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_matter_enter_sceneRtype3
matter_info (2.PB.gp_matter_infoR
matterInfo"�
gp_scene_special_object_infoM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_scene_special_object_infoRtypeG
	info_type (2*.PB.gp_scene_special_object_info.INFO_TYPERinfoType<
player_info (2.PB.gp_player_definite_infoR
playerInfo3
npc_info (2.PB.gp_npc_definite_infoRnpcInfo3
matter_info (2.PB.gp_matter_infoR
matterInfo";
	INFO_TYPE
INFO_PLAYER
INFO_NPC
INFO_MATTER"�
gp_object_state@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_object_stateRtype
roleid (Rroleid!
object_state (RobjectState
	mount_tid (RmountTid(
clear_quit_state (RclearQuitState#
mount_quality (RmountQualityQ
flysword_surface_color_parts (2.PB.color_info_tRflyswordSurfaceColorPartsC
flysword_surface_fashion_parts (RflyswordSurfaceFashionParts&
flysword_is_fly	 (RflyswordIsFly#
surface_level
 (RsurfaceLevel
parade_type (R
paradeType#
parade_spouse (RparadeSpouse<
snatch_protected_timestamp (RsnatchProtectedTimestamp*
child_spouse_guid (RchildSpouseGuid*
child_spouse_name (RchildSpouseName"�
gp_family_mafia_infoE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_family_mafia_infoRtype
	player_id (RplayerId
mafia_id (RmafiaId

mafia_rank (R	mafiaRank$
mafia_pos_type (RmafiaPosType
mafia_owner (R
mafiaOwner#
mafia_support (RmafiaSupport(
mafia_misc_index	 (RmafiaMiscIndex&
mafia_spec_rank
 (RmafiaSpecRank"�
gp_flysword_defined_infoI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_flysword_defined_infoRtype
roleid (RroleidF
flysword (2*.PB.gp_flysword_defined_info.flysword_infoRflysword�
flysword_info
index (Rindex
tid (Rtid
surface_tid (R
surfaceTid
train_level (R
trainLevel
	train_num (RtrainNum
is_summoned (R
isSummoned"�
surface_info
tid (Rtid:

part_color (2.PB.surface_info.color_infoR	partColor
colors (Rcolors
color_count (R
colorCount@
fashion_part (2.PB.surface_info.fashion_infoRfashionPart
train_level (R
trainLevel

train_nums (R	trainNums
is_fly (RisFly
	cur_skill	 (:0RcurSkillZ

color_info
color_index (R
colorIndex+
concentration_vec (RconcentrationVec]
fashion_info*
cur_fashion_index (RcurFashionIndex!
own_fashions (RownFashions"R
gp_hero_dyn_surface
tid (Rtid)
expire_timestamp (RexpireTimestamp"�
gp_hero_defined_infoE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hero_defined_infoRtype
roleid (Rroleid#
heros (2.PB.hero_infoRheros
cmd_type (RcmdType"�
gp_hero_gain_surface_notifyL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_hero_gain_surface_notifyRtype1
surface (2.PB.gp_hero_dyn_surfaceRsurface"�

gp_hero_incre_infoC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hero_incre_infoRtype
index (Rindex:
cmd_type (2.PB.gp_hero_incre_info.CMD_TYPERcmdType
quality (Rquality
exp (Rexp
level (Rlevel
prof_exp (RprofExp

prof_level (R	profLevel
is_summoned	 (R
isSummoned
value
 (Rvalue
is_level_up (R	isLevelUp
train_level (R
trainLevel
	train_num (RtrainNum
cur_surface (R
curSurface

prop_index (R	propIndex
add (Radd"
one_button_up (RoneButtonUp
surfaces (Rsurfaces$
info (2.PB.surface_infoRinfo
color_index (R
colorIndex/
color_concentration (RcolorConcentration

color_part (R	colorPart

surface_id (R	surfaceId2
surface_fashion_index (RsurfaceFashionIndex:
surface_fashion_blueprint (RsurfaceFashionBlueprint"�
CMD_TYPE
MODIFY_PROF_EXP

MODIFY_EXP
PROF_LEVEL_UP
SUMMON_HERO
FLYSWORD_TRAIN
CHANGE_SURFACE
ADD_AND_DEL_SURFACE
FLYSWORD_UPGRADE
SURFACE_ACTIVE_COLOR	
SURFACE_PAINT_COLOR

SURFACE_TRAIN
FLYSWORD_TRAIN_EXCHANGE
SURFACE_SELECT
SURFACE_ACTIVE_FASHION
SURFACE_SELECT_FASHION
SWITCH_FLYMODE
SWITCH_SKILL
SURFACE_TIMEOUT"_

enemy_info
roleid (Rroleid
	timestamp (R	timestamp
	role_name (RroleName"�
gp_enemy_list>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_enemy_listRtype
mode (Rmode/
enemy_infos (2.PB.enemy_infoR
enemyInfos"�
gp_auto_combat_config_s2cC
type (2.PB.S2C_GS_PROTOC_TYPE:type_auto_combat_configRtype.
config (2.PB.auto_combat_configRconfig"�
gp_lottery_prizeA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_lottery_prizeRtype
tid (Rtid

bind_money (R	bindMoney
trade_money (R
tradeMoney
prof_exp (RprofExp
exp (Rexp3
repus (2.PB.gp_lottery_prize.repu_strRrepus3
itmes (2.PB.gp_lottery_prize.item_strRitmes
vp	 (Rvp&
cur_service_tid
 (RcurServiceTid
vip_exp (RvipExpA
repu_str
repuid (Rrepuid

repu_value (R	repuValueL
item_str
tid (Rtid
count (Rcount
quality (Rquality"�
gp_suit_info=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_suit_infoRtype)
enhance_suit_id (:0RenhanceSuitId!
gem_suit_id (:0R	gemSuitId
is_init (:falseRisInit%
object_new_id (:0RobjectNewId"�
gp_attack_loop?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_attack_loopRtype"
object_new_id (RobjectNewId
start (Rstart
skill (Rskill"
target_new_id (RtargetNewId"�
gp_operation_resultD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_operation_resultRtype>
	oper_type (2!.PB.gp_operation_result.OPER_TYPERoperTypeD
oper_result (2#.PB.gp_operation_result.OPER_RESULTR
operResult"!
	OPER_TYPE
OT_NATION_DONATE"F
OPER_RESULT
OR_NATION_DONATE_SUCCESS
OR_NATION_DONATE_FAIL"�
gp_start_cg<
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_start_cgRtype
cg_id (RcgId&
stop_attack (:falseR
stopAttack"�

gp_stop_cg;
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_stop_cgRtype
cg_id (RcgId2
cg_player_complete (:trueRcgPlayerComplete"�
player_stune_config_t'
cur_active_set (:1RcurActiveSetJ
all_config_set (2$.PB.player_stune_config_t.config_setRallConfigSetw

config_set
	set_index (RsetIndex
skill_id (RskillId
locked (:falseRlocked
name (Rname"�
gp_player_stune_config@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_stunt_configRtype1
config (2.PB.player_stune_config_tRconfig"�
gp_stunt_config_resD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_stunt_config_resRtype
retcode (Rretcode%
op (2.PB.STUNT_CONFIG_TYPERop"Q
saint_animal_info&
saint_animal_id (RsaintAnimalId
level (Rlevel"�
gp_talisman_data_notifyH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_talisman_data_notifyRtypeC
notify_type (2".PB.gp_talisman_data_notify.N_TYPER
notifyType/
	talismans (2.PB.talisman_infoR	talismans5
combinations (2.PB.talisman_infoRcombinations:
saint_animals (2.PB.saint_animal_infoRsaintAnimals7
saint_animal_group_level (RsaintAnimalGroupLevel"�
N_TYPE
N_TYPE_SYNCALL

N_TYPE_ADD
N_TYPE_DECOMPOSE
N_TYPE_COMPOSE

N_TYPE_DEL
N_TYPE_COMBINATE
N_TYPE_ANIMAL_INFO"X
retinue_group_info(
retinue_group_id (RretinueGroupId
quality (Rquality"�
retinue_prop
att_prop (RattProp
str_prop (RstrProp
int_prop (RintProp
spt_prop (RsptProp
wll_prop (RwllProp"�
retinue_client_info-
	base_info (2.PB.retinue_infoRbaseInfo
prop (Rprop!
upgrade_prop (RupgradeProp%
fight_capacity (RfightCapacity"�
retinue_task_award_info
taskid (:0Rtaskid
success (:0Rsuccess
awardid (Rawardid"
retinue_exp (:0R
retinueExp!
private_item (RprivateItem"�	
gp_retinue_data_notifyG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_retinue_data_notifyRtypeB
notify_type (2!.PB.gp_retinue_data_notify.N_TYPER
notifyType=
retinue_groups (2.PB.retinue_group_infoRretinueGroups3
retinues (2.PB.retinue_client_infoRretinues

retinue_id (R	retinueId)
task	 (2.PB.retinue_task_infoRtask1
award
 (2.PB.retinue_task_award_infoRaward4
privates (2.PB.retinue_private_infoRprivates
param1 (Rparam1
param2 (Rparam2.
ex_task (2.PB.retinue_task_infoRexTask
	timestamp (R	timestamp+
gifts (2.PB.retinue_gift_infoRgifts"�
N_TYPE
N_TYPE_SYNC_RETINUE
N_TYPE_SYNC_GROUP
N_TYPE_ADD_GROUP
N_TYPE_ADD_RETINUE
N_TYPE_RETINUE_UP_QUALITY
N_TYPE_SYNC_RETINUE_ONE
N_TYPE_ADD_TASK	
N_TYPE_DEL_TASK

N_TYPE_UPGRADE_GROUP
N_TYPE_SELECT_COMBAT
N_TYPE_ADD_PRIVATE
N_TYPE_ACTIVE_PRIVATE
N_TYPE_GAIN_EXP
N_TYPE_DECOMPOSE
N_TYPE_SELECT_FASHION
N_TYPE_RECRUIT
N_TYPE_EX_TASK_REFRESH
N_TYPE_GIVE_GIFT
N_TYPE_TAKE_GIFT
N_TYPE_MOOD_CHANGE
N_TYPE_GIFT_ADD
N_TYPE_CHAT"i
practice_info*
practice_skill_id (RpracticeSkillId
level (:0Rlevel
exp (:0Rexp"�
gp_practice_data_notifyH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_practice_data_notifyRtypeC
notify_type (2".PB.gp_practice_data_notify.N_TYPER
notifyType/
	practices (2.PB.practice_infoR	practices"<
N_TYPE
N_TYPE_SYNC_PRACTICE
N_TYPE_PRACTICE_RSLT"�
gp_double_exp_disable_notifyM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_double_exp_disable_notifyRtype

is_disable (R	isDisable"z
gp_load_protoc_finishF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_load_protoc_finishRtype
is_login (RisLogin"�
gp_player_corp_configF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_crop_configRtype6
config (2.PB.player_corps_attr_config_tRconfig"�
gp_net_error_messageE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_net_error_messageRtype+
for_ipt_proto_type (RforIptProtoType

error_code (R	errorCode"�
gp_gs_error_messageD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_gs_error_messageRtype

proto_type (R	protoType

error_code (R	errorCode
param (Rparam"�
instance_info_t
tid (Rtid
	timestamp (:0R	timestamp
count (:0Rcount(
addition_count (:0RadditionCount%
professional (:0Rprofessional.
refresh_timestamp (:0RrefreshTimestamp
use_time (:0RuseTime(
max_difficulty (:0RmaxDifficulty)
last_left_count	 (:0RlastLeftCount/
last_day_timestamp
 (:0RlastDayTimestamp
mode (:0Rmode%
last_stage_id (:0RlastStageId

last_pos_x (:0RlastPosX

last_pos_y (:0RlastPosY

last_pos_z (:0RlastPosZ"�
gp_instance_infoA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_instance_infoRtype
mode (:-1Rmode'
info (2.PB.instance_info_tRinfo'
passed_instance (RpassedInstance$

total_info (:falseR	totalInfo"�
gp_inventory_sizeB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_inventory_sizeRtype(
inventory_type (:0RinventoryType
cur_size (RcurSize
reason (Rreason"�
gp_level_result@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_level_resultRtype
result (:0Rresult
	list_name (RlistName

list_score (R	listScore"�
modify_corps_data
corpsid (Rcorpsid
mode (Rmode
roleid (Rroleid
money (Rmoney"
contribution (Rcontribution
key (Rkey
value	 (Rvalue
bonus
 (Rbonus"�
gp_ipt_containerA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_ipt_containerRtype4

corps_info (2.PB.modify_corps_dataR	corpsInfo"w
gp_s2c_gs_ping;
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_gs_pingRtype(
client_send_time (RclientSendTime"k
gp_level_score?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_level_scoreRtype
score (:-1Rscore"�
gp_player_list_infoD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_player_list_infoRtype
new_id (RnewId$
fightcapacity (Rfightcapacity"�
gp_level_info>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_level_infoRtype&
inst_start_time (RinstStartTime(
stage_start_time (RstageStartTime"w
gp_notify_ask_helpC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_notify_ask_helpRtype
	timestamp (R	timestamp"�
gp_find_way_resultC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_find_way_resultRtype
retcode (Rretcode(
	way_point (2.PB.a3d_posRwayPoint
reason (Rreason"�
gp_npc_info<
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_npc_infoRtype
newtype (Rnewtype&
can_be_attacked (RcanBeAttacked"
gp_easy_mall_service_endI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_easy_mall_service_endRtype
retcode (Rretcode"�
gp_notify_monitor_bloodH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_notify_monitor_bloodRtype
id (Rid
slot (Rslot"
can_be_attack (RcanBeAttack"s
gp_notify_prop_readyE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_notify_prop_readyRtype
ready (Rready"�
retrieve_info_str
type (Rtype
tid (Rtid

left_count (R	leftCount
	timestamp (R	timestamp%
retrieve_count (RretrieveCount
activity_id (R
activityId"D
retrieve_info_t1
retrieve (2.PB.retrieve_info_strRretrieve"�
gp_retrieve_infoA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_retrieve_infoRtype'
info (2.PB.retrieve_info_tRinfo'
one (2.PB.retrieve_info_strRone
single_data (R
singleData"�
gp_red_packet>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_red_packetRtype
tid (Rtid
count (Rcount
src (Rsrc
src_name (RsrcName
	timestamp (R	timestamp
timeout (Rtimeout"�
gp_secure_idip?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_secure_idipRtype
mode (Rmode
content (Rcontent
para1 (Rpara1
time (Rtime"�
gp_multi_exp=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_multi_expRtype
rate (Rrate
	left_time (RleftTime

login_time (R	loginTime
tid (Rtid"�
gp_refuse_fight@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_refuse_fightRtype
tid (Rtid#
end_timestamp (RendTimestamp"
cancel_in_war (RcancelInWar"�
gp_deliver_compensationH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_deliver_compensationRtype'
list (2.PB.gp_compensationRlist"M
level_sorted_info
score (Rscore
name (Rname
id (Rid"�
gp_send_level_sorted_infoJ
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_send_level_sorted_infoRtype)
list (2.PB.level_sorted_infoRlist"x
gp_hide_fashion_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hide_fashion_resultRtype
mask (:0Rmask"�
gp_property_re?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_property_reRtypeJ
err (2&.PB.gp_property_re.PROPERTY_ERROR_CODE:PROPERTY_SUCCESSRerr"�
PROPERTY_ERROR_CODE
PROPERTY_SUCCESS
PROPERTY_NO_ENOUGH_POINT
PROPERTY_NO_REQUIRED_ITEM
PROPERTY_INVALID_PARAM"�
part_color_hsv 

color_hsv1 (:0R	colorHsv1 

color_hsv2 (:0R	colorHsv2 

color_hsv3 (:0R	colorHsv3 

color_hsv4 (:0R	colorHsv4 

color_hsv5 (:0R	colorHsv5 

color_hsv6 (:0R	colorHsv6 

color_hsv7 (:0R	colorHsv7 

color_hsv8 (:0R	colorHsv8 

color_hsv9	 (:0R	colorHsv9"
color_hsv10
 (:0R
colorHsv10"
color_hsv11 (:0R
colorHsv11"
color_hsv12 (:0R
colorHsv12"
color_hsv13 (:0R
colorHsv13"
color_hsv14 (:0R
colorHsv14"
color_hsv15 (:0R
colorHsv15"
color_hsv16 (:0R
colorHsv16"
color_hsv17 (:0R
colorHsv17"
color_hsv18 (:0R
colorHsv18"
color_hsv19 (:0R
colorHsv19"
color_hsv20 (:0R
colorHsv20"
color_hsv21 (:0R
colorHsv21"
color_hsv22 (:0R
colorHsv22"
color_hsv23 (:0R
colorHsv23"
color_hsv24 (:0R
colorHsv24"
color_hsv25 (:0R
colorHsv25"
color_hsv26 (:0R
colorHsv26"
color_hsv27 (:0R
colorHsv27"
color_hsv28 (:0R
colorHsv28"
color_hsv29 (:0R
colorHsv29"
color_hsv30 (:0R
colorHsv30"
color_hsv31 (:0R
colorHsv31"
color_hsv32  (:0R
colorHsv32"
color_hsv33! (:0R
colorHsv33"
color_hsv34" (:0R
colorHsv34"
color_hsv35# (:0R
colorHsv35"
color_hsv36$ (:0R
colorHsv36"
color_hsv37% (:0R
colorHsv37"
color_hsv38& (:0R
colorHsv38"
color_hsv39' (:0R
colorHsv39"
color_hsv40( (:0R
colorHsv40"
color_hsv41) (:0R
colorHsv41"
color_hsv42* (:0R
colorHsv42"
color_hsv43+ (:0R
colorHsv43"
color_hsv44, (:0R
colorHsv44"
color_hsv45- (:0R
colorHsv45"
color_hsv46. (:0R
colorHsv46"
color_hsv47/ (:0R
colorHsv47"
color_hsv480 (:0R
colorHsv48"
color_hsv491 (:0R
colorHsv49"
color_hsv502 (:0R
colorHsv50"
color_hsv513 (:0R
colorHsv51"
color_hsv524 (:0R
colorHsv52"
color_hsv535 (:0R
colorHsv53"
color_hsv546 (:0R
colorHsv54"
color_hsv557 (:0R
colorHsv55"
color_hsv568 (:0R
colorHsv56"
color_hsv579 (:0R
colorHsv57"
color_hsv58: (:0R
colorHsv58"
color_hsv59; (:0R
colorHsv59"
color_hsv60< (:0R
colorHsv60"
color_hsv61= (:0R
colorHsv61"
color_hsv62> (:0R
colorHsv62"
color_hsv63? (:0R
colorHsv63"
color_hsv64@ (:0R
colorHsv64"�
charm_info_t
charm_type1 (R
charmType1
charm_type2 (R
charmType2
charm_type3 (R
charmType3
charm_type4 (R
charmType4
charm_type5 (R
charmType5"�
fashion_detail#
fashion_index (RfashionIndex

part_color (R	partColor
colors (Rcolors
expire_time (R
expireTime
color_limit (R
colorLimit8
part_color_hsv (2.PB.part_color_hsvRpartColorHsv/

charm_info (2.PB.charm_info_tR	charmInfo"�
baby_fashion_data&
hair (2.PB.fashion_detailRhair,
clothes (2.PB.fashion_detailRclothes*
weapon (2.PB.fashion_detailRweapon"�
gp_summon_child_reC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_summon_child_reRtype
master (:0Rmaster
tid (:0Rtid
stage (:0Rstage
index (:0Rindex
name (Rname&
hair (2.PB.fashion_detailRhair,
clothes (2.PB.fashion_detailRclothes*
weapon	 (2.PB.fashion_detailRweapon
spouse_guid
 (R
spouseGuid
spouse_name (R
spouseName"�
gp_recall_child_reC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_recall_child_reRtype
master (:0Rmaster
index (:0Rindex"�
gp_child_recv_expB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_recv_expRtype"
child_index (:0R
childIndex
exp (:0Rexp"�
gp_child_level_upB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_level_upRtype"
child_index (:0R
childIndex
exp (:0Rexp
level (Rlevel"K
destiny_info
tid (Rtid
level (Rlevel
exp (:0Rexp"�

destiny_op7
op (2.PB.destiny_op.DESTINY_OP:DESTINY_PUSHRop
index (:0Rindex$
info (2.PB.destiny_infoRinfo"@

DESTINY_OP
DESTINY_PUSH 
DESTINY_POP
DESTINY_SET"�
gp_destiny_resultB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_destiny_resultRtype
op (2.PB.destiny_opRop
op_index (:0RopIndexF
op_type (2'.PB.gp_destiny_result.CHILD_RESULT_TYPE:SYNCRopType-
twinkle_location (:-2RtwinkleLocation&
twinkle_index (:0RtwinkleIndex"=
CHILD_RESULT_TYPE
SYNC 
GAIN
MOVE

DEVOUR"�
gp_child_property_reE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_property_reRtype"
child_index (:0R
childIndex\
err (22.PB.gp_child_property_re.CHILD_PROPERTY_ERROR_CODE:CHILD_PROPERTY_SUCCESSRerr"R
CHILD_PROPERTY_ERROR_CODE
CHILD_PROPERTY_SUCCESS
CHILD_PROPERTY_FAILED"�
gp_pet_property_reC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_property_reRtype
	pet_index (:0RpetIndexV
err (2..PB.gp_pet_property_re.PET_PROPERTY_ERROR_CODE:PET_PROPERTY_SUCCESSRerr"L
PET_PROPERTY_ERROR_CODE
PET_PROPERTY_SUCCESS
PET_PROPERTY_FAILED"�
gp_pet_property@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_propertyRtype
	pet_index (RpetIndex
max_hp (RmaxHp
phy_atk (RphyAtk
phy_def (RphyDef
mag_atk (RmagAtk
mag_def (RmagDef
crit (Rcrit
crit_res	 (RcritRes"�
gp_marital_statueB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_marital_statueRtype
roleid (Rroleid
	spouse_id (RspouseId"�
gp_pet_select_skill_reG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_select_skill_reRtype
	pet_index (:0RpetIndex"
skill_index (:0R
skillIndex"~
gp_child_reborn_reC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_reborn_reRtype#
child_index (:-1R
childIndex"v
gp_pet_reborn_reA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_reborn_reRtype
	pet_index (:-1RpetIndex"t
gp_pet_mixis_re@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_mixis_reRtype
	pet_index (:-1RpetIndex"t
gp_pet_train_re@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_train_reRtype
	pet_index (:-1RpetIndex"�
gp_child_rename_reC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_rename_reRtype"
child_index (:0R
childIndex
name (Rname"�
gp_pet_rename_reA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_rename_reRtype
	pet_index (:0RpetIndex
name (Rname"�
gp_pet_learn_skill_reF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_learn_skill_reRtype
	pet_index (:-1RpetIndex"�
gp_pet_skill_lock_reE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_skill_lock_reRtype
	pet_index (:0RpetIndex"
skill_index (:0R
skillIndex
lock (:trueRlock
retcode (:0Rretcode"�
gp_pet_upgrade_reB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_upgrade_reRtype
	pet_index (:0RpetIndex
retcode (:0Rretcode"�
gp_pet_levelup_readan_reI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_levelup_readan_reRtype
	pet_index (:0RpetIndex$
readan_index (:0RreadanIndex
retcode (:0Rretcode"�
gp_pet_clear_readan_reG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_clear_readan_reRtype
	pet_index (:0RpetIndex$
readan_index (:0RreadanIndex
retcode (:0Rretcode"�
gp_change_prof?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_change_profRtype
old_prof (RoldProf
old_body (RoldBody"�
gp_change_faceid_reD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_change_faceid_reRtype
faceid (Rfaceid
result (Rresult"�
gp_xyxw_invite_re?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_inviteRtype
	object_id (RobjectId
	xyxw_type (RxyxwType
param (Rparam"�
gp_xyxw_invite_reply_reE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_invite_replyRtype
	object_id (RobjectId
	xyxw_type (RxyxwType
reply (Rreply"�
gp_xyxw_request_re@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_requestRtype
	object_id (RobjectId
	xyxw_type (RxyxwType"�
gp_xyxw_request_reply_reF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_request_replyRtype
	object_id (RobjectId
	xyxw_type (RxyxwType
reply (Rreply"�
gp_xyxw_qqmm=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_qqmmRtype$
main_object_id (RmainObjectId(
follow_object_id (RfollowObjectId"�
gp_xyxw_info=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_infoRtype
	object_id (RobjectId
	xyxw_type (RxyxwType
pos_in_xyxw (R	posInXyxw
	max_count (RmaxCount
members (Rmembers
xyxw_param1 (R
xyxwParam1
xyxw_param2 (R
xyxwParam2"�
gp_xyxw_stop=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_xyxw_stopRtype
	object_id (RobjectId
pos_in_xyxw (R	posInXyxw
pos (2.PB.a3d_posRpos"�
gp_hometown_show_object_resultO
type (2.PB.S2C_GS_PROTOC_TYPE:#type_gp_hometown_show_object_resultRtype
index (Rindex
	pos_index (RposIndex
x (Rx
y (Ry
dir (Rdir
result (Rresult"�
gp_hometown_hide_object_resultO
type (2.PB.S2C_GS_PROTOC_TYPE:#type_gp_hometown_hide_object_resultRtype
index (Rindex
	pos_index (RposIndex
result (Rresult"�
gp_hometown_client_objects_infoP
type (2.PB.S2C_GS_PROTOC_TYPE:$type_gp_hometown_client_objects_infoRtypeb
	info_type (2E.PB.gp_hometown_client_objects_info.HOMETOWN_CLIENT_OBJECTS_INFO_TYPERinfoTypeV
hometown_client_objects_total (2.PB.hometown_objectRhometownClientObjectsTotalK
hometown_client_objects (2.PB.hometown_objectRhometownClientObjects

last_clean (R	lastClean#
farm (2.PB.farm_objectRfarm"�
!HOMETOWN_CLIENT_OBJECTS_INFO_TYPE/
+HOMETOWN_CLIENT_OBJECTS_INFO_TYPE_ALL_TOTAL/
+HOMETOWN_CLIENT_OBJECTS_INFO_TYPE_ONE_TOTAL/
+HOMETOWN_CLIENT_OBJECTS_INFO_TYPE_ALL_SHOWN/
+HOMETOWN_CLIENT_OBJECTS_INFO_TYPE_ONE_SHOWN0
,HOMETOWN_CLIENT_OBJECTS_INFO_TYPE_LOGIN_INFO"�
gp_hometown_enter_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hometown_enter_resultRtype
roleid (Rroleid
result (Rresult"�
gp_hometown_buy_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hometown_buy_resultRtype

object_tid (R	objectTid
result (Rresult"�
gp_hometown_sell_resultH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hometown_sell_resultRtype
index (Rindex

object_tid (R	objectTid
count (Rcount
result (Rresult"�
gp_hometown_farm_reD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_hometown_farm_reRtype
result (Rresult"
op (2.PB.PLAYER_FARM_OPRop#
farm (2.PB.farm_objectRfarm":
produce_skill_info
id (Rid
level (Rlevel"�
gp_produce_skillA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_produce_skillRtype*
info (2.PB.produce_skill_infoRinfo"�
 gp_produce_skill_level_up_resultQ
type (2.PB.S2C_GS_PROTOC_TYPE:%type_gp_produce_skill_level_up_resultRtype
result (:0Rresult"�
gp_produce_item_resultG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_produce_item_resultRtype
result (:0Rresult
item_id (:0RitemId 

item_count (:0R	itemCount"�
gp_change_equip_suit_resultL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_change_equip_suit_resultRtype
result (:0Rresult(
equip_location (:0RequipLocation"�
gp_self_equip_suitC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_self_equip_suitRtype(
equip_location (:0RequipLocation"�
gp_fashion_info@
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fashion_infoRtype)
selected_fashion (RselectedFashion*
detail (2.PB.fashion_detailRdetail"�
gp_active_fashionB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_active_fashionRtype*
detail (2.PB.fashion_detailRdetail"�
gp_select_fashion_resultI
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_select_fashion_resultRtype
roleid (Rroleid

baby_index (R	babyIndex*
detail (2.PB.fashion_detailRdetail
result (:0Rresult"m
gp_fashion_expireB
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fashion_expireRtype
index (Rindex"�
gp_fashion_renewA
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_fashion_renewRtype*
detail (2.PB.fashion_detailRdetail"�
gp_active_intimate_friend_titleP
type (2.PB.S2C_GS_PROTOC_TYPE:$type_gp_active_intimate_friend_titleRtype
roleid (Rroleid
title (Rtitle"�
gp_active_inlaws_titleG
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_active_inlaws_titleRtype
roleid (Rroleid
title (Rtitle"�
gp_activity_exchange_resultL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_activity_exchange_resultRtype
roleid (Rroleid
result (Rresult"�
gp_active_fashion_color_resultO
type (2.PB.S2C_GS_PROTOC_TYPE:#type_gp_active_fashion_color_resultRtype*
detail (2.PB.fashion_detailRdetail
result (Rresult
color_index (R
colorIndex"�
gp_paint_fashion_resultH
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_paint_fashion_resultRtype
roleid (Rroleid*
detail (2.PB.fashion_detailRdetail
result (Rresult"�
broadcast_fashion_data&
hair (2.PB.fashion_detailRhair$
ear (2.PB.fashion_detailRear&
tail (2.PB.fashion_detailRtail*
tattoo (2.PB.fashion_detailRtattoo5
head_pendant (2.PB.fashion_detailRheadPendant5
face_pendant (2.PB.fashion_detailRfacePendant5
back_pendant (2.PB.fashion_detailRbackPendant3
hip_pendant (2.PB.fashion_detailR
hipPendant,
earring	 (2.PB.fashion_detailRearring.
necklace
 (2.PB.fashion_detailRnecklace5
hand_pendant (2.PB.fashion_detailRhandPendant,
clothes (2.PB.fashion_detailRclothes*
weapon (2.PB.fashion_detailRweapon
	hide_mask (RhideMask)
selected_fashion (RselectedFashion"Y
personal_fashion_data@
all_fashion_detail (2.PB.fashion_detailRallFashionDetail"�
single_fashion#
fashion_index (RfashionIndex

part_index (R	partIndex
color_index (R
colorIndex
	hsv_index (RhsvIndex(
paint_color_cost (RpaintColorCost(
paint_toner_cost (RpaintTonerCost*
paint_bright_cost (RpaintBrightCost"�
gp_mult_paint_fashionF
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MULT_PAINT_FASHIONRtype9
paint_fashions (2.PB.single_fashionRpaintFashions"�
gp_mult_select_fashionG
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_MULT_SELECT_FASHIONRtype'
select_fashions (RselectFashions"�
gp_mult_select_fashion_resultN
type (2.PB.S2C_GS_PROTOC_TYPE:"type_gp_mult_select_fashion_resultRtype
roleid (Rroleid*
detail (2.PB.fashion_detailRdetail
result (Rresult"�
gp_mult_paint_fashion_resultM
type (2.PB.S2C_GS_PROTOC_TYPE:!type_gp_mult_paint_fashion_resultRtype
roleid (Rroleid*
detail (2.PB.fashion_detailRdetail
result (Rresult"�
gp_boardcast_fashion_changeL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_boardcast_fashion_changeRtype3
bc_fashions (2.PB.fashion_detailR
bcFashions
roleid (Rroleid"�
gp_child_use_item_reE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_child_use_item_reRtype
child_index (R
childIndex
	item_type (RitemType
args (Rargs
value (Rvalue

error_code (R	errorCode"�
gp_invite_couple_tourF
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_invite_couple_tourRtype
child_index (R
childIndex
	timestamp (R	timestamp"�
gp_pet_use_item_reC
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_pet_use_item_reRtype
	pet_index (RpetIndex
	item_type (RitemType
args (Rargs
value (Rvalue

error_code (R	errorCode"o
gp_task_system_speakE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_task_system_speakRtype
msg (Rmsg"2

skill_info
id (Rid
level (Rlevel"�
gp_upgrade_all_skillE
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_upgrade_all_skillRtype-

skill_list (2.PB.skill_infoR	skillList"�
gp_cash_notify?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_cash_notifyRtype!
unbound_cash (RunboundCash

bound_cash (R	boundCash"�
*gp_player_search_corps_battle_score_result[
type (2.PB.S2C_GS_PROTOC_TYPE:/type_gp_player_search_corps_battle_score_resultRtype$
faction_one_id (RfactionOneId(
faction_one_name (RfactionOneName$
faction_two_id (RfactionTwoId(
faction_two_name (RfactionTwoName
info (Rinfo

self_score (R	selfScore
	self_rank (RselfRank
kill	 (Rkill)
secondary_attack
 (RsecondaryAttack
	do_damage (RdoDamage
be_hurt (RbeHurt
do_heal (RdoHeal
	gathernum (R	gathernum"�
gp_corps_battle_endD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_corps_battle_endRtype#
battle_result (RbattleResult
players (Rplayers"�
arena_battle_player
roleid (Rroleid
name (Rname
kill (:0Rkill
death (:0Rdeath
damage (:0Rdamage
heal (:0Rheal
hurt (:0Rhurt"x
arena_battle_top_player
roleid (Rroleid
name (Rname
idphoto (Ridphoto
value (:0Rvalue"�
arena_battle_top/
kill (2.PB.arena_battle_top_playerRkill3
damage (2.PB.arena_battle_top_playerRdamage/
heal (2.PB.arena_battle_top_playerRheal/
hurt (2.PB.arena_battle_top_playerRhurt)
mvp (2.PB.arena_battle_playerRmvp+
smvp (2.PB.arena_battle_playerRsmvp"�
gp_arena_battle_endD
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_arena_battle_endRtype#
battle_result (RbattleResult&
top (2.PB.arena_battle_topRtop+
data (2.PB.arena_battle_playerRdata"�
!gp_player_send_redenvelope_resultS
type (2.PB.S2C_GS_PROTOC_TYPE:'type_gp_player_send_red_envelope_resultRtype
retcode (Rretcode"�
!gp_player_draw_redenvelope_resultS
type (2.PB.S2C_GS_PROTOC_TYPE:'type_gp_player_draw_red_envelope_resultRtype
retcode (Rretcode%
money_received (RmoneyReceived7
redenvelope (2.PB.red_envelope_infoRredenvelope"
%gp_player_notify_receive_red_envelopeV
type (2.PB.S2C_GS_PROTOC_TYPE:*type_gp_player_notify_receive_red_envelopeRtype"�
gp_fashion_title_upgrade_reL
type (2.PB.S2C_GS_PROTOC_TYPE: type_gp_fashion_title_upgrade_reRtype
retcode (Rretcode
title_id (RtitleId
title_level (R
titleLevel"�
npt_get_qqgroup_openid_reqB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_QQGROUP_OPENIDRtype
roleid (Rroleid
unionid (Runionid
opt (:0Ropt
	groupcode (R	groupcode"�
npt_get_qqgroup_openid_respC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_RESP_QQGROUP_OPENIDRtype
retcode (:1000Rretcode
roleid (Rroleid
unionid (Runionid
opt (:0Ropt
	groupcode (R	groupcode
is_lost (:0RisLost!
group_openid (RgroupOpenid
msg	 (Rmsg

group_name
 (R	groupName"�
npt_get_http_req<
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_HTTP_REQRtype
method (:0Rmethod
url (Rurl
body (Rbody
reserved (Rreserved"�
npt_get_http_resp=
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GET_HTTP_RESPRtype
method (:0Rmethod
retcode (:200Rretcode
contents (Rcontents
reserved (Rreserved"�
npt_unbind_qqgroup_reqB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_UNBIND_QQGROUP_REQRtype
roleid (Rroleid
unionid (Runionid!
group_openid (RgroupOpenid
opt (:0Ropt"�
npt_unbind_qqgroup_respC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_UNBIND_QQGROUP_RESPRtype
retcode (:1000Rretcode
roleid (Rroleid
is_lost (:0RisLost
msg (Rmsg"t
npt_recharge_plat_vipA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_RECHARGE_PLAT_VIPRtype
vipKind (RvipKind"�
npt_send_tlog_info>
type (2.PB.NET_PROTOCBUF_TYPE:NPT_SEND_TLOG_INFORtype
	tlog_type (RtlogType9
guide (2#.PB.npt_send_tlog_info.guide_info_tRguide8
act (2&.PB.npt_send_tlog_info.activity_info_tRactB
appstore (2&.PB.npt_send_tlog_info.appstore_info_tRappstoreB
socialspace (2 .PB.npt_send_tlog_info.ss_info_tRsocialspaceY
guide_info_t
guide_index (R
guideIndex
step (Rstep
state (Rstate2
activity_info_t
activity_id (R
activityId�
appstore_info_t'
iad_attribution (RiadAttribution$
iad_click_date (	RiadClickDate.
iad_conversion_date (	RiadConversionDate 
iad_org_name (	R
iadOrgName&
iad_campaign_id (RiadCampaignId*
iad_campaign_name (	RiadCampaignName$
iad_adgroup_id (RiadAdgroupId(
iad_adgroup_name (	RiadAdgroupName
iad_keyword	 (	R
iadKeyword`
	ss_info_t
msg_id (	RmsgId

cur_length (R	curLength

max_length (R	maxLength"s
	TLOG_TYPE
TLOG_TYPE_GUIDE
TLOG_TYPE_ACTIVITY
TLOG_TYPE_APPSTORE!
TLOG_TYPE_SOCIALSPACE_WARNING"�
npt_qqgroup_joinkey_reqC
type (2.PB.NET_PROTOCBUF_TYPE:NPT_QQGROUP_JOINKEY_REQRtype
roleid (Rroleid
unionid (Runionid!
group_openid (RgroupOpenid"�
npt_qqgroup_joinkey_respD
type (2.PB.NET_PROTOCBUF_TYPE:NPT_QQGROUP_JOINKEY_RESPRtype
retcode (:1000Rretcode
roleid (Rroleid
is_lost (:0RisLost$
join_group_key (RjoinGroupKey
msg (Rmsg"�
npt_grc_get_self_plat_vip_infoJ
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GRC_GET_SELF_PLAT_VIP_INFORtype"
plat_vip_kind (RplatVipKind
retcode (Rretcode"�
greet_investor_info_cl
	greet_tid (RgreetTid

valid_time (R	validTime7
investor_list (2.PB.player_id_nameRinvestorList"�
greet_investee_info_cl
	greet_tid (RgreetTid

valid_time (R	validTime-
dst_role (2.PB.player_id_nameRdstRole"�
greet_reward_info_cl
	greet_tid (RgreetTid
	award_tid (RawardTid
award_count (R
awardCount
role_id (RroleId"�
npt_greet_invest_infoA
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GREET_INVEST_INFORtype6
investor (2.PB.greet_investor_info_clRinvestor6
investee (2.PB.greet_investee_info_clRinvestee.
award (2.PB.greet_reward_info_clRaward"�
npt_greet_invest_awardB
type (2.PB.NET_PROTOCBUF_TYPE:NPT_GREET_INVEST_AWARDRtype
reserved (RreservedD

award_type (2%.PB.npt_greet_invest_award.AWARD_TYPER	awardType"(

AWARD_TYPE
INVESTOR
INVESTEE"\
adventure_task_list_item&
role (2.PB.player_id_nameRrole
idphoto (Ridphoto"G
adventure_task_list0
item (2.PB.adventure_task_list_itemRitem"~
npt_req_adventure_task_listG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REQ_ADVENTURE_TASK_LISTRtype
taskid (Rtaskid"�
npt_rep_adventure_task_listG
type (2.PB.NET_PROTOCBUF_TYPE:NPT_REP_ADVENTURE_TASK_LISTRtype
taskid (Rtaskid0
item (2.PB.adventure_task_list_itemRitem"=

talent_set
name (Rname
	talent_id (RtalentId"]
player_talent_t!

active_idx (:-1R	activeIdx'
all_set (2.PB.talent_setRallSet"{
gp_player_talent:
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_talentRtype+
talent (2.PB.player_talent_tRtalent"�
gp_player_talent_op:
type (2.PB.C2S_GS_PROTOC_TYPE:GPROTOC_TALENTRtype
op (2.PB.GPT_OP_TYPERop
index (Rindex
name (Rname
layer (Rlayer
	talent_id (RtalentId"�
gp_player_talent_res>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_talent_resRtype
op (2.PB.GPT_OP_TYPERop
retcode (Rretcode
index (Rindex
name (Rname
layer (Rlayer
	talent_id (RtalentId"L
one_cooldown
id (Rid
rem_ms (RremMs
max_ms (RmaxMs"�
gp_cooldown<
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_cooldownRtype(
mode (2.PB.gp_cooldown.MODERmode"
arr (2.PB.one_cooldownRarr"
MODE
ALL 
PART"�
gp_drag_point>
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_drag_pointRtype
speed (Rspeed
pos (2.PB.a3d_posRpos
dis_min (RdisMin
dis_max (RdisMax"�
gp_drag_line=
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_drag_lineRtype
speed (Rspeed
pos (2.PB.a3d_posRpos
dir (Rdir
width (Rwidth
dis_min (RdisMin
dis_max (RdisMax"Q
gp_drag_remove?
type (2.PB.S2C_GS_PROTOC_TYPE:type_gp_drag_removeRtype"�
player_pk_man_t
setting (Rsetting
pk_point (RpkPoint
kill_num (RkillNum$
enemy (2.PB.enemy_infoRenemy"O
task_sceneweather
sceneid (Rsceneid 
weathertype (Rweathertype"X
player_task_sceneweather_data7
taskweather (2.PB.task_sceneweatherRtaskweather*8
RENAME_REASON_TYPE

AROUND 

FRIEND

SPOUSE*�7
NET_PROTOCBUF_TYPE
NPT_TEST
NPT_CORPS_APPOINT
NPT_RESPONSE
NPT_APPLY_CORPS
NPT_CORPS_DATA
NPT_COMMON_SEARCH
NPT_ZHAOJILING
NPT_SYNC_MIRROR_INFO
NPT_SYNC_NATION_WAR_INFO	
NPT_NATION_WAR_OPERATE

NPT_GET_CORP_NAME
NPT_NATION_WAR_OPERATE_INFO"
NPT_NATION_WAR_OPERATE_INFO_RE
NPT_TEAM_INVITE
NPT_TEAM_INFO
NPT_FRIEND_INVITE
NPT_GET_PLAYER_PROFILE
NPT_GREETING_INFO_SYNC
NPT_GREETING_REPLY
NPT_BLESSING_INFO
NPT_NATIONWAR_EVENT
NPT_NATION_SHUTUP
NPT_NATION_SHUTUP_INFO
NPT_NATION_SHUTUP_REPLY 
NPT_NATION_COMMANDER_APPOINT
NPT_NATION_COMMANDER_NOTIFY
NPT_NATION_WAR_HISTORY
NPT_REQUEST_INFO
NPT_ASK_HELP
NPT_GROUP_OP
NPT_GROUP_NOTIFY  
NPT_SEND_TENCENT_SECURE_INFO!
NPT_OFFICER_GIFT_GET"
NPT_OFFICER_GIFT_NOTIFY#
NPT_TEAM_RECRUIT_OPERATE%"
NPT_TEAM_RECRUIT_OPERATE_REPLY&
NPT_UPDATE_GRC_INFO'
NPT_GRC_SEND_GIFT)
NPT_GRC_RCV_GIFT*
NPT_GRC_FRIEND_LIST+
NPT_GRC_GIFT_LIST,
NPT_GRC_SEND_GIFT_RE-
NPT_GRC_RCV_GIFT_RE.
NPT_RECHARGE_ACTIVITY_GET/ 
NPT_RECHARGE_ACTIVITY_GET_RE0
NPT_WEAK_NATION_GIFT_NOTIFY1
NPT_WEAK_NATION_GIFT_GET2
NPT_SERVER_INFO_NOTIFY3
NPT_GET_QQGROUP_OPENID4
NPT_RESP_QQGROUP_OPENID5
NPT_GRC_TURN_ON_OFF6
NPT_GRC_TURN_ON_OFF_RE7
NPT_DIE_ELITE_TID8
NPT_GET_DIE_ELITE_TID9
NPT_GRC_EXCEED_FRIEND_LIST:
NPT_GET_HTTP_REQ;
NPT_GET_HTTP_RESP<
NPT_SEND_TLOG_INFO=
NPT_UNBIND_QQGROUP_REQ?
NPT_UNBIND_QQGROUP_RESP@
NPT_RECHARGE_PLAT_VIPA
NPT_QQGROUP_JOINKEY_REQB
NPT_QQGROUP_JOINKEY_RESPC
NPT_TOP_REWARDD"
NPT_GRC_GET_SELF_PLAT_VIP_INFOE
NPT_GREET_INVEST_INFOF
NPT_GREET_INVEST_AWARDG
NPT_SEND_LEVEL_SORTED_INFOH
NPT_PLATFORM_REGISTERI 
NPT_PLATFORM_REGISTER_RESULTJ
NPT_PLATFORM_TEAM_INVITEK
NPT_PLATFORM_INVITE_STATUSL
NPT_PLATFORM_CLOSE_NOTIFYM
NPT_PLATFORM_REJECT_NOTIFYN
NPT_PLATFORM_CANCELO
NPT_PLATFORM_CANCEL_RESULTP
NPT_PLATFORM_REJOIN_NOTIFYQ
NPT_PLATFORM_REGISTER_INFOR
NPT_PLATFORM_REGINFO_RESULTS
NPT_PLATFORM_SUCCESS_NOTIFYT
NPT_PLATFORM_GET_TEAM_LISTU
NPT_PLATFORM_TEAM_LISTV
NPT_PLATFORM_JOIN_TEAMW!
NPT_PLATFORM_JOIN_TEAM_RESULTX"
NPT_PLATFORM_ADD_MEMBER_NOTIFYY
NPT_PLATFORM_TEAM_REACH_MINZ
NPT_PLATFORM_TEAM_REACH_MAX[!
NPT_PLATFORM_JOIN_TEAM_NOTIFY\
NPT_ARENA_APPLY]
NPT_ARENA_APPLY_RESULT^
NPT_ARENA_MATCH_RESULT_
NPT_ARENA_QUIT`
NPT_ARENA_QUIT_RESULTa
NPT_ARENA_SEARCH_LISTb 
NPT_ARENA_SEARCH_LIST_RESULTc
NPT_ARENA_GET_AWARDd
NPT_ARENA_GET_AWARD_RESULTe
NPT_PLAYER_CONTEST_BEGINf#
NPT_PLAYER_CONTEST_BEGIN_RESULTg
NPT_PLAYER_CONTEST_EXITh
NPT_PLAYER_CONTEST_ANSWERi#
NPT_PLAYER_CONTEST_FACTION_HELPj*
&NPT_PLAYER_CONTEST_FACTION_HELP_RESULTk!
NPT_FACTION_TASK_CALLFOR_HELPl
NPT_FACTION_TASK_HELP_MSGm
NPT_CORPS_HISTORYn
NPT_GET_BLESSING_HISTORYo
NPT_BLESSING_HISTORY_RESULTp
NPT_GET_AMITY_INFOq
NPT_SERVER_LEVEL_NOTIFYr
NPT_FAKE_AUCTION_LISTs 
NPT_FAKE_AUCTION_LIST_RESULTt
NPT_USE_GIFT_CARDu
NPT_USE_GIFT_CARD_RESULTv(
$NPT_NOTIFY_CLIENT_CORPS_BATTLE_BEGINw!
NPT_PLAYER_ENTER_CORPS_BATTLEx(
$NPT_PLAYER_ENTER_CORPS_BATTLE_RESULTy'
#NPT_PLAYER_SEARCH_CORPS_BATTLE_LISTz.
*NPT_PLAYER_SEARCH_CORPS_BATTLE_LIST_RESULT{
NPT_FRIEND_BLESS|
NPT_FRIEND_BLESS_NOTIFY}
NPT_GET_MARRIAGE_INFO~ 
NPT_GET_MARRIAGE_INFO_RESULT
NPT_FAKE_AUCTION_PRICE�"
NPT_FAKE_AUCTION_PRICE_RESULT�#
NPT_PLAYER_SEARCH_RED_ENVELOPE�*
%NPT_PLAYER_SEARCH_RED_ENVELOPE_RESULT�
NPT_CORPS_MANIFESTO�
NPT_GET_AMITY_MAX�
NPT_GET_AMITY_MAX_RESULT�
NPT_GET_ACTIVITY_AWARD�
NPT_CORPS_MERGE_OP�%
 NPT_GET_CORPS_TOWER_BATTLE_AWARD�,
'NPT_GET_CORPS_TOWER_BATTLE_AWARD_RESULT�$
NPT_SYNC_BIND_MOBILE_PHONE_INFO�
NPT_SYNC_RECHARGE_INFO�
NPT_REVENGE_BATTLE_RESULT�
NPT_UI_CONTROL�
NPT_GET_TP_ADDON�
NPT_GET_TP_ADDON_RESULT�
NPT_CHANGE_ZONE_RESPONSE�
NPT_NOTIFY_ZONE_LIST�
NPT_GET_FRIEND_LIST�
NPT_CHECK_SECT_PREMISE�"
NPT_CHECK_SECT_PREMISE_RESULT�
NPT_CHECK_SECT_GRADUATE�#
NPT_CHECK_SECT_GRADUATE_RESULT�
NPT_SECT_APPLY_REQUEST�
NPT_SECT_INVITE_REQUEST�
NPT_SECT_QUIT�
NPT_SECT_EXPEL�

NPT_NO_USE�
NPT_NO_USE2�
NPT_NO_USE3�
NPT_NO_USE4�
NPT_INFORM_SECT_INFO�
NPT_SELECT_SECT_TITLE�#
NPT_CENTER_BATTLE_PLAYER_APPLY�&
!NPT_CENTER_BATTLE_PLAYER_APPLY_RE�)
$NPT_CENTER_BATTLE_PLAYER_SEARCH_LIST�,
'NPT_CENTER_BATTLE_PLAYER_SEARCH_LIST_RE�"
NPT_CENTER_BATTLE_PLAYER_QUIT�%
 NPT_CENTER_BATTLE_PLAYER_QUIT_RE�&
!NPT_CENTER_BATTLE_PLAYER_PUNISHED�
NPT_CENTER_BATTLE_JOIN_ASK�"
NPT_CENTER_BATTLE_JOIN_ANSWER�
NPT_WEDDING_BEGIN_NOTIFY�
NPT_GET_GLOBAL_DATA�
NPT_GET_GLOBAL_DATA_RE� 
NPT_GLOBAL_DATA_SYNC_PLAYER�
NPT_RENAME_INFORM�
NPT_GM_SHUTDOWN_SERVER�
NPT_PARADING_BEGIN_NOTIFY�
NPT_HOMETOWN_FARM_UPDATE�
NPT_ARENA_GROUP_INVITE�
NPT_CORPS_RACE_STATUS�
NPT_CORPS_GET_RACE_INFO�
NPT_CORPS_RACE_CHEER�
NPT_CORPS_RACE_BOOS�
NPT_FORBID_STRANGER_CHAT�"
NPT_CORPS_CENTER_BATTLE_BEGIN�%
 NPT_CORPS_SERVER_BATTLE_SWAP_POS�(
#NPT_CORPS_SERVER_BATTLE_SWAP_POS_RE�#
NPT_PLAYER_CONSTELLATION_BEGIN�'
"NPT_PLAYER_CONSTELLATION_QUESTIONS�$
NPT_PLAYER_CONSTELLATION_ANSWER�"
NPT_PLAYER_CONSTELLATION_INFO�$
NPT_PLAYER_CONSTELLATION_CHANGE�
NPT_REFRESH_H5_GAME_REPU�
NPT_GET_H5_GAME_REWARD�!
NPT_SOCIAL_SPACE_TOKEN_CHECK� 
NPT_REQ_ADVENTURE_TASK_LIST� 
NPT_REP_ADVENTURE_TASK_LIST�
NPT_GET_RECOMMEND_FRIEND� 
NPT_GET_RECOMMEND_FRIEND_RE�
NPT_FRIEND_GROUP_CHANGE�
NPT_FRIEND_GROUP_CHANGE_RE�
NPT_FRIEND_GROUP_MOVE�
NPT_SYNC_FUNC_SWITCH� 
NPT_PLAYER_CONTEST_FOR_HELP�
NPT_PLAYER_SEARCH_BRIEF�
NPT_PLAYER_SEARCH_BRIEF_RE�
NPT_GET_PLAYER_TEAM_INFO� 
NPT_GET_PLAYER_TEAM_INFO_RE�!
NPT_CORPS_SEARCH_DETAIL_INFO� 
NPT_GOLDEN_INTIMATE_OPERATE�#
NPT_GOLDEN_INTIMATE_OPERATE_RE�
NPT_GOLDEN_INTIMATE_INVITE�
NPT_SECRET_LOVE_OP�
NPT_SECRET_LOVE_NOTIFY�
NPT_MARRIAGE_MSG�
NPT_TEAM_CHATROOM_OP�
NPT_TEAM_CHATROOM_NOTIFY�
NPT_TEAM_CHATROOM_INVITE�'
"NPT_SOCIAL_SPACE_ROAM_FRIEND_APPLY�*
%NPT_SOCIAL_SPACE_ROAM_FRIEND_APPLY_RE�
NPT_GET_SERVER_DEBUG_STATE�"
NPT_GET_SERVER_DEBUG_STATE_RE�
NPT_CORPS_RACE_RANDOMEVENT�
NPT_CORPS_RACE_RUNNER_POS�
NPT_CORPS_RACE_GET_AWARD�!
NPT_CORPS_RACE_GET_AWARD_RET�)
$NPT_CORPS_RACE_GET_CHAMPAIGN_HISTORY�-
(NPT_CORPS_RACE_GET_CHAMPAIGN_HISTORY_RET�
NPT_GRC_USER_LOGIN_INFO�
NPT_GRC_RCV_ALL_GIFT�
NPT_GRC_SEND_ALL_GIFT�,
'NPT_GRC_GET_USER_RECEIVE_GIFT_INFO_LIST�
NPT_GET_OPENID�
NPT_GET_OPENID_RE�!
NPT_REQ_ADVENTURE_TASK_RATIO�!
NPT_REP_ADVENTURE_TASK_RATIO�
NPT_UPDATE_LOGIN_STATUS�
NPT_NOTIFY_RECHARGE�*�
GOLDEN_INTIMATE_OPERATE
GIO_GOLDEN_APPLY
GIO_GOLDEN_GET
GIO_GOLDEN_DELETE
GIO_MESSAGE_MODIFY
GIO_LEVEL_AWARD

GIO_RENAME
GIO_SELECT_TITLE
GIO_SELECT_THEME*�
FRIEND_GROUP_CHANGE_TYPE 
FRIEND_GROUP_CHANGE_TYPE_NEW#
FRIEND_GROUP_CHANGE_TYPE_RENAME#
FRIEND_GROUP_CHANGE_TYPE_DELETE!
FRIEND_GROUP_CHANGE_TYPE_SORT*�
CORPS_POSITION
	CP_NORMAL 
	CP_MASTER
CP_VICE_MASTER
CP_SECRETARY
CP_CONSULTANT

CP_MINISTER
CP_JINGYING
CP_TEMPP
	CP_INTERNd*;
CORPS_SPECIAL_POSITION

CSP_NORMAL 
CSP_COMMANDER
*e

MERGE_TYPE
MERGE_TYPE_NULL 
MERGE_TYPE_MERGE
MERGE_TYPE_BE_MERGE
MERGE_TYPE_AUTO*:

MERGE_ROLE
MERGE_ROLE_MERGER
MERGE_ROLE_MERGED*a
SEARCH_CORPS_SERVER_BATTLE_TYPE
SCSBT_BRIEF
SCSBT_PLAYERS
SCSBT_CHANGE_POS_INFOS*_
SEARCH_CORPS_KEY_TYPE
SCKT_ONLINE
SCKT_ONLINE_NEWBEE
	SCKT_NAME

SCKT_MERGE*a
MIRROR_STATE
MIRROR_S_CREATING
MIRROR_S_OK
MIRROR_S_CLOSING
MIRROR_S_CLOSED*�
ARENA_AWARD_TYPE 
ARENA_AWARD_TYPE_TOTAL_COUNT
ARENA_AWARD_TYPE_WIN_COUNT*
&ARENA_AWARD_TYPE_CONSECUTIVE_WIN_COUNT
ARENA_AWARD_TYPE_RANK*%
ask_help_info_type
AT_ASK_HELP *�6
C2S_GS_PROTOC_TYPE
GPROTOC_BEGIN_T�'
"GPROTOC_EQUIP_GRADE_AND_QUALITY_UP�
GPROTOC_CREATE_CORPS�
GPROTOC_EQUIP_ATTACH_GEM�
GPROTOC_EQUIP_DETACH_GEM�#
GPROTOC_EQUIP_AFFIXES_TRANSFER�
GPROTOC_EQUIP_LIANXING�
GPROTOC_EQUIP_TRANSFER�
GPROTOC_ITEM_COMBINE�
GPROTOC_NATION_ESCORT_OP�
GPROTOC_GET_BROADCAST_BUFF�
GPROTOC_SET_PK_SETTING�
GPROTOC_NATION_DONATE�
GPROTOC_AUTO_REWARD_OP�"
GPROTOC_GET_ACHIEVEMENT_AWARD�
GPROTOC_MOUNT_OPERATION�
GPROTOC_GUAJI�
GPROTOC_VIP_OPERATION�
GPROTOC_AUTO_COMBAT_CONFIG�
GPROTOC_UPGRADE_BINGFA�
GPROTOC_START_ATTACK_LOOP�
GPROTOC_CG_PLAYER_OP�
GPROTOC_WINE_SITDOWN�
GPROTOC_CANCEL_ACTION� 
GPROTOC_ESCORT_CHANGE_SPEED�
GPROTOC_MINIGAME_OPERATE�
GPROTOC_CARD_OPERATE�
GPROTOC_STUNT_CONFIG�
GPROTOC_BLACK_SHOP_COMMAND�
GPROTOC_DUKE_COMMAND�
GPROTOC_HERO_TRIAL�
GPROTOC_CORP_CONFIG�
GPROTOC_ENTER_INSTANCE�
GPROTOC_BUY_INSTANCE�
GPROTOC_BUY_BACKPACK�
GPROTOC_EQUIP_OPEN_ADDON�
GPROTOC_JIEYUN_SKILL�
GPROTOC_EQUIP_TRAIN�!
GPROTOC_EQUIP_SWITCH_SURFACE�
GPROTOC_CORP_FARM_OP�
GPROTOC_CLIMB_TOWER_OP�
GPROTOC_GS_PING�
GPROTOC_EQUIP_REFINE�
GPROTOC_GET_LIST_INFO�
GPROTOC_CAMP_FIRE�$
GPROTOC_CLIMBTOWER_SHOP_COMMAND�
GPROTOC_ASK_HELP�%
 GPROTOC_EQUIP_ATTACH_GEM_UPGRADE�
GPROTOC_FIND_WAY�
GPROTOC_CHANGE_MIRROR�
GPROTOC_BUY_FORCE�
GPROTOC_PLANT�
GPROTOC_EQUIP_CHAIJIE�
GPROTOC_CLIENT_PRINT_INFO�
GPROTOC_RETRIEVE_OP�
GPROTOC_FASHION_MODE�
GPROTOC_SWEEP_INSTANCE�
GPROTOC_GET_RED_PACKET�
GPROTOC_COMMON_OPERATION�
GPROTOC_COMPENSATION�
GPROTOC_UPGRADE_CHARIOT�
GPROTOC_WUHUN_OPEN�
GPROTOC_WUHUN_UPGRADE�
GPROTOC_RENT_CHARIOT�
GPROTOC_ADD_PROPERTY�
GPROTOC_CLEAR_PROPERTY�!
GPROTOC_CHANGE_PROPERTY_PLAN�#
GPROTOC_PRODUCE_SKILL_LEVEL_UP�
GPROTOC_PRODUCE_ITEM�
GPROTOC_CHANGE_EQUIP_SUIT�
GPROTOC_SELECT_FASHION�
GPROTOC_SUMMON_PET�
GPROTOC_RECALL_PET�
GPROTOC_ACTIVITY_EXCHANGE�
GPROTOC_PET_ADD_PROPERTY�
GPROTOC_PET_CLEAR_PROPERTY�
GPROTOC_PET_SELECT_SKILL� 
GPROTOC_FLY_SWORD_OPERATION�
GPROTOC_TALISMAN_OPERATE�
GPROTOC_RETINUE_OPERATE�
GPROTOC_PET_REBORN�
GPROTOC_PET_MIXIS�
GPROTOC_PET_TRAIN�
GPROTOC_PET_RENAME�%
 GPROTOC_HUNT_DRAGON_POINT_SWITCH�
GPROTOC_PLAYER_PRACTICE�!
GPROTOC_ACTIVE_FASHION_COLOR�
GPROTOC_PAINT_FASHION�
GPROTOC_KASI_RESTORE�"
GPROTOC_EQUIP_BASEATTR_REFINE�#
GPROTOC_EQUIP_EXTRAATTR_RECAST�
GPROTOC_GET_EXP_LOTTERY�
GPROTOC_GET_LOTTERY_EXP�!
GPROTOC_FAKEAUCTION_SELFSHOP�
GPROTOC_FAKEAUCTION_OPEN�
GPROTOC_FAKEAUCTION_REOPEN�
GPROTOC_FAKEAUCTION_CLOSE�
GPROTOC_FAKEAUCTION_BUY�"
GPROTOC_FAKEAUCTION_GET_MONEY� 
GPROTOC_FRIEND_BLESS_REWARD�-
(GPROTOC_PLAYER_SEARCH_CORPS_BATTLE_SCORE�"
GPROTOC_PET_SET_PROPERTY_PLAN�%
 GPROTOC_PLAYER_SEND_RED_ENVELOPE�%
 GPROTOC_PLAYER_DRAW_RED_ENVELOPE� 
GPROTOC_SET_CORPS_MANIFESTO�
GPROTOC_XYXW_INVITE�
GPROTOC_XYXW_INVITE_REPLY�
GPROTOC_XYXW_REQUEST�
GPROTOC_XYXW_REQUEST_REPLY�
GPROTOC_XYXW_LEAVE�!
GPROTOC_HOMETOWN_SHOW_OBJECT�!
GPROTOC_HOMETOWN_HIDE_OBJECT�
GPROTOC_HOMETOWN_ENTER�
GPROTOC_HOMETOWN_BUY�
GPROTOC_HOMETOWN_SELL�
GPROTOC_ENTER_AFK_MODE�
GPROTOC_LEAVE_AFK_MODE�
GPROTOC_EXCHANGE_CASH�!
GPROTOC_APPLY_REVENGE_BATTLE�$
GPROTOC_RESPONSE_REVENGE_BATTLE�
GPROTOC_EQUIP_ITEM�
GPROTOC_GET_FORTUNE_CAT�
GPROTOC_DUEL_INVITE_C� 
GPROTOC_DUEL_INVITE_REPLY_C�
GPROTOC_EXTEND_FORTUNE_CAT�(
#GPROTOC_GET_BIND_MOBILE_PHONE_AWARD�$
GPROTOC_PLAYER_SEARCH_RANK_INFO�#
GPROTOC_USE_LOTTERY_ONE_BUTTON�#
GPROTOC_GET_LOTTERY_ONE_BUTTON�
GPROTOC_SET_ID_PHOTO�
GPROTOC_PET_LEVELUP_READAN�
GPROTOC_PET_CLEAR_READAN�
GPROTOC_HOMETOWN_BENEFIT�
GPROTOC_USE_RENAME_ITEM�
GPROTOC_ILLEGAL_REPORT�
GPROTOC_CHANGE_FACEID�
GPROTOC_MARRIAGE_SNATCHING�
GPROTOC_PET_SKILL_LOCK�
GPROTOC_PET_UPGRADE�
GPROTOC_HOMETOWN_FARM_OP�
GPROTOC_SELECT_GFX_SUIT�
GPROTOC_USE_DICE�"
GPROTOC_FASHION_TITLE_UPGRADE�
GPROTOC_CHILD_ADD_PROPERTY�!
GPROTOC_CHILD_CLEAR_PROPERTY�$
GPROTOC_CHILD_SET_PROPERTY_PLAN�
GPROTOC_CHILD_REBORN�&
!GPROTOC_CHECK_WEDDING_SNATCH_COND�!
GPROTOC_CHILD_PREGNATE_REPLY�!
GPROTOC_CHILD_DELIVERY_REPLY�
GPROTOC_CHILD_RENAME�
GPROTOC_SUMMON_CHILD�
GPROTOC_RECALL_CHILD�
GPROTOC_CHILD_LEARN_SKILL�
GPROTOC_CHILD_CULTURE�
GPROTOC_CHILD_SLEEP�
GPROTOC_DEVOUR_DESTINY�
GPROTOC_MOVE_DESTINY�
GPROTOC_REFRESH_DESTINY�"
GPROTOC_SECOND_HOMETOWN_ENTER�(
#GPROTOC_CENTER_FACTION_BATTLE_AWARD�
GPROTOC_CHILD_TALK�
GPROTOC_GUAJI_FENGMO�(
#GPROTOC_CHILD_MARRIAGE_INVITE_REPLY�%
 GPROTOC_CHILD_MARRIAGE_PUT_CHILD�#
GPROTOC_CHILD_MARRIAGE_CONFIRM�(
#GPROTOC_CHILD_MARRIAGE_SELECT_TITLE�
GPROTOC_CHILD_TOUR�!
GPROTOC_CHILD_TOUR_GET_AWARD�
GPROTOC_CHILD_DIVORCE_OP�
GPROTOC_CHILD_ATTACH_GEM�
GPROTOC_CHILD_DETACH_GEM�!
GPROTOC_ITEM_COMBINE_ADVANCE�
GPROTOC_CHILD_LIANXING�'
"GPROTOC_EXTEND_FASHION_COLOR_LIMIT�
GPROTOC_PLAYER_SLAVE_OP�&
!GPROTOC_CORPS_SERVER_BATTLE_AWARD� 
GPROTOC_USE_ITEM_WITH_COUNT�
GPROTOC_REFENCE_OPERATE�
GPROTOC_SOCIAL_SPACE_OP�
GPROTOC_HIDE_FASHION� 
GPROTOC_MULT_SELECT_FASHION�
GPROTOC_MULT_PAINT_FASHION�
GPROTOC_TASK_FIND_NPC�
GPROTOC_ENTER_PELT�
GPROTOC_INTERACT_MATTER�
GPROTOC_INTERACT_SPEEDUP�
GPROTOC_EQUIP_ENHANCE�
GPROTOC_KOTODAMA_OP� 
GPROTOC_INSTANCE_LUCKY_DRAW�$
GPROTOC_FRIEND_ADD_EMAIL_INVITE�
GPROTOC_CHATBOX_SELECT�
GPROTOC_TALENT�!
GPROTOC_EQUIP_SKILL_TRANSFER�
GPROTOC_GUARD_RENAME�
GPROTOC_GUARD_CHANGE_SHAPE�
GPROTOC_GUARD_SUMMON�
GPROTOC_GUARD_TRAIN�
GPROTOC_GUARD_CHANGE_SKILL�
GPROTOC_GUARD_PUTIN_SLOT�
GPROTOC_GUARD_TAKEOUT_SLOT�
GPROTOC_GUARD_SLOT_LEVELUP�
GPROTOC_GUARD_ADD_EXP�
GPROTOC_ROLE_SET_NAME�
GPROTOC_PLAYER_PRACTICE_SU�
GPROTOC_INTIMATE_OP�
GPROTOC_TAKE_PHOTO� 
GPROTOC_FAUCTION_STORE_OPEN�#
GPROTOC_FAUCTION_STORE_LEVELUP�
GPROTOC_SET_WECHAT_SWITCH�
GPROTOC_COLLECTION_OP�
GPROTOC_WISH_TREE_IRRIGATE�
GPROTOC_WISH_TREE_HARVEST�
GPROTOC_MATTER_PAUSE_MOVE�
GPROTOC_ROLL_ITEM_REQUEST�
GPROTOC_RUNINTO_NPC�
GPROTOC_LONGYU_UNLOCK�
GPROTOC_LONGYU_ACTIVATE�
GPROTOC_WEATHER_FORECAST�
GPROTOC_INTERACT_OPERATION�!
GPROTOC_FAKE_AUCTION_REFRESH�
GPROTOC_CAR_RACE_INNER�$
GPROTOC_CENTER_BATTLE_COMMANDER�#
GPROTOC_CENTER_BATTLE_TELEPORT�
GPROTOC_END_T�*)
collection_op_type
COT_REFRESH_POS*a
take_photo_type
TAKE_PHOTO_TYPE_NONE 
TAKE_PHOTO_TYPE_ENTER
TAKE_PHOTO_TYPE_LEAVE*?
GS_INTIMATE_OP_TYPE
GIOT_CHANGE_NAME
GIOT_DEL_FORCE*�
KOTODAMA_EVENT
KOTODAMA_EVENT_SYNC_ALL
KOTODAMA_EVENT_SELECT
KOTODAMA_EVENT_LEVEL_UP
KOTODAMA_EVENT_LEVEL_UP_TOP
KOTODAMA_EVENT_LEARN
KOTODAMA_EVENT_MODIFY_POINT*�
SOCIAL_SPACE_OP_TYPE
SS_OP_GIVE_GIFT
SS_OP_PLACE_GIFT

SS_OP_STEP
SS_OP_STYLE_SET
SS_OP_STYLE_GET
SS_OP_EXTEND_ALBUM*@
use_money_type

USE_M_BIND
USE_M_TRADE
	USE_M_MIX*V
STUNT_CONFIG_TYPE
SCT_SET_RUNE_SET 
SCT_UPGRADE_RUNE_SET
SCT_SET_SKILL*R
HOMETOWN_OPERATION
HOMETOWN_BENEFIT
HOMETOWN_RELAX
HOMETOWN_CLEAN*O
PLAYER_FARM_OP
	PFO_PLANT
	PFO_RAISE
PFO_HARVEST

PFO_UPDATE*�H
S2C_GS_PROTOC_TYPE
type_gp_self_enter_world�
type_gp_scene_info�!
type_gp_player_definite_info�%
 type_gp_instance_lucky_draw_info� 
type_gp_equip_starup_result�
type_gp_revive_times_info� 
type_gp_item_combine_result� 
type_gp_nation_escort_mount�"
type_gp_equip_transfer_result�
type_gp_pk_man�
type_gp_fight_back_list�
type_gp_auto_reward_list�
type_gp_vip_info�
type_gp_object_state�
type_gp_hero_defined_info�
type_gp_enemy_list�
type_gp_hero_incre_info�
type_auto_combat_config�
type_gp_lottery_prize�
type_gp_suit_info�!
type_gp_nation_escort_locate�'
"type_gp_equip_affixes_transfer_res�
type_gp_attack_loop�
type_gp_operation_result�
type_gp_start_cg�
type_gp_stop_cg�
type_gp_escort_speed_state�
type_gp_wine_info�$
type_gp_minigame_operate_notify�
type_gp_player_enter_scene�
type_gp_stunt_config_res�
type_gp_stunt_config�
type_gp_load_protoc_finish�
type_gp_player_enter_slice�
type_gp_npc_definite_info�
type_gp_npc_enter_scene�
type_gp_player_crop_config�
type_gp_transform_state�"
type_gp_object_change_faction�#
type_gp_broadcast_value_change�
type_gp_net_error_message�
type_gp_instance_info�
type_gp_inventory_size�
type_gp_level_result�
type_gp_npc_enter_slice�
type_gp_ipt_container�
type_gp_self_definite_info�#
type_gp_instance_finished_info�$
type_gp_equip_attach_gem_result�
type_gp_gs_ping� 
type_gp_equip_refine_result�
type_gp_level_score�
type_gp_player_list_info�
type_gp_level_info�&
!type_gp_scene_special_object_info�
type_gp_matter_enter_scene�
type_gp_notify_ask_help�
type_gp_matter_info�
type_gp_gain_surface�,
'type_gp_equip_attach_gem_upgrade_result�
type_gp_find_way_result�$
type_gp_equip_detach_gem_result�#
type_gp_npc_definite_info_list�
type_gp_npc_info�"
type_gp_easy_mall_service_end�&
!type_gp_player_definite_info_list�
type_gp_matter_info_list�!
type_gp_notify_monitor_blood�
type_gp_notify_prop_ready�
type_gp_retrieve_info�
type_gp_red_packet�%
 type_gp_equip_skill_transfer_res�
type_gp_gs_error_message�
type_gp_multi_exp�
type_gp_refuse_fight�!
type_gp_deliver_compensation�#
type_gp_send_level_sorted_info�
type_gp_secure_idip�
type_gp_family_mafia_info�
type_gp_property_re�
type_gp_produce_skill�*
%type_gp_produce_skill_level_up_result� 
type_gp_produce_item_result�%
 type_gp_change_equip_suit_result�
type_gp_self_equip_suit�
type_gp_fashion_info�
type_gp_active_fashion�"
type_gp_select_fashion_result�%
 type_gp_activity_exchange_result�
type_gp_pet_property_re� 
type_gp_pet_select_skill_re�!
type_gp_talisman_data_notify� 
type_gp_retinue_data_notify�
type_gp_pet_reborn_re�
type_gp_pet_mixis_re�
type_gp_pet_train_re�
type_gp_pet_rename_re�"
type_gp_flysword_defined_info�&
!type_gp_double_exp_disable_notify�!
type_gp_practice_data_notify�(
#type_gp_active_fashion_color_result�!
type_gp_paint_fashion_result�
type_gp_pet_use_item_re�
type_gp_task_system_speak�)
$type_gp_equip_baseattr_refine_result�*
%type_gp_equip_extraattr_recast_result�
type_gp_exp_lottery_result�%
 type_gp_fauction_selflist_result�&
!type_gp_fauction_get_money_result�!
type_gp_fauction_open_result�#
type_gp_fauction_reopen_result� 
type_gp_fauction_buy_result�"
type_gp_fauction_close_result�
type_gp_upgrade_all_skill�
type_gp_cash_notify�"
type_gp_fauction_state_notify�4
/type_gp_player_search_corps_battle_score_result�
type_gp_corps_battle_end�
type_gp_arena_battle_end�
type_gp_pet_learn_skill_re�,
'type_gp_player_send_red_envelope_result�,
'type_gp_player_draw_red_envelope_result�/
*type_gp_player_notify_receive_red_envelope�
type_gp_pet_property�
type_gp_marital_statue�
type_gp_xyxw_invite�
type_gp_xyxw_invite_reply�
type_gp_xyxw_request�
type_gp_xyxw_request_reply�
type_gp_xyxw_info�
type_gp_xyxw_stop�(
#type_gp_hometown_show_object_result�(
#type_gp_hometown_hide_object_result�)
$type_gp_hometown_client_objects_info�"
type_gp_hometown_enter_result� 
type_gp_hometown_buy_result�!
type_gp_hometown_sell_result�!
type_gp_marriage_anniversary�
type_gp_fortune_cat_info�
type_gp_fortune_cat_result�
type_gp_duel_prepare�
type_gp_duel_cancel�
type_gp_duel_start�
type_gp_duel_stop�
type_gp_duel_result�
type_gp_duel_out_of_range�
type_gp_duel_invite�
type_gp_duel_invite_reply�"
type_gp_revenge_battle_letter�(
#type_gp_revenge_battle_apply_result� 
type_gp_revenge_battle_info�+
&type_gp_revenge_battle_response_result�*
%type_gp_revenge_battle_enter_instance�%
 type_gp_hero_gain_surface_notify�
type_gp_fashion_expire�
type_gp_fashion_renew�)
$type_gp_active_intimate_friend_title�%
 type_gp_sect_mentor_title_notify�
type_gp_center_battle_end�+
&type_gp_player_search_rank_info_result�&
!type_gp_lottery_one_button_result� 
type_gp_player_guaji_result�"
type_gp_change_photoid_result�(
#type_gp_active_surface_color_result�'
"type_gp_paint_surface_color_result�)
$type_gp_update_roam_battle_kill_info�"
type_gp_pet_levelup_readan_re� 
type_gp_pet_clear_readan_re�
type_gp_rename_inform�"
type_gp_illegal_report_result�
type_gp_change_prof�
type_gp_change_faceid_re�
type_gp_pet_skill_lock_re�
type_gp_pet_upgrade_re�
type_gp_hometown_farm_re� 
type_gp_center_battle_prize�%
 type_gp_fashion_title_upgrade_re�
type_gp_child_property_re�
type_gp_child_reborn_re�*
%type_gp_check_wedding_snatch_cond_res�#
type_gp_child_request_pregnate�*
%type_gp_child_request_pregnate_result�#
type_gp_child_request_delivery�*
%type_gp_child_request_delivery_result�
type_gp_child_rename_re�
type_gp_child_use_item_re�
type_gp_child_recv_exp�
type_gp_child_level_up�
type_gp_summon_child_re�
type_gp_recall_child_re�"
type_gp_wedding_snatch_notify�"
type_gp_child_adoption_result�
type_gp_destiny_result�"
type_gp_child_marriage_invite�)
$type_gp_child_marriage_invite_result�!
type_gp_child_marriage_start�!
type_gp_child_marriage_state�'
"type_gp_child_marriage_inlaws_info�
type_gp_invite_couple_tour�+
&type_gp_child_marriage_send_invite_res�"
type_gp_child_marriage_result�!
type_gp_child_divorce_notify� 
type_gp_active_inlaws_title� 
type_gp_child_starup_result�.
)type_gp_extend_fashion_color_limit_result�
type_gp_player_slave_op_re� 
type_gp_player_refence_info�
type_gp_social_space_op_re� 
type_gp_hide_fashion_result�&
!type_gp_mult_paint_fashion_result�'
"type_gp_mult_select_fashion_result�!
type_gp_task_find_npc_result�
type_gp_photo_info�
type_gp_counter_data�
type_gp_counter_change�
type_gp_npc_affix_data_chg�
type_gp_npc_play_sound�
type_gp_switch_data�
type_gp_switch_change�
type_gp_enter_pelt_result�
type_gp_interact_info�
type_gp_buff_box_fx�
type_gp_enhance_info�
type_gp_enhance_result�
type_gp_kotodama_op_result�
type_gp_object_buff�
type_gp_player_chatbox�
type_gp_talent�
type_gp_talent_res�
type_gp_guard_data�
type_gp_guard_get�
type_gp_guard_name_change�&
!type_gp_guard_change_shape_notice� 
type_gp_guard_summon_notice�
type_gp_guard_train_result�&
!type_gp_guard_change_skill_result�
type_gp_guard_prop_update�
type_gp_guard_slot_update�!
type_gp_guard_add_exp_result�
type_gp_guard_cast_skill�
type_gp_cooldown�
type_gp_xyxw_qqmm�
type_gp_retinue_cast_skill�!
type_gp_role_set_name_result�
type_gp_instance_finish�
type_gp_drag_point�
type_gp_drag_line�
type_gp_drag_remove�
type_gp_dice_result�
type_gp_intimate_op_re�
type_gp_take_photo_ret�
type_gp_duel_info�
type_gp_set_dir_and_camera�'
"type_gp_fauction_store_open_result�*
%type_gp_fauction_store_levelup_result�"
type_gp_fauction_store_update�
type_gp_scene_weather�
type_gp_collection_op_re�
type_gp_collection_notify�
type_gp_wish_tree_data�&
!type_gp_wish_tree_irrigate_result�%
 type_gp_wish_tree_harvest_result� 
type_gp_social_space_notify�
type_gp_matter_enter_slice�
type_gp_notice_roll_items�
type_gp_roll_item_response�
type_gp_roll_item_result�
type_gp_runinto_npc�
type_gp_longyu_collection�!
type_gp_longyu_unlock_notify�#
type_gp_longyu_activate_notify�
type_gp_weather_forecast� 
type_gp_receive_reward_fail�%
 type_gp_boardcast_fashion_change�!
type_gp_battle_damage_notify�$
type_gp_battle_try_close_notify�$
type_gp_fauction_refresh_result� 
type_gp_adventure_task_list�"
type_gp_adventure_task_finish�
type_gp_car_race_inner�+
&type_gp_center_battle_commander_notify�
GP_END_T�*K
GPT_OP_TYPE

GPT_ACTIVE 

GPT_EXTERN
GPT_NAME

GPT_TALENT